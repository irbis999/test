FROM node:15

ENV APP_ROOT /web
ENV TZ Europe/Moscow

WORKDIR ${APP_ROOT}
COPY . ${APP_ROOT}

RUN yarn install
RUN yarn build

EXPOSE 4000

CMD ["yarn", "start"]
