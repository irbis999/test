/* eslint-disable @typescript-eslint/no-var-requires */
// https://habr.com/ru/post/492742/
const client = require("prom-client");
const register = new client.Registry();
client.collectDefaultMetrics({ register });

const express = require("express");
const app = express();
const argv = require("minimist")(process.argv.slice(2));
const port = argv.port || 4000;

app.use(express.static("dist"));

app.get("/metrics", async (req, res) => {
  try {
    res.setHeader("Content-Type", register.contentType);
    res.end(await register.metrics());
  } catch (e) {
    res.statusCode = 500;
    res.end(e.message);
  }
});

app.get("*", (req, res) => {
  res.sendFile("./dist/index.html", { root: __dirname });
});

app.listen(port, () => {
  console.log(`Admin app listening at http://localhost:${port}`);
});
