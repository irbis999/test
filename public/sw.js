/* eslint-disable */
importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js')

workbox.setConfig({debug: false})

workbox.routing.registerRoute(
  new RegExp('/_nuxt/.+'),
  new workbox.strategies.CacheFirst({
    cacheName: 'nuxt-cache',
    plugins: [new workbox.expiration.ExpirationPlugin({maxEntries: 500})],
  }),
)
