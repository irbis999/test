window.tinyMCE.PluginManager.add('typograf', function (editor) {
  editor.ui.registry.addButton('tp', {
    tooltip: 'Расставить кавычки',
    text: '«»',
    onAction () {
      const elems = editor.selection.getNode()
        .closest('body')
        .querySelectorAll('p, h2, h3, h4')
      for (const elem of elems)
        elem.innerHTML = window.Typograf.execute(elem.innerHTML)
    },
  })
})
