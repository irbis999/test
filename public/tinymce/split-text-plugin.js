window.tinyMCE.PluginManager.add('split_text', function (editor) {
  function getNextSiblings (element) {
    const items = []
    while (element) {
      items.push(element)
      element = element.nextSibling
    }
    return items
  }

  editor.ui.registry.addButton('split_text', {
    tooltip: 'Разбить текст',
    text: '⤮',
    onAction () {
      const elem = editor.selection.getNode()
        .closest('p, h2, h3, h4, blockquote')
      const elements = getNextSiblings(elem)
      let html = ''
      for (const el of elements) {
        html += el.outerHTML
        el.remove()
      }
      editor.fire('splitText', {html})
    },
  })
})
