import { InjectionKey } from "vue";
import { createStore, Store, useStore as baseUseStore } from "vuex";
import {
  PlatformType,
  RoleType,
  UserType,
  MatterTypeType,
  StoryType,
} from "@/helpers/types";

type Obj = {
  id: number | string;
  title: string;
};

export interface ReferencesType {
  matter_types: { id: MatterTypeType; title: string; can_create: boolean }[];
  platforms: PlatformType[];
  roles: RoleType[];
  statuses: { id: string; title: string }[];
  districts: Obj[];
  sales_categories: Obj[];
  permissions: { id: string; title: string; category_id: string }[];
  stories: StoryType[];
  users: UserType[];
  persona_categories: { id: string; title: string }[];
  templates: Obj[];
  topline_item_themes: Obj[];
  editorial_types: Obj[];
  notifications_count: number;
}

export interface State extends ReferencesType {
  user: UserType;
  loggedIn: boolean;
  orderOptions: { id: string; title: string }[];
  autoscrollAllowedTypes: string[];
}

// https://next.vuex.vuejs.org/guide/typescript-support.html#typing-usestore-composition-function
export const key: InjectionKey<Store<State>> = Symbol("store");
export const useStore = (): Store<State> => baseUseStore(key);
export const store = createStore<State>({
  state: {
    matter_types: [],
    platforms: [],
    roles: [],
    statuses: [],
    permissions: [],
    stories: [],
    users: [],
    persona_categories: [],
    templates: [],
    topline_item_themes: [],
    editorial_types: [],
    notifications_count: 0,
    districts: [],
    sales_categories: [],
    orderOptions: [
      { id: "created_at,desc", title: "По дате создания ↓" },
      { id: "created_at,asc", title: "По дате создания ↑" },
      { id: "updated_at,desc", title: "По дате изменения ↓" },
      { id: "updated_at,asc", title: "По дате изменения ↑" },
      { id: "published_at,desc", title: "По дате публикации ↓" },
      { id: "published_at,asc", title: "По дате публикации ↑" },
    ],
    autoscrollAllowedTypes: [
      "Matter::NewsItem",
      "Matter::Article",
      "Matter::Opinion",
      "Matter::Video",
      "Matter::Gallery",
      "Matter::Podcast",
      "Matter::Broadcast",
    ],
    loggedIn: false,
    user: {
      authorization: "",
      created_at: "",
      first_name: "",
      full_name: "",
      id: 0,
      is_active: true,
      last_name: "",
      platform_ids: [],
      role: {
        id: 0,
        is_admin: false,
        permissions: [],
        rank: 0,
        title: "",
        created_at: "",
        updated_at: "",
      },
      updated_at: "",
      email: "",
      phone: "",
      last_sign_in_at: "",
    },
  },
  mutations: {
    LOG_IN(state, user: UserType) {
      state.loggedIn = true;
      state.user = user;
      localStorage.setItem("token", user.authorization);
    },
    LOG_OUT() {
      localStorage.removeItem("token");
      location.reload();
    },
    LOCK_SCROLL() {
      document.documentElement.classList.add("locked");
    },
    UNLOCK_SCROLL() {
      document.documentElement.classList.remove("locked");
    },
    UPDATE_NOTIFICATIONS_COUNT(state, num) {
      state.notifications_count += num;
    },
    RESET_NOTIFICATIONS_COUNT(state) {
      state.notifications_count = 0;
    },
    SET_REFERENCES(state, data: ReferencesType) {
      state.matter_types = data.matter_types;
      state.platforms = data.platforms;
      state.roles = data.roles;
      state.statuses = data.statuses;
      state.districts = data.districts;
      state.sales_categories = data.sales_categories;
      state.permissions = data.permissions;
      state.stories = data.stories;
      state.users = data.users;
      state.persona_categories = data.persona_categories;
      state.templates = data.templates;
      state.topline_item_themes = data.topline_item_themes;
      state.editorial_types = data.editorial_types;
      state.notifications_count = data.notifications_count;
    },
    ADD_PLATFORM(state, item: PlatformType) {
      state.platforms.push(item);
    },
    UPDATE_PLATFORM(state, item: PlatformType) {
      const index = state.platforms.findIndex((el) => el.id === item.id);
      state.platforms.splice(index, 1, item);
    },
    REMOVE_PLATFORM(state, item: PlatformType) {
      const index = state.platforms.findIndex((el) => el.id === item.id);
      state.platforms.splice(index, 1);
    },
    ADD_ROLE(state, item: RoleType) {
      state.roles.push(item);
    },
    UPDATE_ROLE(state, item: RoleType) {
      const index = state.roles.findIndex((el) => el.id === item.id);
      state.roles.splice(index, 1, item);
    },
    REMOVE_ROLE(state, item: RoleType) {
      const index = state.roles.findIndex((el) => el.id === item.id);
      state.roles.splice(index, 1);
    },
    ADD_USER(state, item: UserType) {
      state.users.push(item);
    },
    UPDATE_USER(state, item: UserType) {
      const index = state.users.findIndex((el) => el.id === item.id);
      state.users.splice(index, 1, item);
    },
    REMOVE_USER(state, item: UserType) {
      const index = state.users.findIndex((el) => el.id === item.id);
      state.users.splice(index, 1);
    },
  },
});
