import { defineComponent, onMounted } from "vue";
import { RouterView } from "vue-router";
import Topline from "./components/Topline";
import device from "./helpers/device";
import { useStore } from "./store";

export default defineComponent({
  setup() {
    const store = useStore();
    onMounted(() => {
      if (device.isDesktop) {
        document.documentElement.classList.add("desktop");
      }
    });

    return () => (
      <div>
        {store.state.loggedIn && <Topline />}
        <RouterView />
      </div>
    );
  },
});
