/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType } from "vue";
import Textarea from "../form/Textarea";
import { QuoteBlockType } from "@/helpers/types";

export default defineComponent({
  name: "Quote",
  props: {
    item: { type: Object as PropType<QuoteBlockType>, required: true },
  },
  setup(props) {
    return () => (
      <div>
        <Textarea
          class="mb-2"
          value={props.item.text}
          onChange={(value) => (props.item.text = value)}
          required
          placeholder="Текст цитаты"
        >
          Текст цитаты
        </Textarea>
        <input
          v-model={props.item.source_title}
          class="f-input mb-2"
          placeholder="Название источника"
        />
        <input
          v-model={props.item.source_url}
          class="f-input"
          placeholder="Ссылка на источник"
          type="url"
        />
      </div>
    );
  },
});
