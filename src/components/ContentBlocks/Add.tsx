import { can } from "@/helpers/permissions";
import {
  ContentBlockType,
  GalleryBlockType,
  ImageType,
  CommonBlockType,
  HtmlBlockType,
  QuoteBlockType,
  TeaserMatterBlockType,
  TeaserGalleryBlockType,
  ReadAlsoBlockType,
  FileBlockType,
  VideoBlockType,
  ImageBlockType,
} from "@/helpers/types";
import { defineComponent, PropType, ref, Transition, withModifiers } from "vue";
import styles from "../../styles/components/AddContentBlock/index.module.scss";
import Loading from "../Loading";
import uploadFile from "@/helpers/uploadFile";
import emptyImage from "../ImageBlock/emptyImage";

export default defineComponent({
  name: "Add",
  props: {
    onAdd: {
      type: Function as PropType<(item: ContentBlockType) => void>,
      required: true,
    },
  },
  setup(props) {
    const active = ref(false);
    const busy = ref(false);

    function callOnAdd(block: ContentBlockType) {
      props.onAdd(block);
      active.value = false;
    }

    function createBaseBlock(): ContentBlockType {
      return {
        id: Math.random(),
        position: 0,
        kind: "common",
      };
    }

    function addCommonBlock() {
      const block: CommonBlockType = {
        ...createBaseBlock(),
        text: "",
        kind: "common",
      };
      callOnAdd(block);
    }

    function addHtmlBlock() {
      const block: HtmlBlockType = {
        ...createBaseBlock(),
        code: "",
        kind: "html",
      };
      callOnAdd(block);
    }

    function addQuoteBlock() {
      const block: QuoteBlockType = {
        ...createBaseBlock(),
        text: "",
        source_title: "",
        source_url: "",
        kind: "quote",
      };
      callOnAdd(block);
    }

    function addTeaserMatterBlock() {
      const block: TeaserMatterBlockType = {
        ...createBaseBlock(),
        matter_id: 0,
        matter_title: "",
        kind: "teaser_matter",
      };
      callOnAdd(block);
    }

    function addTeaserGalleryBlock() {
      const block: TeaserGalleryBlockType = {
        ...createBaseBlock(),
        matter_id: 0,
        matter_title: "",
        kind: "teaser_gallery",
      };
      callOnAdd(block);
    }

    function addReadAlsoBlock() {
      const block: ReadAlsoBlockType = {
        ...createBaseBlock(),
        title: "",
        read_also: [],
        kind: "read_also",
      };
      callOnAdd(block);
    }

    function addVideoBlock() {
      const block: VideoBlockType = {
        ...createBaseBlock(),
        code: "",
        image: { ...emptyImage },
        video: { ...emptyImage },
        kind: "video",
      };
      callOnAdd(block);
    }

    async function addFileBlock(e: Event) {
      const file = await upload(e);
      if (!file) return;
      const block: FileBlockType = {
        ...createBaseBlock(),
        kind: "file",
        file,
      };
      callOnAdd(block);
    }

    async function addImageBlock(e: Event, kind: "image" | "image360") {
      const file = await upload(e);
      if (!file) return;
      const block: ImageBlockType = {
        ...createBaseBlock(),
        kind,
        image: { ...emptyImage, ...file },
      };
      callOnAdd(block);
    }

    async function upload(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      if (busy.value || !files || !files[0]) return;
      busy.value = true;
      const file = await uploadFile(files[0], 1500);
      busy.value = false;
      return file;
    }

    async function addGallery(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      if (busy.value || !files) return;
      busy.value = true;
      const gallery: GalleryBlockType = {
        kind: "gallery",
        id: Math.random(),
        images: [] as ImageType[],
        position: 0,
      };
      for (const file of Array.from(files)) {
        const resource = await uploadFile(file, 1500);
        if (resource)
          gallery.images.push({
            ...emptyImage,
            ...resource,
            original_url: resource.url,
            id: Math.random(),
          });
      }
      props.onAdd(gallery);
      active.value = false;
      busy.value = false;
    }

    return () => (
      <div class="my-4">
        <div class="relative flex justify-center">
          <button
            class={styles.icon}
            type="button"
            onClick={() => (active.value = true)}
          />
        </div>
        <Transition name="fade-up" mode="out-in">
          {active.value && (
            /* eslint-disable jsx-a11y/no-static-element-interactions */
            /* eslint-disable jsx-a11y/click-events-have-key-events */
            <div
              class={styles.list}
              onClick={withModifiers(() => (active.value = false), ["self"])}
            >
              <div class={styles.content}>
                {can("edit_text") && (
                  <div>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__common}`}
                      onClick={addCommonBlock}
                    >
                      Текст
                    </button>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__quote}`}
                      onClick={addQuoteBlock}
                    >
                      Цитата
                    </button>
                  </div>
                )}
                {can("edit_media") && (
                  <div>
                    <label class={`${styles.item} ${styles.__image}`}>
                      Фото
                      <input
                        type="file"
                        hidden
                        accept="image/jpeg, image/png"
                        onChange={(e) => addImageBlock(e, "image")}
                      />
                    </label>
                    <label class={`${styles.item} ${styles.__gallery}`}>
                      Галерея
                      <input
                        type="file"
                        hidden
                        accept="image/jpeg, image/png"
                        multiple
                        onChange={addGallery}
                      />
                    </label>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__html}`}
                      onClick={addHtmlBlock}
                    >
                      Виджет
                    </button>
                    <label class={`${styles.item} ${styles.__image360}`}>
                      Фото 360
                      <input
                        type="file"
                        hidden
                        accept="image/jpeg, image/png"
                        onChange={(e) => addImageBlock(e, "image360")}
                      />
                    </label>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__video}`}
                      onClick={addVideoBlock}
                    >
                      Видео
                    </button>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__teaser}`}
                      onClick={addReadAlsoBlock}
                    >
                      Читайте также
                    </button>
                    <label class={`${styles.item} ${styles.__file}`}>
                      Файл
                      <input type="file" hidden onChange={addFileBlock} />
                    </label>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__teaser}`}
                      onClick={addTeaserMatterBlock}
                    >
                      Тизер материала
                    </button>
                    <button
                      type="button"
                      class={`${styles.item} ${styles.__teaser}`}
                      onClick={addTeaserGalleryBlock}
                    >
                      Тизер галереи
                    </button>
                  </div>
                )}
                {busy.value && <Loading />}
                <button
                  type="button"
                  class="btn mt-4"
                  onClick={() => (active.value = false)}
                >
                  Закрыть окно
                </button>
              </div>
            </div>
          )}
        </Transition>
      </div>
    );
  },
});
