/* eslint-disable vue/no-mutating-props */
import { FileBlockType } from "@/helpers/types";
import { defineComponent, PropType } from "vue";
import uploadFile from "@/helpers/uploadFile";
import styles from "../../styles/components/FileBlock.module.scss";

export default defineComponent({
  name: "FileBlock",
  props: {
    item: { type: Object as PropType<FileBlockType>, required: true },
  },
  setup(props) {
    let busy = false;
    async function upload(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      if (!files?.length || busy) return;
      busy = true;
      const file = await uploadFile(files[0]);
      if (file) {
        props.item.file.file_id = file.file_id;
        props.item.file.url = file.url;
        props.item.file.file_size = file.file_size;
        props.item.file.file_filename = file.file_filename;
        props.item.file.file_content_type = file.file_content_type;
        props.item.file.file_changed = file.file_changed;
      }
      busy = false;
    }

    return () => (
      <div>
        <div class="f-label">Файл</div>
        <div class={styles.cols}>
          <a href={props.item.file.url} target="_blank">
            Скачать файл
          </a>
          <div>
            <input
              class="f-input mb-4"
              v-model={props.item.file.description}
              placeholder="Название файлы"
              required
            />
            <input type="file" onChange={upload} />
          </div>
        </div>
      </div>
    );
  },
});
