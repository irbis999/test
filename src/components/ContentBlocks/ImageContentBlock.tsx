import { ImageBlockType } from "@/helpers/types";
import { defineComponent, PropType } from "vue";
import ImageBlock from "../ImageBlock";

export default defineComponent({
  name: "ImageContentBlock",
  props: {
    item: { type: Object as PropType<ImageBlockType>, required: true },
  },
  setup(props) {
    return () => (
      <div>
        <div class="f-label">
          {props.item.kind === "image" ? "Фото" : "Фото 360"}
        </div>
        <ImageBlock
          image={props.item.image}
          maxWidth={1500}
          watermark={props.item.kind === "image"}
        />
      </div>
    );
  },
});
