/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType, ref } from "vue";
import { MatterType, TeaserMatterBlockType } from "@/helpers/types";
import useInfScroll from "@/helpers/useInfScroll";
import debounce from "lodash.debounce";
import Loading from "../Loading";
import InfScroll from "../InfScroll";

export default defineComponent({
  name: "TeaserMatter",
  props: {
    item: { type: Object as PropType<TeaserMatterBlockType>, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      status: ["published"],
    });
    const { items, pending, fetch, busy } = useInfScroll<MatterType>(
      "matters",
      apiParams
    );

    const processInput = debounce(() => {
      if (!apiParams.value.q) return;
      fetch(true);
    }, 300);

    return () => (
      <div>
        <div class="f-label">Тизер материала</div>
        <input
          class="py-2 px-3 bg-blue-100 rounded-sm mb-2 block w-full outline-none cursor-default"
          required
          placeholder="Материал не выбран"
          onKeydown={(e) => e.preventDefault()}
          value={props.item.matter_title}
        />
        <input
          class="f-input"
          v-model={apiParams.value.q}
          placeholder="🔍 Поиск материала"
          onKeydown={(e) => {
            if (e.key === "Enter") e.preventDefault();
          }}
          onInput={processInput}
        />
        <div class="overflow-auto mt-2 border" style={{ height: "120px" }}>
          {items.value.map((el) => (
            <button
              type="button"
              key={el.id}
              class="py-2 px-3 cursor-pointer text-blue-500 hover:bg-blue-100 block w-full text-left"
              onClick={() => {
                props.item.matter_id = el.id;
                props.item.matter_title = el.title;
              }}
            >
              {el.title}
            </button>
          ))}
          {pending.value && <Loading class="my-8" />}
          <InfScroll
            class="my-8"
            onLoad={() => (apiParams.value.q ? fetch() : null)}
            busy={busy.value}
            parent
          />
        </div>
      </div>
    );
  },
});
