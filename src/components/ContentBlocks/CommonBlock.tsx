/* eslint-disable vue/no-mutating-props */
import { defineComponent, nextTick, PropType, ref, toRefs, watch } from "vue";
import Tiny from "../Tiny";
import { CommonBlockType } from "@/helpers/types";

export default defineComponent({
  name: "Common",
  props: {
    item: { type: Object as PropType<CommonBlockType>, required: true },
    dragId: { type: Number, required: true },
    onAdd: {
      type: Function as PropType<(item: CommonBlockType) => void>,
      required: true,
    },
  },
  setup(props) {
    const editorActive = ref(true);

    watch(toRefs(props).dragId, async () => {
      editorActive.value = false;
      await nextTick();
      editorActive.value = true;
    });

    function add(html: string) {
      props.onAdd({
        id: Math.random(),
        kind: "common",
        text: html,
        position: 0,
      });
    }

    return () => (
      <div>
        <div class="f-label">
          Текст
          <span class="text-red-700 ml-1">*</span>
        </div>
        {editorActive.value && (
          <Tiny
            value={props.item.text}
            onChange={(value) => (props.item.text = value)}
            onSplitText={(value) => add(value)}
          />
        )}
      </div>
    );
  },
});
