/* eslint-disable vue/no-mutating-props */
import { ReadAlsoBlockType } from "@/helpers/types";
import { defineComponent, PropType } from "vue";
import Input from "../form/Input";

export default defineComponent({
  name: "ReadAlso",
  props: {
    item: { type: Object as PropType<ReadAlsoBlockType>, required: true },
  },
  setup(props) {
    return () => (
      <div>
        <Input
          value={props.item.title}
          onChange={(value) => (props.item.title = value)}
          required
          placeholder="Например: материалы по теме"
        >
          Заголовок блока
        </Input>
        {props.item.read_also.map((el, i) => (
          <div>
            <div class="f-label flex justify-between">
              <span>
                Материал {i + 1}
                <span class="text-bg-red-600 ml-1">*</span>
              </span>
              <button
                class="remove"
                type="button"
                onClick={() => props.item.read_also.splice(i, 1)}
              >
                Удалить материал
              </button>
            </div>
            <input
              class="f-input mb-4"
              required
              placeholder="Заголовок материала"
              v-model={el.title}
            />
            <input
              class="f-input mb-4"
              required
              placeholder="Ссылка на материала"
              type="url"
              v-model={el.url}
            />
          </div>
        ))}
        <button
          type="button"
          class="btn"
          onClick={() => props.item.read_also.push({ title: "", url: "" })}
        >
          Добавить материал
        </button>
      </div>
    );
  },
});
