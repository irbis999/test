/* eslint-disable vue/no-mutating-props */
import {
  CommonBlockType,
  ContentBlockType,
  FileBlockType,
  GalleryBlockType,
  HtmlBlockType,
  ImageBlockType,
  QuoteBlockType,
  ReadAlsoBlockType,
  TeaserGalleryBlockType,
  TeaserMatterBlockType,
  VideoBlockType,
} from "@/helpers/types";
import { defineComponent, PropType, ref } from "vue";
import CommonBlock from "./CommonBlock";
import QuoteBlock from "./QuoteBlock";
import FileBlock from "./FileBlock";
import GalleryBlock from "./GalleryBlock";
import HtmlBlock from "./HtmlBlock";
import ImageContentBlock from "./ImageContentBlock";
import ReadAlsoBlock from "./ReadAlsoBlock";
import TeaserGalleryBlock from "./TeaserGalleryBlock";
import TeaserMatterBlock from "./TeaserMatterBlock";
import VideoBlock from "./VideoBlock";
import Add from "./Add";
import { cant } from "@/helpers/permissions";
import styles from "../../styles/components/ContentBlocks.module.scss";

interface ItemType {
  content_blocks: ContentBlockType[];
}

export default defineComponent({
  name: "ContentBlocks",
  props: {
    item: { type: Object as PropType<ItemType>, required: true },
  },
  setup(props) {
    const dragId = ref(Math.random());

    function setPositions() {
      props.item.content_blocks.forEach((el, i) => (el.position = i));
    }

    function removeBlock(index: number, item: ContentBlockType) {
      item.id >= 1
        ? (item._destroy = true)
        : props.item.content_blocks.splice(index, 1);
    }

    function moveBlock(item: ContentBlockType, inc: number) {
      const index = props.item.content_blocks.indexOf(item);
      // Крайние положения
      if (index === props.item.content_blocks.length - 1 && inc > 0) return;
      if (index === 0 && inc < 0) return;
      props.item.content_blocks.splice(index, 1);
      props.item.content_blocks.splice(index + inc, 0, item);
      dragId.value = Math.random();
      setPositions();
    }

    function itemClass(item: ContentBlockType) {
      const isText = ["common", "quote"].includes(item.kind);
      const isMedia = !isText;
      const cantEditText = isText && cant("edit_text");
      const cantEditMedia = isMedia && cant("edit_media");
      return {
        [styles.item]: true,
        "pointer-events-none": cantEditText || cantEditMedia,
        "opacity-50": cantEditText || cantEditMedia,
      };
    }

    function addBlock(index: number, item: ContentBlockType) {
      props.item.content_blocks.splice(index, 0, item);
      setPositions();
    }

    return () => (
      <div class="mb-6">
        <div class="f-label">Контент</div>
        <Add onAdd={(item) => addBlock(0, item)} />
        {props.item.content_blocks.map((el, i) => (
          <div key={el.id} class="relative mb-6" v-show={!el._destroy}>
            <div class={itemClass(el)}>
              {el.kind === "common" && (
                <CommonBlock
                  item={el as CommonBlockType}
                  dragId={dragId.value}
                  onAdd={(value) => addBlock(i + 1, value)}
                />
              )}
              {el.kind === "quote" && (
                <QuoteBlock item={el as QuoteBlockType} />
              )}
              {el.kind === "file" && <FileBlock item={el as FileBlockType} />}
              {el.kind === "gallery" && (
                <GalleryBlock item={el as GalleryBlockType} />
              )}
              {el.kind === "html" && <HtmlBlock item={el as HtmlBlockType} />}
              {["image", "image360"].includes(el.kind) && (
                <ImageContentBlock item={el as ImageBlockType} />
              )}
              {el.kind === "read_also" && (
                <ReadAlsoBlock item={el as ReadAlsoBlockType} />
              )}
              {el.kind === "teaser_gallery" && (
                <TeaserGalleryBlock item={el as TeaserGalleryBlockType} />
              )}
              {el.kind === "teaser_matter" && (
                <TeaserMatterBlock item={el as TeaserMatterBlockType} />
              )}
              {el.kind === "video" && (
                <VideoBlock item={el as VideoBlockType} />
              )}
              <div class={styles.controls}>
                <button
                  type="button"
                  class={`btn m-icon ${styles.button} ${styles.handle}`}
                  title="Переместить вверх"
                  onClick={() => moveBlock(el, -1)}
                >
                  arrow_upward
                </button>
                <button
                  type="button"
                  class={`btn m-icon ${styles.button}`}
                  title="Удалить блок"
                  onClick={() => removeBlock(i, el)}
                >
                  clear
                </button>
                <button
                  type="button"
                  class={`btn m-icon ${styles.button} ${styles.handle} ${styles.__bottom}`}
                  title="Переместить вниз"
                  onClick={() => moveBlock(el, 1)}
                >
                  arrow_downward
                </button>
              </div>
            </div>
            <Add onAdd={(item) => addBlock(i + 1, item)} />
          </div>
        ))}
      </div>
    );
  },
});
