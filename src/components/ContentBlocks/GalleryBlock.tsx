/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable vue/no-mutating-props */
import { GalleryBlockType, ImageType } from "@/helpers/types";
import { computed, defineComponent, PropType, ref } from "vue";
import uploadFile from "@/helpers/uploadFile";
import emptyImage from "../ImageBlock/emptyImage";
import ImageBlock from "../ImageBlock";
import Dragg from "vuedraggable";

// https://www.typescriptlang.org/docs/handbook/2/functions.html#construct-signatures
type DraggableType = new () => {
  $props: {
    list: unknown[];
    itemKey: string;
    class?: string;
    delay?: number;
    delayOnTouchOnly?: boolean;
  };
};
const Draggable = Dragg as unknown as DraggableType;

export default defineComponent({
  name: "GalleryBlock",
  props: {
    item: { type: Object as PropType<GalleryBlockType>, required: true },
  },
  setup(props) {
    const activeId = ref<number | null>(null);

    const activeImage = computed(() => {
      if (activeId.value === null) return null;
      const image = props.item.images.find((el) => el.id === activeId.value);
      if (!image) return null;
      return image._destroy ? null : image;
    });

    function removeImage(img: ImageType) {
      img.id > 1
        ? (img._destroy = true)
        : props.item.images.splice(props.item.images.indexOf(img), 1);
      const image = props.item.images.find((el) => !el._destroy);
      activeId.value = image ? image.id : null;
    }

    let busy = false;
    async function addImages(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      if (!files || busy) return;
      busy = true;
      for (const file of Array.from(files)) {
        const resource = await uploadFile(file, 1500);
        if (resource)
          props.item.images.push({
            ...emptyImage,
            ...resource,
            id: Math.random(),
          });
      }
      busy = false;
    }

    function imageClass(img: ImageType) {
      return {
        "border-white": true,
        "border-4": true,
        relative: true,
        "border-blue-500": activeImage.value?.id === img.id,
      };
    }

    // https://github.com/SortableJS/vue.draggable.next/issues/44
    const slots = {
      item: ({ element: el }: { element: ImageType }) => (
        <div class={imageClass(el)} v-show={!el._destroy}>
          <img
            src={el.url}
            alt="Изображение"
            onClick={() => (activeId.value = el.id)}
            style={{ height: "65px" }}
          />
          <button
            type="button"
            class="m-icon bg-white cursor-pointer absolute bg-center bg-no-repeat right-0 top-0 h-10 w-10"
            hidden={activeId.value === el.id}
            onClick={() => removeImage(el)}
          >
            highlight_off
          </button>
        </div>
      ),
    };

    return () => (
      <div>
        <div class="f-label">Галерея</div>
        {activeImage.value && <ImageBlock image={activeImage.value} />}
        <div>
          <Draggable
            delay={100}
            delayOnTouchOnly={true}
            class="flex flex-wrap my-4"
            list={props.item.images}
            itemKey="id"
          >
            {slots}
          </Draggable>
          <label class="btn">
            Добавить изображения
            <input
              hidden
              type="file"
              accept="image/jpeg, image/png"
              multiple
              onChange={addImages}
            />
          </label>
        </div>
      </div>
    );
  },
});
