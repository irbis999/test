/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType } from "vue";
import Textarea from "@/components/form/Textarea";
import { HtmlBlockType } from "@/helpers/types";

export default defineComponent({
  name: "Html",
  props: {
    item: { type: Object as PropType<HtmlBlockType>, required: true },
  },
  setup(props) {
    return () => (
      <Textarea
        value={props.item.code}
        onChange={(value) => (props.item.code = value)}
        required
      >
        Код виджета
      </Textarea>
    );
  },
});
