/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType } from "vue";
import ImageBlock from "../ImageBlock";
import uploadFile from "@/helpers/uploadFile";
import styles from "../../styles/components/VideoBlock.module.scss";
import { VideoBlockType } from "@/helpers/types";

export default defineComponent({
  props: {
    item: { type: Object as PropType<VideoBlockType>, required: true },
  },
  setup(props) {
    let busy = false;
    async function upload(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      if (!files?.length || busy) return;
      busy = true;
      const file = await uploadFile(files[0]);
      if (file) {
        props.item.video.file_id = file.file_id;
        props.item.video.url = file.url;
        props.item.video.file_size = file.file_size;
        props.item.video.file_filename = file.file_filename;
        props.item.video.file_content_type = file.file_content_type;
        props.item.video.file_changed = file.file_changed;
      }
      console.log(props.item.video);
      busy = false;
    }

    return () => (
      <div>
        <div class="f-label"></div>
        <ImageBlock image={props.item.image} removable />
        <div class={styles.cols}>
          <div>
            <div class="f-label">Код видео</div>
            <input
              class="f-input"
              v-model={props.item.code}
              placeholder="Вставьте код с видео - iframe"
              pattern="<iframe.+</iframe>"
            />
          </div>
          <div>
            <div class="f-label">Или загрузите файл с видео</div>
            {props.item.video.url && (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video
                class={styles.video}
                src={props.item.video.url}
                controls
                preload="metadata"
              />
            )}
            <input type="file" accept="video/mp4" onChange={upload} />
          </div>
        </div>
      </div>
    );
  },
});
