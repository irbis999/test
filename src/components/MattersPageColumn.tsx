import { computed, defineComponent, PropType, ref } from "vue";
import device from "@/helpers/device";
import styles from "../styles/components/MattersPageColumn.module.scss";
import { MatterType, MatterUsersType } from "@/helpers/types";
import { useStore } from "@/store";
import useInfScroll from "@/helpers/useInfScroll";
import { SelectString } from "./form/Select";
import MatterCard from "./MatterCard";
import Loading from "./Loading";
import InfScroll from "./InfScroll";

export default defineComponent({
  props: {
    status: {
      type: Object as PropType<{ id: string; title: string }>,
      required: true,
    },
    matters: { type: Array as PropType<MatterType[]>, required: true },
    params: {
      type: Object as PropType<{ [key: string]: unknown }>,
      required: true,
    },
    mattersUsers: { type: Array as PropType<MatterUsersType>, required: true },
  },
  setup(props) {
    const store = useStore();
    const lsKey = `indexSort_${props.status.id}`;
    const orderChanged = ref(false);
    const apiParams = ref({
      ...props.params,
      status: [props.status.id],
      order: ref(localStorage.getItem(lsKey) || "created_at,desc"),
    });
    const { items, fetch, busy, pending } = useInfScroll("matters", apiParams);
    const itemsToShow = computed(() => {
      return orderChanged.value
        ? items.value
        : [...props.matters, ...items.value];
    });

    return () => (
      <div>
        <div class={styles.top}>
          <div class="font-bold">{props.status.title}</div>
          <SelectString
            value={apiParams.value.order}
            options={store.state.orderOptions}
            onChange={(value) => {
              apiParams.value.order = value;
              orderChanged.value = true;
              localStorage.setItem(lsKey, apiParams.value.order);
              fetch(true);
            }}
          ></SelectString>
        </div>
        <div
          class={{
            [styles.matters]: true,
            [styles.__windows]: device.isWindows,
          }}
        >
          {itemsToShow.value.map((el, i) => (
            <MatterCard
              key={i}
              matter={el}
              mattersUsers={props.mattersUsers}
            ></MatterCard>
          ))}
          {pending.value && <Loading />}
          <InfScroll
            onLoad={() => (itemsToShow.value.length < 20 ? null : fetch())}
            busy={busy.value}
            parent
          />
        </div>
      </div>
    );
  },
});
