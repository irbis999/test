import { defineComponent, onBeforeUnmount, PropType } from "vue";
import { useStore } from "@/store";
import styles from "../styles/components/Modal.module.scss";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type OnCloseType = (...args: any) => any;

export default defineComponent({
  name: "Modal",
  props: {
    onClose: { type: Function as PropType<OnCloseType>, required: true },
  },
  setup(props, { slots }) {
    const store = useStore();
    store.commit("LOCK_SCROLL");
    onBeforeUnmount(() => store.commit("UNLOCK_SCROLL"));

    return () => (
      <div class={styles.modal}>
        <div class={styles.content}>
          <button
            class="m-icon text-black large absolute right-0 top-0 m-2 cursor-pointer opacity-50 transition duration-300 hover:opacity-100"
            onClick={props.onClose}
          >
            close
          </button>
          {slots.default ? slots.default() : null}
        </div>
      </div>
    );
  },
});
