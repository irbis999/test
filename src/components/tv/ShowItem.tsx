import { defineComponent, PropType, ref, Transition } from "vue";
import api from "../../helpers/api";
import { error } from "../../helpers/notifications";
import Modal from "../Modal";
import ShowForm, { TvShowType } from "./ShowForm";

export default defineComponent({
  name: "ShowItem",
  props: {
    item: { type: Object as PropType<TvShowType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: TvShowType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление программы")) return;
      busy = true;
      try {
        await api.delete(`/television/shows/${props.item.id}`);
        if (props.onRemove) props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <ShowForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <button onClick={() => (modalVisible.value = true)}>
          {props.item.title}
        </button>
        <button onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
