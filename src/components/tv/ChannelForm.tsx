import { defineComponent, onMounted, PropType, ref, withModifiers } from "vue";
import Loading from "../Loading";
import api from "../../helpers/api";
import { error } from "../../helpers/notifications";
import emptyImage from "../ImageBlock/emptyImage";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import ImageBlock from "../ImageBlock";
import { ImageType } from "@/helpers/types";

export interface TvChannelType {
  id: number;
  title: string;
  short_title: string;
  description: string;
  image: ImageType;
}

export default defineComponent({
  name: "ChannelForm",
  props: {
    id: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(item: TvChannelType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<TvChannelType>({
      id: 0,
      title: "",
      short_title: "",
      description: "",
      image: { ...emptyImage },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<TvChannelType>(
          `television/channels/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `television/channels/${item.value.id}`
          : "television/channels";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<TvChannelType>(url, {
          television_channel: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Телеканал</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
          >
            Название
          </Input>
          <Input
            value={item.value.short_title}
            onChange={(value) => (item.value.short_title = value)}
          >
            Короткое название
          </Input>
          <Textarea
            value={item.value.description}
            onChange={(value) => (item.value.description = value)}
          >
            Описание
          </Textarea>
          <ImageBlock class="mb-4" image={item.value.image} removable />
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
