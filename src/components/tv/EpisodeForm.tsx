import { defineComponent, onMounted, PropType, ref, withModifiers } from "vue";
import Loading from "../Loading";
import api from "../../helpers/api";
import { error } from "../../helpers/notifications";
import emptyImage from "../ImageBlock/emptyImage";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import { SelectNumber } from "../form/Select";
import ImageBlock from "../ImageBlock";
import VideoBlock from "../ContentBlocks/VideoBlock";
import { ImageType } from "@/helpers/types";
import { TvShowType } from "./ShowForm";
import { VideoBlockType } from "@/helpers/types";

export interface TvEpisodeType {
  id: number;
  television_show_id: number;
  title: string;
  description: string;
  image: ImageType;
  video: VideoBlockType;
}

export default defineComponent({
  name: "ShowForm",
  props: {
    id: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(value: TvEpisodeType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const shows = ref<TvShowType[]>([]);
    const item = ref<TvEpisodeType>({
      id: 0,
      television_show_id: 0,
      title: "",
      description: "",
      image: { ...emptyImage },
      video: {
        id: 0,
        position: 0,
        kind: "video",
        code: "",
        video: { ...emptyImage },
        image: { ...emptyImage },
      },
    });

    onMounted(async () => {
      const { data } = await api.get<TvShowType[]>("television/shows");
      shows.value = data;
      item.value.television_show_id = shows.value[0]?.id;
      if (props.id) {
        const { data } = await api.get<TvEpisodeType>(
          `television/episodes/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `television/episodes/${item.value.id}`
          : "television/episodes";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<TvEpisodeType>(url, {
          television_episode: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Эпизод</h1>
          <SelectNumber
            value={item.value.television_show_id}
            options={shows.value}
            onChange={(value) => (item.value.television_show_id = value)}
          >
            Программа
          </SelectNumber>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
          >
            Название
          </Input>
          <Textarea
            value={item.value.description}
            onChange={(value) => (item.value.description = value)}
          >
            Описание
          </Textarea>
          <ImageBlock class="mb-4" image={item.value.image} removable />
          <VideoBlock class="mb-4" item={item.value.video} />
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
