import { defineComponent, onMounted, PropType, ref, withModifiers } from "vue";
import Loading from "../Loading";
import api from "../../helpers/api";
import { error } from "../../helpers/notifications";
import emptyImage from "../ImageBlock/emptyImage";
import { useStore } from "../../store";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import { SelectNumber } from "../form/Select";
import Checkbox from "../form/Checkbox";
import ImageBlock from "../ImageBlock";
import { ImageType } from "@/helpers/types";

export interface TvShowType {
  id: number;
  platform_id: number;
  title: string;
  description: string;
  is_active: boolean;
  image: ImageType;
}

export default defineComponent({
  name: "ShowForm",
  props: {
    id: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(item: TvShowType) => void>,
      required: true,
    },
  },
  setup(props) {
    const store = useStore();
    const platforms = store.state.platforms;
    const loaded = ref(false);
    const item = ref<TvShowType>({
      id: 0,
      platform_id: platforms[0].id,
      title: "",
      description: "",
      is_active: false,
      image: { ...emptyImage },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<TvShowType>(
          `television/shows/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `television/shows/${item.value.id}`
          : "television/shows";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<TvShowType>(url, {
          television_show: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Программа</h1>
          <SelectNumber
            value={item.value.platform_id}
            options={platforms}
            onChange={(value) => (item.value.platform_id = value)}
          >
            Издание
          </SelectNumber>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
          >
            Заголовок
          </Input>
          <Textarea
            value={item.value.description}
            onChange={(value) => (item.value.description = value)}
          >
            Описание
          </Textarea>
          <Checkbox
            value={item.value.is_active}
            onChange={(value) => (item.value.is_active = value)}
          >
            Программа активна
          </Checkbox>
          <ImageBlock class="mb-4" image={item.value.image} removable />
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
