/* eslint-disable vue/no-mutating-props */
import { defineComponent, ref, PropType, Transition } from "vue";
import MenuSectionForm, { MenuSectionType } from "./MenuSectionForm";
import MenuSection from "./MenuSection";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Modal from "../Modal";

export default defineComponent({
  name: "MenuSections",
  props: {
    platformId: { type: Number, required: true },
    sections: { type: Array as PropType<MenuSectionType[]>, required: true },
  },
  setup(props) {
    const sectionModalVisible = ref(false);

    async function setPositions() {
      props.sections.forEach((el, i) => (el.position = i));
      try {
        const positions = props.sections.map((el) => ({
          id: el.id,
          position: el.position,
        }));
        await api.patch("menu_sections/positions", { positions });
      } catch (e) {
        error(e);
      }
    }

    function moveSection(item: MenuSectionType, inc: number) {
      const index = props.sections.indexOf(item);
      // Крайние положения
      if (index === props.sections.length - 1 && inc > 0) return;
      if (index === 0 && inc < 0) return;
      props.sections.splice(index, 1);
      props.sections.splice(index + inc, 0, item);
      void setPositions();
    }

    return () => (
      <div>
        <Transition name="fade-up">
          {sectionModalVisible.value && (
            <Modal onClose={() => (sectionModalVisible.value = false)}>
              <MenuSectionForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  props.sections.push(item);
                  sectionModalVisible.value = false;
                  void setPositions();
                }}
              />
            </Modal>
          )}
        </Transition>
        {props.sections.map((el, i) => (
          <MenuSection
            item={el}
            key={i}
            platformId={props.platformId}
            onUpdate={(item) => props.sections.splice(i, 1, item)}
            onRemove={() => {
              props.sections.splice(i, 1);
              void setPositions();
            }}
            onUp={() => moveSection(el, -1)}
            onDown={() => moveSection(el, 1)}
          />
        ))}
        <button
          type="button"
          class="btn my-6"
          onClick={() => (sectionModalVisible.value = true)}
        >
          Добавить раздел
        </button>
      </div>
    );
  },
});
