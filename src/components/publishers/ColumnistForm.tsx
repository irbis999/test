import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import compressImg from "@/helpers/compressImg";

export interface ColumnistType {
  id: number;
  platform_id: number;
  full_name: string;
  occupation: string;
  image_url: string;
  image?: File;
  matter_count?: number;
}

export default defineComponent({
  name: "ColumnistForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(item: ColumnistType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<ColumnistType>({
      id: 0,
      platform_id: props.platformId,
      full_name: "",
      occupation: "",
      image_url: "",
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<ColumnistType>(`columnists/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `columnists/${item.value.id}`
          : "columnists";
        const method = item.value.id ? "patch" : "post";
        const formData = new FormData();
        if (props.platformId)
          formData.append(
            "columnist[platform_id]",
            props.platformId.toString()
          );
        formData.append("columnist[full_name]", item.value.full_name);
        formData.append("columnist[occupation]", item.value.occupation);
        if (item.value.image)
          formData.append("columnist[image]", item.value.image);
        const { data } = await api[method]<ColumnistType>(url, formData);
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Эксперт</h1>
          <Input
            value={item.value.full_name}
            onChange={(value) => (item.value.full_name = value)}
            required
            placeholder="ФИО"
          >
            ФИО
          </Input>
          <Input
            value={item.value.occupation}
            onChange={(value) => (item.value.occupation = value)}
            required
            placeholder="Род деятельности"
          >
            Род деятельности
          </Input>
          <div class="mb-6">
            <div class="f-label">Фото</div>
            {item.value.image_url && (
              <img
                class="mb-4"
                style={{ maxWidth: "300px" }}
                src={item.value.image_url}
                alt="Фото колумниста"
              />
            )}
            <input
              type="file"
              accept="image/jpeg, image/png"
              onChange={async (e) => {
                const files = (e.target as HTMLInputElement).files;
                if (files?.length)
                  item.value.image = await compressImg(files[0]);
              }}
            />
          </div>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
