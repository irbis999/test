import { defineComponent, PropType, ref, Transition } from "vue";
import MenuItemForm, { MenuItemType } from "./MenuItemForm";
import Modal from "../Modal";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";

export default defineComponent({
  name: "MenuItem",
  props: {
    item: { type: Object as PropType<MenuItemType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: MenuItemType) => void>,
      required: true,
    },
    onRemove: { type: Function as PropType<() => void>, default: () => {} },
    onUp: { type: Function as PropType<() => void>, default: () => {} },
    onDown: { type: Function as PropType<() => void>, default: () => {} },
  },
  setup(props) {
    const modalVisible = ref(false);

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление пункта меню")) return;
      busy = true;
      try {
        await api.delete(`menu_items/${props.item.id}`);
        props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <MenuItemForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <button type="button" onClick={() => (modalVisible.value = true)}>
          {props.item.title}
        </button>
        <button type="button" class="btn ml-auto m-icon" onClick={props.onUp}>
          arrow_upward
        </button>
        <button type="button" class="btn m-icon" onClick={props.onDown}>
          arrow_downward
        </button>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
