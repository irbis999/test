import { defineComponent, PropType, computed } from "vue";
import { RouterLink } from "vue-router";
import { StaticPageType } from "./StaticPageForm";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { useStore } from "@/store";

export default defineComponent({
  name: "SpecialProjectItem",
  props: {
    item: { type: Object as PropType<StaticPageType>, required: true },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const store = useStore();
    const siteUrl = computed(() => {
      const platform = store.state.platforms.find(
        (el) => el.id === props.item.platform_id
      );
      return `${platform?.hostname}/pages/${props.item.slug}`;
    });

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление статической страницы")) return;
      busy = true;
      try {
        await api.delete(`static_pages/${props.item.id}`);
        if (props.onRemove) props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <RouterLink to={`/publishers/static-pages/${props.item.id}`}>
          {props.item.title}
        </RouterLink>
        <a class="btn ml-auto m-icon" href={siteUrl.value} target="_blank">
          open_in_new
        </a>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
