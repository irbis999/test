import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import MetaInformation, { metaInformation } from "@/components/MetaInformation";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import { StoryType } from "@/helpers/types";
import emptyImage from "../ImageBlock/emptyImage";
import ImageBlock from "../ImageBlock";

export default defineComponent({
  name: "StoryForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(item: StoryType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<StoryType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      slug: "",
      lead: "",
      main_page_title: "",
      text: "",
      image: { ...emptyImage },
      meta_information_attributes: { ...metaInformation },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<StoryType>(`stories/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id ? `stories/${item.value.id}` : "stories";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<StoryType>(url, {
          story: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Сюжет</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <Input
            value={item.value.slug}
            onChange={(value) => (item.value.slug = value)}
            required
            placeholder="Слаг"
          >
            Слаг
          </Input>
          <Input
            value={item.value.lead}
            onChange={(value) => (item.value.lead = value)}
            required
            placeholder="Лид"
          >
            Лид
          </Input>
          <Input
            value={item.value.main_page_title}
            onChange={(value) => (item.value.main_page_title = value)}
            required
            placeholder="Заголовок для главной"
          >
            Заголовок для главной
          </Input>
          <Input
            value={item.value.text}
            onChange={(value) => (item.value.text = value)}
            required
            placeholder="Текст"
          >
            Текст
          </Input>
          <div class="f-label">Заходное фото</div>
          <ImageBlock class="mb-4" image={item.value.image} removable />
          <MetaInformation item={item.value}></MetaInformation>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
