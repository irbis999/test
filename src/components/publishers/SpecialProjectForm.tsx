import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import Checkbox from "../form/Checkbox";
import emptyImage from "../ImageBlock/emptyImage";
import { ImageType } from "@/helpers/types";
import ImageBlock from "../ImageBlock";

export interface SpecialProjectType {
  id: number;
  platform_id: number;
  title: string;
  url: string;
  is_active: boolean;
  image: ImageType;
}

export default defineComponent({
  name: "SpecialProjectForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: SpecialProjectType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<SpecialProjectType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      url: "",
      is_active: true,
      image: { ...emptyImage },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<SpecialProjectType>(
          `special_projects/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `special_projects/${item.value.id}`
          : "special_projects";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<SpecialProjectType>(url, {
          special_project: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Спецпроект</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <Input
            value={item.value.url}
            type="url"
            onChange={(value) => (item.value.url = value)}
            required
            placeholder="URL"
          >
            URL
          </Input>
          <Checkbox
            value={item.value.is_active}
            onChange={(value) => (item.value.is_active = value)}
          >
            Спецпроект активен
          </Checkbox>
          <div class="f-label">Изображение</div>
          <ImageBlock class="mb-4" image={item.value.image} removable />
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
