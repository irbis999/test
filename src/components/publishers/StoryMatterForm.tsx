import { MatterType as MType, StoryType } from "@/helpers/types";
import { defineComponent, PropType, ref, withModifiers } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import api from "@/helpers/api";
import { flash, error } from "@/helpers/notifications";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";

interface MatterType extends MType {
  is_main: boolean;
}

export default defineComponent({
  props: {
    story: { type: Object as PropType<StoryType>, required: true },
    onAdd: {
      type: Function as PropType<(item: MatterType) => void>,
      required: true,
    },
  },
  setup(props) {
    const apiParams = ref({
      platform_id: [props.story.platform_id],
      q: "",
    });
    const { items, fetch, busy, pending } = useInfScroll<MatterType>(
      "matters",
      apiParams
    );

    async function addToStory(item: MatterType) {
      try {
        await api.post(`stories/${props.story.id}/matters`, { id: item.id });
        item.is_main = false;
        flash("Материал добавлен в сюжет");
        props.onAdd(item);
      } catch (e) {
        error(e);
      }
    }

    return () => (
      <div>
        <h1>Добавление материала в сюжет</h1>
        <form onSubmit={withModifiers(() => fetch(true), ["prevent"])}>
          <input
            class="f-input"
            inputmode="search"
            placeholder="Поиск"
            onInput={(e) =>
              (apiParams.value.q = (e.target as HTMLInputElement).value)
            }
          />
          <div
            class="border overflow-auto mt-4 empty:hidden"
            style={{ maxHeight: "60vh", minHeight: "30px" }}
          >
            {items.value.map((el, i) => (
              <div key={i} class="px-4 py-2 flex justify-between items-center">
                <span>{el.title}</span>
                <button
                  type="button"
                  class="btn"
                  onClick={() => addToStory(el)}
                >
                  Добавить в сюжет
                </button>
              </div>
            ))}
            {pending.value && <Loading />}
            <InfScroll onLoad={() => fetch()} busy={busy.value} parent />
          </div>
        </form>
      </div>
    );
  },
});
