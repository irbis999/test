import { defineComponent, PropType, ref, Transition } from "vue";
import TeaserForm, { TeaserType } from "./TeaserForm";
import Modal from "../Modal";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";

export default defineComponent({
  name: "TeaserItem",
  props: {
    item: { type: Object as PropType<TeaserType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: TeaserType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление тизерного блока")) return;
      busy = true;
      try {
        await api.delete(`teaser_blocks/${props.item.id}`);
        if (props.onRemove) props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <TeaserForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <button type="button" onClick={() => (modalVisible.value = true)}>
          {props.item.title}
        </button>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
