import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import Checkbox from "../form/Checkbox";

export interface ToplineItemType {
  id: number;
  platform_id: number;
  title: string;
  url: string;
  is_active: boolean;
  in_new_tab: boolean;
}

export default defineComponent({
  name: "ToplineItemForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: ToplineItemType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<ToplineItemType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      url: "",
      is_active: true,
      in_new_tab: false,
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<ToplineItemType>(
          `topline_items/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `topline_items/${item.value.id}`
          : "topline_items";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<ToplineItemType>(url, {
          topline_item: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Пункт топлайн</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <Input
            value={item.value.url}
            onChange={(value) => (item.value.url = value)}
            required
            placeholder="Ссылка"
          >
            Ссылка
          </Input>
          <Checkbox
            value={item.value.in_new_tab}
            onChange={(value) => (item.value.in_new_tab = value)}
          >
            Открывать в новой вкладке
          </Checkbox>
          <Checkbox
            value={item.value.is_active}
            onChange={(value) => (item.value.is_active = value)}
          >
            Активен
          </Checkbox>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
