import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import compressImg from "@/helpers/compressImg";
import Checkbox from "../form/Checkbox";

export interface TeaserType {
  id: number;
  platform_id: number;
  title: string;
  title_first: string;
  url_first: string;
  image_first_url?: string;
  image_first?: File;
  title_second: string;
  url_second: string;
  image_second_url?: string;
  image_second?: File;
  title_third: string;
  url_third: string;
  image_third_url?: string;
  image_third?: File;
  is_shown_on_desktop: boolean;
  is_shown_on_mobile: boolean;
}

export default defineComponent({
  name: "TeaserForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: TeaserType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<TeaserType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      title_first: "",
      url_first: "",
      title_second: "",
      url_second: "",
      title_third: "",
      url_third: "",
      is_shown_on_desktop: true,
      is_shown_on_mobile: true,
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<TeaserType>(`teaser_blocks/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `teaser_blocks/${item.value.id}`
          : "teaser_blocks";
        const method = item.value.id ? "patch" : "post";
        const formData = new FormData();
        if (item.value.platform_id) {
          formData.append(
            "teaser_block[platform_id]",
            `${item.value.platform_id}`
          );
        } else {
          formData.append("teaser_block[is_global]", "1");
        }
        formData.append("teaser_block[title]", `${item.value.title}`);
        formData.append(
          "teaser_block[title_first]",
          `${item.value.title_first}`
        );
        formData.append(
          "teaser_block[title_second]",
          `${item.value.title_second}`
        );
        formData.append(
          "teaser_block[title_third]",
          `${item.value.title_third}`
        );
        formData.append("teaser_block[url_first]", `${item.value.url_first}`);
        formData.append("teaser_block[url_second]", `${item.value.url_second}`);
        formData.append("teaser_block[url_third]", `${item.value.url_third}`);
        formData.append(
          "teaser_block[is_shown_on_desktop]",
          `${item.value.is_shown_on_desktop}`
        );
        formData.append(
          "teaser_block[is_shown_on_mobile]",
          `${item.value.is_shown_on_mobile}`
        );
        if (item.value.image_first) {
          formData.append(
            "teaser_block[image_first]",
            `${item.value.image_first}`
          );
        }
        if (item.value.image_second) {
          formData.append(
            "teaser_block[image_second]",
            `${item.value.image_second}`
          );
        }
        if (item.value.image_third) {
          formData.append(
            "teaser_block[image_third]",
            `${item.value.image_third}`
          );
        }
        const { data } = await api[method]<TeaserType>(url, formData);
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function setImage(
      e: Event,
      key: "image_first" | "image_second" | "image_third"
    ) {
      const files = (e.target as HTMLInputElement).files;
      if (!files?.length) return;
      item.value[key] = await compressImg(files[0], 400);
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Тизерный блок</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
          >
            Заголовок блока
          </Input>
          <Input
            value={item.value.title_first}
            onChange={(value) => (item.value.title_first = value)}
            required
          >
            Заголовок первого блока
          </Input>
          <Input
            value={item.value.url_first}
            onChange={(value) => (item.value.url_first = value)}
            required
            type="url"
          >
            Ссылка первого материала
          </Input>
          <div class="mb-6">
            <div class="f-label">Изображение первого материала</div>
            {item.value.image_first_url && (
              <img
                class="mb-4 max-w-40"
                src={item.value.image_first_url}
                alt="Изображение материала"
              />
            )}
            <input
              type="file"
              accept="image/jpeg, image/png"
              onChange={(e) => setImage(e, "image_first")}
            />
          </div>
          <Input
            value={item.value.title_second}
            onChange={(value) => (item.value.title_second = value)}
            required
          >
            Заголовок второго блока
          </Input>
          <Input
            value={item.value.url_second}
            onChange={(value) => (item.value.url_second = value)}
            required
            type="url"
          >
            Ссылка второго материала
          </Input>
          <div class="mb-6">
            <div class="f-label">Изображение второго материала</div>
            {item.value.image_first_url && (
              <img
                class="mb-4 max-w-40"
                src={item.value.image_second_url}
                alt="Изображение материала"
              />
            )}
            <input
              type="file"
              accept="image/jpeg, image/png"
              onChange={(e) => setImage(e, "image_second")}
            />
          </div>
          <Input
            value={item.value.title_third}
            onChange={(value) => (item.value.title_third = value)}
            required
          >
            Заголовок третьего блока
          </Input>
          <Input
            value={item.value.url_third}
            onChange={(value) => (item.value.url_third = value)}
            required
            type="url"
          >
            Ссылка третьего материала
          </Input>
          <div class="mb-6">
            <div class="f-label">Изображение третьего материала</div>
            {item.value.image_third_url && (
              <img
                class="mb-4 max-w-40"
                src={item.value.image_third_url}
                alt="Изображение материала"
              />
            )}
            <input
              type="file"
              accept="image/jpeg, image/png"
              onChange={(e) => setImage(e, "image_third")}
            />
          </div>
          <Checkbox
            value={item.value.is_shown_on_desktop}
            onChange={(value) => (item.value.is_shown_on_desktop = value)}
          >
            Показывать на десктопе
          </Checkbox>
          <Checkbox
            value={item.value.is_shown_on_mobile}
            onChange={(value) => (item.value.is_shown_on_mobile = value)}
          >
            Показывать на мобайле
          </Checkbox>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
