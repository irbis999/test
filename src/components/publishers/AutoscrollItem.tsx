/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType, withModifiers } from "vue";
import { htmlDatetoFullDate } from "@/helpers/dateFormats";
import Text from "../form/Text";
import { SelectString } from "../form/Select";
import Input from "../form/Input";
import { DropdownNumber, DropdownString } from "../Dropdown";
import { useStore } from "@/store";
import MatterCell from "../MatterCell";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import styles from "../../styles/components/AutoscrollItem.module.scss";

export interface AutoscrollItemType {
  id: number;
  show_start_at: string;
  show_end_at: string;
  gender: string[];
  age: string[];
  geo: string[];
  period: string;
  views_count: number | null;
  matter_title: string;
  matter_id: number | null;
  position: number;
  platforms: number[];
}

const genderOptions = [
  { id: "male", title: "Мужской" },
  { id: "female", title: "Женский" },
];
const ageOptions = [
  { id: "less_18", title: "Меньше 18" },
  { id: "18_24", title: "18-24" },
  { id: "25_34", title: "25-34" },
  { id: "35_44", title: "35-44" },
  { id: "44_more", title: "Старше 45" },
];
const geoOptions = [{ id: "tambov", title: "Тамбов" }];
const periodOptions = [
  { id: "always", title: "Показывать всегда" },
  { id: "once_a_day", title: "Показывать один раз в сутки" },
];

export default defineComponent({
  props: {
    item: { type: Object as PropType<AutoscrollItemType>, required: true },
    platformId: { type: Number, default: 0 },
  },
  setup(props) {
    const store = useStore();

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        await api.patch(`autoscroll_configs/${props.item.id}`, {
          autoscroll_config: {
            ...props.item,
            show_start_at: htmlDatetoFullDate(props.item.show_start_at),
            show_end_at: htmlDatetoFullDate(props.item.show_end_at),
          },
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    function reset() {
      if (busy) return;
      props.item.show_start_at = "";
      props.item.show_end_at = "";
      props.item.gender = [];
      props.item.age = [];
      props.item.geo = [];
      props.item.period = "always";
      props.item.views_count = null;
      props.item.matter_title = "";
      props.item.matter_id = null;
      void send();
    }

    return () => (
      <form class={styles.item} onSubmit={withModifiers(send, ["prevent"])}>
        <div>
          <Text value={`${props.item.position + 1}`}>Позиция</Text>
          <SelectString
            value={props.item.period}
            options={periodOptions}
            onChange={(value) => (props.item.period = value)}
          >
            Период
          </SelectString>
          <Input
            type="datetime-local"
            value={props.item.show_start_at}
            onChange={(value) => (props.item.show_start_at = value)}
          >
            Начать показ в
          </Input>
          <Input
            type="datetime-local"
            value={props.item.show_end_at}
            onChange={(value) => (props.item.show_end_at = value)}
          >
            Завершить показ в
          </Input>
          <div class="f-label">Пол</div>
          <DropdownString
            class="mb-6"
            value={props.item.gender}
            options={genderOptions}
            onChange={(value) => (props.item.gender = value)}
          >
            Пол
          </DropdownString>
          <div class="f-label">Возраст</div>
          <DropdownString
            class="mb-6"
            value={props.item.age}
            options={ageOptions}
            onChange={(value) => (props.item.age = value)}
          >
            Возраст
          </DropdownString>
          <div class="f-label">Гео</div>
          <DropdownString
            class="mb-6"
            value={props.item.geo}
            options={geoOptions}
            onChange={(value) => (props.item.geo = value)}
          >
            Гео
          </DropdownString>
          {!props.platformId && (
            <div>
              <div class="f-label">Показывать на изданиях</div>
              <DropdownNumber
                class="mb-6"
                value={props.item.platforms}
                options={store.state.platforms}
                onChange={(value) => (props.item.platforms = value)}
              >
                Показывать на изданиях
              </DropdownNumber>
            </div>
          )}
        </div>
        <div>
          <div class="f-label">Материал</div>
          <MatterCell
            class="mb-6"
            platformId={props.platformId}
            item={props.item}
            type={store.state.autoscrollAllowedTypes}
            dropErrorType="Новость, Статья, Мнение, Видео, Фотогалерея, Подкаст, Онлайн-трансляция"
          />
          <Text value={props.item.views_count?.toString()}>
            Кол-во просмотров
          </Text>
        </div>
        <div>
          <button class="btn green">Сохранить</button>
          <button class="btn red" type="button" onClick={reset}>
            Сбросить
          </button>
        </div>
      </form>
    );
  },
});
