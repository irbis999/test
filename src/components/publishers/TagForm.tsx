import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import { MetaInformationType } from "@/helpers/types";
import MetaInformation, { metaInformation } from "@/components/MetaInformation";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";

export interface TagType {
  id: number;
  platform_id: number;
  title: string;
  slug: string;
  meta_information_attributes: MetaInformationType;
}

export default defineComponent({
  name: "TagForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: TagType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<TagType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      slug: "",
      meta_information_attributes: { ...metaInformation },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<TagType>(`tags/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id ? `tags/${item.value.id}` : "tags";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<TagType>(url, {
          tag: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Тег</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <Input
            value={item.value.slug}
            onChange={(value) => (item.value.slug = value)}
            required
            placeholder="Слаг"
          >
            Слаг
          </Input>
          <MetaInformation item={item.value}></MetaInformation>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
