/* eslint-disable vue/no-mutating-props */
import { defineComponent, Transition, ref, PropType } from "vue";
import MenuItemForm, { MenuItemType } from "./MenuItemForm";
import MenuSectionForm, { MenuSectionType } from "./MenuSectionForm";
import MenuItem from "./MenuItem";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Modal from "../Modal";

export default defineComponent({
  name: "MenuSection",
  props: {
    platformId: { type: Number, required: true },
    item: { type: Object as PropType<MenuSectionType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: MenuSectionType) => void>,
      required: true,
    },
    onRemove: { type: Function as PropType<() => void>, default: () => {} },
    onUp: { type: Function as PropType<() => void>, default: () => {} },
    onDown: { type: Function as PropType<() => void>, default: () => {} },
  },
  setup(props) {
    const sectionModalVisible = ref(false);
    const menuItemModalVisible = ref(false);

    async function setPositions() {
      props.item.menu_items.forEach((el, i) => (el.position = i));
      try {
        const positions = props.item.menu_items.map((el) => ({
          id: el.id,
          position: el.position,
        }));
        await api.patch("menu_items/positions", { positions });
      } catch (e) {
        error(e);
      }
    }

    let busy = false;
    async function removeSection() {
      if (busy || !confirm("Подтвердите удаление раздела")) return;
      busy = true;
      try {
        await api.delete(`menu_sections/${props.item.id}`);
        props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    function moveMenuItem(item: MenuItemType, inc: number) {
      const index = props.item.menu_items.indexOf(item);
      // Крайние положения
      if (index === props.item.menu_items.length - 1 && inc > 0) return;
      if (index === 0 && inc < 0) return;
      props.item.menu_items.splice(index, 1);
      props.item.menu_items.splice(index + inc, 0, item);
      void setPositions();
    }

    return () => (
      <div class="mb-4">
        <Transition name="fade-up">
          {sectionModalVisible.value && (
            <Modal onClose={() => (sectionModalVisible.value = false)}>
              <MenuSectionForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  sectionModalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <Transition name="fade-up">
          {menuItemModalVisible.value && (
            <Modal onClose={() => (menuItemModalVisible.value = false)}>
              <MenuItemForm
                sectionId={props.item.id}
                platformId={props.platformId}
                onUpdate={(item) => {
                  props.item.menu_items.push(item);
                  menuItemModalVisible.value = false;
                  void setPositions();
                }}
              />
            </Modal>
          )}
        </Transition>
        <details class="mb-0">
          <summary class="mb-0">
            <span>{props.item.title}</span>
          </summary>
          <div class="ml-6">
            {props.item.menu_items.map((el, i) => (
              <MenuItem
                key={i}
                item={el}
                onRemove={() => {
                  props.item.menu_items.splice(i, 1);
                  void setPositions();
                }}
                onUpdate={(item) => props.item.menu_items.splice(i, 1, item)}
                onUp={() => moveMenuItem(el, -1)}
                onDown={() => moveMenuItem(el, 1)}
              />
            ))}
            <div class="flex my-6">
              <button
                type="button"
                class="btn"
                onClick={() => (menuItemModalVisible.value = true)}
              >
                Добавить пункт раздела
              </button>
              <button
                type="button"
                class="btn blue"
                onClick={() => (sectionModalVisible.value = true)}
              >
                Редактировать раздел
              </button>
              <button type="button" class="btn red" onClick={removeSection}>
                Удалить раздел
              </button>
              <button
                type="button"
                class="btn ml-auto m-icon"
                onClick={props.onUp}
              >
                arrow_upward
              </button>
              <button type="button" class="btn m-icon" onClick={props.onDown}>
                arrow_downward
              </button>
            </div>
          </div>
        </details>
      </div>
    );
  },
});
