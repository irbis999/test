import { defineComponent, PropType, ref, Transition, computed } from "vue";
import StoryForm from "./StoryForm";
import Modal from "../Modal";
import api from "@/helpers/api";
import { StoryType } from "@/helpers/types";
import { error } from "@/helpers/notifications";
import { useStore } from "@/store";
import { RouterLink } from "vue-router";

export default defineComponent({
  name: "TagItem",
  props: {
    item: { type: Object as PropType<StoryType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: StoryType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);
    const store = useStore();
    const siteUrl = computed(() => {
      const platform = store.state.platforms.find(
        (el) => el.id === props.item.platform_id
      );
      return `${platform?.hostname}/story/${props.item.slug}`;
    });

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление сюжета")) return;
      busy = true;
      try {
        await api.delete(`stories/${props.item.id}`);
        if (props.onRemove) props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <StoryForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <RouterLink to={`/publishers/stories/${props.item.id}`}>
          {props.item.title}
        </RouterLink>
        <a class="btn ml-auto m-icon" href={siteUrl.value} target="_blank">
          open_in_new
        </a>
        <button
          class="btn m-icon"
          type="button"
          onClick={() => (modalVisible.value = true)}
        >
          edit
        </button>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
