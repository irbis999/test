import { defineComponent, PropType, ref, Transition, computed } from "vue";
import Form, { TagType } from "./TagForm";
import Modal from "../Modal";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { useStore } from "@/store";

export default defineComponent({
  name: "TagItem",
  props: {
    item: { type: Object as PropType<TagType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: TagType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);
    const store = useStore();
    const siteUrl = computed(() => {
      const platform = store.state.platforms.find(
        (el) => el.id === props.item.platform_id
      );
      return `${platform?.hostname}/tags/${props.item.slug}`;
    });

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление тега")) return;
      busy = true;
      try {
        await api.delete(`tags/${props.item.id}`);
        if (props.onRemove) props.onRemove();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <Form
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <button type="button" onClick={() => (modalVisible.value = true)}>
          {props.item.title}
        </button>
        <a class="btn ml-auto m-icon" href={siteUrl.value} target="_blank">
          open_in_new
        </a>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
