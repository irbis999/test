import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import { SelectString } from "../form/Select";

export interface BannerType {
  id: number;
  platform_id: number;
  slug: string;
  description: string;
  code: string;
  provider: string;
  visibility: string;
}

export default defineComponent({
  name: "BannerForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: BannerType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<BannerType>({
      id: 0,
      platform_id: props.platformId,
      slug: "",
      description: "",
      code: "",
      provider: "html",
      visibility: "both",
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<BannerType>(`banners/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id ? `banners/${item.value.id}` : "banners";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<BannerType>(url, {
          banner: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    const visibilityOptions = [
      { id: "both", title: "Везде" },
      { id: "mobile", title: "Мобайл" },
      { id: "desktop", title: "Десктоп" },
    ];

    const typeOptions = [
      { id: "html", title: "HTML" },
      { id: "adfox", title: "AdFox" },
    ];

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Баннер</h1>
          <Input
            value={item.value.slug}
            onChange={(value) => (item.value.slug = value)}
            required
            placeholder="Слаг"
          >
            Слаг
          </Input>
          <Input
            value={item.value.description}
            onChange={(value) => (item.value.description = value)}
            required
            placeholder="Описание"
          >
            Описание
          </Input>
          <Textarea
            value={item.value.code}
            onChange={(value) => (item.value.code = value)}
            required
            placeholder="Код баннера"
          >
            Код баннера
          </Textarea>
          <SelectString
            value={item.value.visibility}
            onChange={(value) => (item.value.visibility = value)}
            options={visibilityOptions}
          >
            Показывать
          </SelectString>
          <SelectString
            value={item.value.provider}
            onChange={(value) => (item.value.provider = value)}
            options={typeOptions}
          >
            Тип баннера
          </SelectString>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
