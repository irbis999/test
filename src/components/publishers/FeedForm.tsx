import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import { DropdownString, DropdownNumber } from "../Dropdown";
import { useStore } from "@/store";
import { RubricType } from "./RubricForm";
import { StoryType } from "@/helpers/types";

export interface ItemType {
  id?: number;
  platform_id: number;
  title: string;
  filters: {
    matter_types: string[];
    rubrics: number[];
    stories: number[];
  };
}

export default defineComponent({
  name: "FeedForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: ItemType) => void>,
      required: true,
    },
  },
  setup(props) {
    const store = useStore();
    const loaded = ref(false);
    const rubrics = ref<RubricType[]>([]);
    const stories = ref<StoryType[]>([]);
    const item = ref<ItemType>({
      platform_id: props.platformId,
      title: "",
      filters: {
        matter_types: [],
        rubrics: [],
        stories: [],
      },
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<ItemType>(`news_feeds/${props.id}`);
        item.value = data;
      }
      const params = {
        platform_id: props.platformId || item.value.platform_id,
        limit: 150,
      };
      const { data: r } = await api.get<RubricType[]>("rubrics", { params });
      const { data: s } = await api.get<StoryType[]>("stories", { params });
      rubrics.value = r;
      stories.value = s;
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `news_feeds/${item.value.id}`
          : "news_feeds";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<ItemType>(url, {
          news_feed: item.value,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Вкладка ленты новостей</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <div class="f-label">Типы материалов</div>
          <DropdownString
            class="mb-6"
            value={item.value.filters.matter_types}
            options={store.state.matter_types}
            onChange={(value) => (item.value.filters.matter_types = value)}
          >
            Типы материалов
          </DropdownString>
          <div class="f-label">Рубрики</div>
          <DropdownNumber
            class="mb-6"
            value={item.value.filters.rubrics}
            options={rubrics.value}
            onChange={(value) => (item.value.filters.rubrics = value)}
          >
            Рубрики
          </DropdownNumber>
          <div class="f-label">Сюжеты</div>
          <DropdownNumber
            class="mb-6"
            value={item.value.filters.stories}
            options={stories.value}
            onChange={(value) => (item.value.filters.stories = value)}
          >
            Сюжеты
          </DropdownNumber>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
