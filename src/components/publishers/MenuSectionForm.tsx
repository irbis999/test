import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import { MenuItemType } from "./MenuItemForm";

export interface MenuSectionType {
  id: number;
  platform_id: number;
  title: string;
  menu_items: MenuItemType[];
  position?: number;
  section_type?: "main" | "footer";
}

export default defineComponent({
  name: "TagForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: MenuSectionType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<MenuSectionType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      menu_items: [],
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<MenuSectionType>(
          `menu_sections/${props.id}`
        );
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `menu_sections/${item.value.id}`
          : "menu_sections";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<MenuSectionType>(url, {
          menu_section: { ...item.value, section_type: "main" },
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Раздел меню</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Название раздела"
          >
            Название раздела
          </Input>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
