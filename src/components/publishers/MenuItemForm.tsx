import { defineComponent, ref, onMounted, PropType, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import Checkbox from "../form/Checkbox";

export interface MenuItemType {
  id: number;
  platform_id: number;
  menu_section_id: number;
  title: string;
  url: string;
  is_active: boolean;
  position?: number;
}

export default defineComponent({
  name: "MenuItemForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    sectionId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: MenuItemType) => void>,
      required: true,
    },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref<MenuItemType>({
      id: 0,
      platform_id: props.platformId,
      menu_section_id: props.sectionId,
      title: "",
      url: "",
      is_active: true,
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<MenuItemType>(`menu_items/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `menu_items/${item.value.id}`
          : "menu_items";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<MenuItemType>(url, {
          menu_item: { ...item.value, menu: "top" },
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Пункт меню</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Заголовок"
          >
            Заголовок
          </Input>
          <Input
            value={item.value.url}
            onChange={(value) => (item.value.url = value)}
            required
            placeholder="Ссылка"
          >
            Ссылка
          </Input>
          <Checkbox
            value={item.value.is_active}
            onChange={(value) => (item.value.is_active = value)}
          >
            Активность
          </Checkbox>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
