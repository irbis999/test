import { defineComponent, ref, withModifiers } from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Input from "../form/Input";
import { useRouter } from "vue-router";

export interface StaticPageType {
  id: number;
  platform_id: number;
  title: string;
  slug: string;
}

export default defineComponent({
  name: "RubricForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
  },
  setup(props) {
    const router = useRouter();
    const item = ref<StaticPageType>({
      id: 0,
      platform_id: props.platformId,
      title: "",
      slug: "",
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id
          ? `static_pages/${item.value.id}`
          : "static_pages";
        const method = item.value.id ? "patch" : "post";
        const { data } = await api[method]<StaticPageType>(url, {
          static_page: item.value,
        });
        await router.push(`/publishers/static-pages/${data.id}`);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <form onSubmit={withModifiers(send, ["prevent"])}>
        <h1>Статическая страница</h1>
        <Input
          value={item.value.title}
          onChange={(value) => (item.value.title = value)}
          required
          placeholder="Заголовок"
        >
          Заголовок
        </Input>
        <Input
          value={item.value.slug}
          onChange={(value) => (item.value.slug = value)}
          required
          placeholder="Слаг"
        >
          Слаг
        </Input>
        <button class="btn green">Сохранить</button>
      </form>
    );
  },
});
