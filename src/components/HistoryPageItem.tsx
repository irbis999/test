import { defineComponent, PropType } from "vue";
import date from "@/helpers/date";

export interface ItemType {
  text: string;
  created_at: string;
}

export default defineComponent({
  name: "HistoryPageItem",
  props: {
    item: { type: Object as PropType<ItemType>, required: true },
  },
  setup(props) {
    return () => (
      <div class="list-item">
        <span v-html={props.item.text}></span>
        <span>{date(props.item.created_at)}</span>
      </div>
    );
  },
});
