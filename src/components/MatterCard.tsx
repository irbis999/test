import { defineComponent, PropType, computed } from "vue";
import { MatterType, MatterUsersType } from "@/helpers/types";
import { useStore } from "@/store";
import { useRouter } from "vue-router";
import styles from "../styles/components/MatterCard.module.scss";
import date from "@/helpers/date";

export default defineComponent({
  name: "MatterCard",
  props: {
    matter: {
      type: Object as PropType<MatterType>,
      required: true,
    },
    mattersUsers: {
      type: Array as PropType<MatterUsersType>,
      default: () => [],
    },
  },
  setup(props) {
    const router = useRouter();
    // Object will not loose reactivity
    // eslint-disable-next-line vue/no-setup-props-destructure
    const matter = props.matter;
    const store = useStore();
    const type = computed(() => {
      const type = store.state.matter_types.find((el) => el.id === matter.type);
      return type ? type.title : "";
    });
    const users = computed(() => {
      const el = props.mattersUsers.find((el) => el.id === matter.id);
      return el ? el.users : "";
    });

    return () => (
      <div class="border-b p-3 relative">
        <div class="absolute text-xs right-1">{matter.id}</div>
        <a
          class="mr-12 block mb-2"
          // Using A instead of RouterLink because of draggable attribute
          draggable="false"
          href={`/matters/${matter.id}`}
          onClick={(e) => {
            e.preventDefault();
            void router.push(`/matters/${matter.id}`);
          }}
        >
          {matter.title}
        </a>
        <div class="text-sm mb-1">{matter.editor_name}</div>
        <div class={styles.meta}>
          <span>{matter.platform.title}</span>
          <span>{type}</span>
          {matter.rubric_title && <span>{matter.rubric_title}</span>}
          <span>{matter.chars_count}</span>
          <span>{matter.images_count}</span>
          <span class={styles.counter}>
            <span class="m-icon">visibility</span>
            <span>{matter.counters.views}</span>
          </span>
          <span class={styles.counter}>
            <span class="m-icon">person</span>
            <span>{matter.counters.uniques}</span>
          </span>
          {users.value && (
            <div class={styles.users}>
              <div class="m-icon">edit</div>
              <span>{users}</span>
            </div>
          )}
          <div class={styles.bottom}>
            <div class="flex items-center">
              <div class="m-icon">add_circle_outline</div>
              <div class="ml-1">{date(matter.published_at)}</div>
            </div>
            <div class="flex items-center">
              <div class="m-icon">update</div>
              <div class="ml-1">{date(matter.updated_at)}</div>
            </div>
            {matter.is_locked && <div class="m-icon">lock</div>}
          </div>
        </div>
      </div>
    );
  },
});
