/* eslint-disable vue/no-mutating-props */
import { MatterType } from "@/helpers/types";
import { defineComponent, PropType } from "vue";
import Checkbox from "../form/Checkbox";
import Input from "../form/Input";
import Tiny from "../Tiny";

export default defineComponent({
  name: "Persona",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    return () => (
      <div>
        <Input
          value={props.matter.last_name}
          onChange={(value) => (props.matter.last_name = value)}
          required
        >
          Фамилия
        </Input>
        <Input
          value={props.matter.first_name}
          onChange={(value) => (props.matter.first_name = value)}
          required
        >
          Имя
        </Input>
        <Input
          value={props.matter.patronymic}
          onChange={(value) => (props.matter.patronymic = value)}
          required
        >
          Отчество
        </Input>
        <Input
          value={props.matter.occupation}
          onChange={(value) => (props.matter.occupation = value)}
        >
          Род деятельности
        </Input>
        <div class="mb-4">
          <div class="f-label">Пол</div>
          <select class="f-input" v-model={props.matter.sex}>
            <option value="undefined">Не указан</option>
            <option value="male">Мужской</option>
            <option value="female">Женский</option>
          </select>
        </div>
        <Input
          value={props.matter.birth_date}
          onChange={(value) => (props.matter.birth_date = value)}
          type="date"
        >
          Дата рождения
        </Input>
        <Checkbox
          value={props.matter.show_only_year}
          onChange={(value) => (props.matter.show_only_year = value)}
        >
          Показывать только год рождения
        </Checkbox>
        <Input
          value={props.matter.birth_place}
          onChange={(value) => (props.matter.birth_place = value)}
        >
          Место рождения
        </Input>
        <Input
          value={props.matter.party}
          onChange={(value) => (props.matter.party = value)}
        >
          Политическая партия
        </Input>
        <Input
          value={props.matter.education}
          onChange={(value) => (props.matter.education = value)}
        >
          Образование
        </Input>
        <div class="mb-4">
          <div class="f-label">Семейное положение</div>
          <select class="f-input" v-model={props.matter.marital_status}>
            <option value="married">Женат / Замужем</option>
            <option value="not_married">Не женат / Не замужем</option>
            <option value="divorced">В разводе</option>
          </select>
        </div>
        <Input
          value={props.matter.children}
          onChange={(value) => (props.matter.children = value)}
        >
          Дети
        </Input>
        <Input
          value={props.matter.facebook}
          onChange={(value) => (props.matter.facebook = value)}
          type="url"
        >
          Facebook
        </Input>
        <Input
          value={props.matter.vkontakte}
          onChange={(value) => (props.matter.vkontakte = value)}
          type="url"
        >
          Вконтакте
        </Input>
        <Input
          value={props.matter.twitter}
          onChange={(value) => (props.matter.twitter = value)}
          type="url"
        >
          Twitter
        </Input>
        <Input
          value={props.matter.odnoklassniki}
          onChange={(value) => (props.matter.odnoklassniki = value)}
          type="url"
        >
          Одноклассники
        </Input>
        <Input
          value={props.matter.instagram}
          onChange={(value) => (props.matter.instagram = value)}
          type="url"
        >
          Instagram
        </Input>
        <Input
          value={props.matter.youtube}
          onChange={(value) => (props.matter.youtube = value)}
          type="url"
        >
          YouTube
        </Input>
        <Input
          value={props.matter.website}
          onChange={(value) => (props.matter.website = value)}
          type="url"
        >
          Личный сайт
        </Input>
        <Input
          value={props.matter.wikipedia}
          onChange={(value) => (props.matter.wikipedia = value)}
          type="url"
        >
          Wikipedia
        </Input>
        <Tiny
          class="mb-6"
          value={props.matter.resume}
          onChange={(value) => (props.matter.resume = value)}
        >
          Резюме
        </Tiny>
      </div>
    );
  },
});
