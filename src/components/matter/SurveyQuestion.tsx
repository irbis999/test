/* eslint-disable vue/no-mutating-props */
import { computed, defineComponent, PropType } from "vue";
import { TestAnswerType, TestQuestionType } from "@/helpers/types";
import Textarea from "../form/Textarea";
import ImageBlock from "../ImageBlock";
import Checkbox from "../form/Checkbox";

export default defineComponent({
  name: "SurveyQuestion",
  props: {
    index: { type: Number, default: 0 },
    item: { type: Object as PropType<TestQuestionType>, required: true },
    onRemove: { type: Function as PropType<() => void>, required: true },
  },
  setup(props) {
    const answers = computed(() =>
      props.item.answers.filter((el) => !el._destroy)
    );

    function removeAnswer(item: TestAnswerType) {
      const index = props.item.answers.findIndex((el) => el.id === item.id);
      item.id < 1
        ? props.item.answers.splice(index, 1)
        : (item._destroy = true);
    }

    return () => (
      <div class="my-6">
        <div class="f-label flex justify-between">
          <span>
            Вопрос {props.index + 1}
            <span class="text-red-600 ml-1">*</span>
          </span>
          <button type="button" class="remove" onClick={props.onRemove}>
            Удалить вопрос
          </button>
        </div>
        <input
          class="f-input mb-4"
          v-model={props.item.text}
          required
          placeholder="Вопрос"
        />
        <Textarea
          value={props.item.explanation}
          onChange={(value) => (props.item.explanation = value)}
        >
          Пояснение к ответу
        </Textarea>
        <ImageBlock image={props.item.image} />
        {answers.value.map((el, i) => (
          <div class="ml-6 my-4 border-b" key={el.id}>
            <div class="f-label flex justify-between">
              <span>
                Ответ {i + 1}
                <span class="text-red-600 ml-1">*</span>
              </span>
              <button
                class="remove"
                type="button"
                onClick={() => removeAnswer(el)}
              >
                Удалить ответ
              </button>
            </div>
            <input
              v-model={el.text}
              required
              placeholder="Ответ"
              class="f-input mb-4"
            />
            <Checkbox
              value={el.is_correct}
              onChange={(value) => (el.is_correct = value)}
            >
              Верный ответ
            </Checkbox>
          </div>
        ))}
        <button
          type="button"
          class="btn ml-4 mt-4"
          onClick={() => {
            props.item.answers.push({
              id: Math.random(),
              text: "",
              is_correct: false,
              _destroy: false,
            });
          }}
        >
          Добавить ответ
        </button>
      </div>
    );
  },
});
