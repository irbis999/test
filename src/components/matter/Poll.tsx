/* eslint-disable vue/no-mutating-props */
import { MatterType } from "@/helpers/types";
import { computed, defineComponent, PropType } from "vue";
import { error } from "@/helpers/notifications";
import Checkbox from "../form/Checkbox";

export default defineComponent({
  name: "Poll",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    function addPoll() {
      props.matter.poll = {
        id: Math.random(),
        _destroy: false,
        title: "",
        is_active: false,
        options: [],
      };
      addAnswer();
      addAnswer();
    }

    function addAnswer() {
      props.matter.poll.options.push({
        id: Math.random(),
        title: "",
        _destroy: false,
      });
    }

    function removeAnswer(item: {
      id: number;
      title: string;
      _destroy: boolean;
    }) {
      if (props.matter.poll.options.length <= 2)
        return error("Опрос должен содержать минимум 2 варианта ответа");
      const index = props.matter.poll.options.findIndex(
        (el) => el.id === item.id
      );
      item.id < 1
        ? props.matter.poll.options.splice(index, 1)
        : (item._destroy = true);
    }

    const options = computed(() =>
      props.matter.poll.options.filter((el) => !el._destroy)
    );

    return () => (
      <div class="mb-6">
        {props.matter.poll && !props.matter.poll._destroy ? (
          <div>
            <div class="f-label flex justify-between">
              <span>
                Опрос
                <span class="text-red-600 ml-1">*</span>
              </span>
              <button
                class="remove"
                type="button"
                onClick={() => (props.matter.poll._destroy = true)}
              >
                Удалить опрос
              </button>
            </div>
            <input
              class="f-input mb-4"
              v-model={props.matter.poll.title}
              required
              placeholder="Вопрос"
            />
            <Checkbox
              value={props.matter.poll.is_active}
              onChange={(value) => (props.matter.poll.is_active = value)}
            >
              Опрос активен
            </Checkbox>
            {options.value.map((el, i) => (
              <div key={el.id}>
                <div class="f-label flex justify-between">
                  <span>
                    Ответ {i + 1}
                    <span class="text-red-600 ml-1">*</span>
                  </span>
                  <button
                    class="remove"
                    type="button"
                    onClick={() => removeAnswer(el)}
                  >
                    Удалить ответ
                  </button>
                </div>
                <input
                  required
                  placeholder="Ответ"
                  v-model={el.title}
                  class="f-input mb-4"
                />
              </div>
            ))}
            <button type="button" onClick={addAnswer} class="btn">
              Добавить ответ
            </button>
          </div>
        ) : (
          <button type="button" onClick={addPoll} class="btn">
            Добавить опрос
          </button>
        )}
      </div>
    );
  },
});
