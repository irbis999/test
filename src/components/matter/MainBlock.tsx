/* eslint-disable vue/no-mutating-props */
import date from "@/helpers/date";
import { cant } from "@/helpers/permissions";
import { MatterType } from "@/helpers/types";
import { useStore } from "@/store";
import { computed, defineComponent, onMounted, PropType, ref } from "vue";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { SelectNumber } from "../form/Select";
import ImageBlock from "../ImageBlock";
import Poll from "./Poll";
import OfficialComment from "./OfficialComment";
import Tilda from "./Tilda";
import Card from "./Card";
import Test from "./Test";
import Survey from "./Survey";
import PressConference from "./PressConference";
import Persona from "./Persona";
import ContentBlocks from "../ContentBlocks";

interface ColumnistType {
  id: number;
  full_name: string;
}

export default defineComponent({
  name: "MainBlock",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const store = useStore();
    const platform = computed(() => {
      return store.state.platforms.find(
        (el) => el.id === props.matter.platform_id
      );
    });
    const maxTitleLength = platform.value?.max_title_length || 150;
    const maxLeadLength = platform.value?.max_lead_length || 300;
    const isOpinion = props.matter.type === "Matter::Opinion";
    const columnists = ref<ColumnistType[]>([]);

    onMounted(async () => {
      try {
        if (isOpinion) {
          const { data } = await api.get<ColumnistType[]>("columnists", {
            params: { platform_id: props.matter.platform_id },
          });
          columnists.value = data;
          if (!props.matter.columnist_id)
            props.matter.columnist_id = data[0]?.id;
        }
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div>
        <div class="f-label pb-2">
          {props.matter.platform.title} / {props.matter.type_title} / Создан:{" "}
          {date(props.matter.created_at)} / Обновлен{" "}
          {date(props.matter.updated_at)}
        </div>
        <Input
          value={props.matter.title}
          onChange={(value) => (props.matter.title = value)}
          required
          placeholder="Название материала"
          disabled={cant("edit_text")}
          maxlength={maxTitleLength}
        >
          Заголовок
        </Input>
        <Textarea
          value={props.matter.lead}
          onChange={(value) => (props.matter.lead = value)}
          required
          placeholder="Вступительный текст"
          disabled={cant("edit_text")}
          maxlength={maxLeadLength}
        >
          Лид
        </Textarea>
        {isOpinion && columnists.value.length && (
          <SelectNumber
            value={props.matter.columnist_id}
            options={columnists.value.map((el) => ({
              id: el.id,
              title: el.full_name,
            }))}
            onChange={(value) => (props.matter.columnist_id = value)}
          >
            Эксперт
          </SelectNumber>
        )}
        <div class="f-label">Заходное фото</div>
        <ImageBlock class="mb-6" image={props.matter.image} />
        {props.matter.type === "Matter::Card" && <Card matter={props.matter} />}
        {props.matter.type === "Matter::Tilda" && (
          <Tilda matter={props.matter} />
        )}
        {props.matter.type === "Matter::Test" && <Test matter={props.matter} />}
        {props.matter.type === "Matter::Survey" && (
          <Survey matter={props.matter} />
        )}
        {props.matter.type === "Matter::PressConference" && (
          <PressConference matter={props.matter} />
        )}
        {props.matter.type === "Matter::Persona" && (
          <Persona matter={props.matter} />
        )}
        {[
          "Matter::NewsItem",
          "Matter::Article",
          "Matter::Video",
          "Matter::Gallery",
          "Matter::Opinion",
          "Matter::Persona",
        ].includes(props.matter.type) && <ContentBlocks item={props.matter} />}
        {!["Matter::Tilda", "Matter::Test", "Matter::Survey"].includes(
          props.matter.type
        ) && <Poll matter={props.matter} />}
        {[
          "Matter::NewsItem",
          "Matter::Article",
          "Matter::Video",
          "Matter::Gallery",
          "Matter::Opinion",
        ].includes(props.matter.type) && (
          <OfficialComment matter={props.matter} />
        )}
      </div>
    );
  },
});
