/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType } from "vue";
import { MatterType } from "@/helpers/types";
import api from "@/helpers/api";
import { error, flash } from "@/helpers/notifications";
import Input from "../form/Input";

export default defineComponent({
  name: "Tilda",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    let busy = false;
    async function updateContent() {
      if (busy) return;
      busy = true;
      try {
        await api.post(`matters/${props.matter.id}/update_content`);
        flash("Контент будет обновлен в ближайшее время");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div>
        <Input
          value={props.matter.tilda_url}
          onChange={(value) => (props.matter.tilda_url = value)}
          required
        >
          Ссылка на тильду
        </Input>
        <button type="button" onClick={updateContent}>
          Обновить контент
        </button>
      </div>
    );
  },
});
