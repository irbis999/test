/* eslint-disable vue/no-mutating-props */
import { TicketType } from "@/helpers/types";
import { defineComponent, nextTick, PropType, ref } from "vue";
import Input from "../form/Input";
import ImageBlock from "../ImageBlock";
import Tiny from "../Tiny";

type CallbackType = () => void;

export default defineComponent({
  name: "Ticket",
  props: {
    item: { type: Object as PropType<TicketType>, required: true },
    onRemove: { type: Function as PropType<CallbackType>, required: true },
    onUp: { type: Function as PropType<CallbackType>, required: true },
    onDown: { type: Function as PropType<CallbackType>, required: true },
  },
  setup(props) {
    const editorActive = ref(true);

    function moveTicket(direction: "up" | "down") {
      direction === "up" ? props.onUp() : props.onDown();
      window.dispatchEvent(new Event("restartTiny"));
    }

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    window.addEventListener("restartTiny", async () => {
      editorActive.value = false;
      await nextTick();
      editorActive.value = true;
    });

    return () => (
      <div class="my-6 pb-6 border-b">
        <Input
          value={props.item.title}
          onChange={(value) => (props.item.title = value)}
          required
          placeholder="Заголовок карточки"
        >
          Заголовок карточки
        </Input>
        <ImageBlock image={props.item.image} removable />
        {editorActive.value && (
          <Tiny
            class="my-6"
            value={props.item.text}
            onChange={(value) => (props.item.text = value)}
            required
          >
            Текст карточки
          </Tiny>
        )}
        <div class="flex">
          <button
            type="button"
            class="btn m-icon"
            onClick={() => moveTicket("up")}
          >
            arrow_upward
          </button>
          <button
            type="button"
            class="btn m-icon"
            onClick={() => moveTicket("down")}
          >
            arrow_downward
          </button>
          <button
            type="button"
            class="btn red m-icon"
            onClick={() => props.onRemove()}
          >
            delete
          </button>
        </div>
      </div>
    );
  },
});
