/* eslint-disable vue/no-mutating-props */
import { defineComponent, PropType } from "vue";
import { TestResultType } from "@/helpers/types";
import Textarea from "../form/Textarea";
import ImageBlock from "../ImageBlock";

export default defineComponent({
  name: "TestResult",
  props: {
    maxScore: { type: Number, required: true },
    item: { type: Object as PropType<TestResultType>, required: true },
    onRemove: { type: Function as PropType<() => void>, required: true },
  },
  setup(props) {
    return () => (
      <div class="mb-6 pb-6 border-b">
        <div class="f-label flex justify-between">
          <span>
            Количество баллов
            <span class="text-red-600 ml-1"></span>
          </span>
          <button onClick={props.onRemove} class="remove">
            Удалить результат
          </button>
        </div>
        <div class="flex mb-4">
          <select v-model={props.item.score[0]} class="f-input">
            <option disabled>От (включительно)</option>
            {Array.from({ length: props.maxScore }).map((el, i) => (
              <option key={i} value={i}>
                {i}
              </option>
            ))}
            <option value=""></option>
          </select>
          <div class="mx-3 m-icon">remove</div>
          <select v-model={props.item.score[1]} class="f-input">
            <option disabled>До (включительно)</option>
            {Array.from({ length: props.maxScore }).map((el, i) => (
              <option key={i} value={i}>
                {i}
              </option>
            ))}
            <option value=""></option>
          </select>
        </div>
        <Textarea
          value={props.item.text}
          onChange={(value) => (props.item.text = value)}
          required
        >
          Текст
        </Textarea>
        <ImageBlock image={props.item.image} />
      </div>
    );
  },
});
