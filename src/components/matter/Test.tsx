/* eslint-disable vue/no-mutating-props */
import { MatterType, TestQuestionType, TestResultType } from "@/helpers/types";
import { computed, defineComponent, PropType } from "vue";
import emptyImage from "../ImageBlock/emptyImage";
import Tiny from "../Tiny";
import TestQuestion from "./TestQuestion";
import TestResult from "./TestResult";

export default defineComponent({
  name: "Test",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const questions = computed(() =>
      props.matter.questions.filter((el) => !el._destroy)
    );
    const results = computed(() =>
      props.matter.results.filter((el) => !el._destroy)
    );

    function removeQuestion(item: TestQuestionType) {
      const index = props.matter.questions.findIndex((el) => el.id === item.id);
      item.id < 1
        ? props.matter.questions.splice(index, 1)
        : (item._destroy = true);
    }

    function removeResult(item: TestResultType) {
      const index = props.matter.results.findIndex((el) => el.id === item.id);
      item.id < 1
        ? props.matter.results.splice(index, 1)
        : (item._destroy = true);
    }

    function addQuestion() {
      props.matter.questions.push({
        id: Math.random(),
        text: "",
        explanation: "",
        answers: [],
        image: { ...emptyImage },
        _destroy: false,
      });
    }

    function addResult() {
      props.matter.results.push({
        id: Math.random(),
        text: "",
        score: [0, 0],
        image: { ...emptyImage },
        _destroy: false,
      });
    }

    return () => (
      <div>
        <Tiny
          value={props.matter.text}
          onChange={(value) => (props.matter.text = value)}
        >
          Текст
        </Tiny>
        <div class="text-2xl my-6">Вопросы</div>
        {questions.value.map((el, i) => (
          <TestQuestion
            key={el.id}
            item={el}
            index={i}
            onRemove={() => removeQuestion(el)}
          />
        ))}
        <div class="border-t mt-6 pt-6">
          <button type="button" class="btn blue mb-6" onClick={addQuestion}>
            Добавить вопрос
          </button>
        </div>
        {results.value.map((el) => (
          <TestResult
            item={el}
            key={el.id}
            onRemove={() => removeResult(el)}
            maxScore={questions.value.length}
          />
        ))}
        <button type="button" class="btn blue mb-6" onClick={addResult}>
          Добавить результат
        </button>
      </div>
    );
  },
});
