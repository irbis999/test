/* eslint-disable vue/no-mutating-props */
import { MatterType, TestQuestionType } from "@/helpers/types";
import { computed, defineComponent, PropType } from "vue";
import Textarea from "../form/Textarea";
import ImageBlock from "../ImageBlock";
import emptyImage from "../ImageBlock/emptyImage";
import Tiny from "../Tiny";
import SurveyQuestion from "./SurveyQuestion";

export default defineComponent({
  name: "Survey",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const questions = computed(() =>
      props.matter.questions.filter((el) => !el._destroy)
    );

    function removeQuestion(item: TestQuestionType) {
      const index = props.matter.questions.findIndex((el) => el.id === item.id);
      item.id < 1
        ? props.matter.questions.splice(index, 1)
        : (item._destroy = true);
    }

    function addQuestion() {
      props.matter.questions.push({
        id: Math.random(),
        text: "",
        explanation: "",
        answers: [],
        image: { ...emptyImage },
        _destroy: false,
      });
    }

    return () => (
      <div>
        <Tiny
          value={props.matter.text}
          onChange={(value) => (props.matter.text = value)}
        >
          Текст
        </Tiny>
        <div class="text-2xl my-6">Вопросы</div>
        {questions.value.map((el, i) => (
          <SurveyQuestion
            key={el.id}
            item={el}
            index={i}
            onRemove={() => removeQuestion(el)}
          />
        ))}
        <div class="border-t mt-6 pt-6">
          <button type="button" class="btn blue" onClick={addQuestion}>
            Добавить вопрос
          </button>
        </div>
        <div class="mb-6 pb-6 border-b">
          <div class="text-2xl my-6">Результат</div>
          <Textarea
            required
            value={props.matter.result.text}
            onChange={(value) => (props.matter.result.text = value)}
          >
            Текст
          </Textarea>
          <ImageBlock image={props.matter.result.image} />
        </div>
      </div>
    );
  },
});
