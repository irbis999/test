/* eslint-disable vue/no-mutating-props */
import api from "@/helpers/api";
import { can, cant } from "@/helpers/permissions";
import { MatterType, StoryType } from "@/helpers/types";
import { useStore } from "@/store";
import { computed, defineComponent, onMounted, PropType, ref } from "vue";
import { DropdownNumber } from "../Dropdown";
import Checkbox from "../form/Checkbox";
import Input from "../form/Input";
import { SelectNumber, SelectString } from "../form/Select";
import Text from "../form/Text";
import Loading from "../Loading";
import MetaInformation from "../MetaInformation";
import { RubricType } from "../publishers/RubricForm";
import Authors from "./Authors";
import Tags from "./Tags";

export default defineComponent({
  name: "AsideBlock",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const store = useStore();
    const loaded = ref(false);
    const rubrics = ref<RubricType[]>([]);
    const hasRubric = ["Matter::NewsItem", "Matter::Article"].includes(
      props.matter.type
    );

    const matterUsers = computed(() => {
      return store.state.users
        .filter(
          (el) =>
            el.id === props.matter.editor_id ||
            el.id === props.matter.photographer_id ||
            el.platform_ids.includes(props.matter.platform_id)
        )
        .map((el) => ({ id: el.id, title: el.full_name }));
    });

    const matterStatuses = computed(() => {
      const statuses = [...store.state.statuses].filter(
        (el) => !["deleted", "published", "planned"].includes(el.id)
      );
      if (can("manage_matters"))
        statuses.push({ id: "deleted", title: "Удален" });
      if (can("publish_matters")) {
        statuses.push({ id: "published", title: "Опубликован" });
        statuses.push({ id: "planned", title: "Запланирован" });
      }
      return statuses;
    });

    const personaCategories = computed(() => {
      const categories = [...store.state.persona_categories];
      categories.unshift({ id: "", title: "Без категории" });
      return categories;
    });

    const matterStories = computed(() => {
      const stories = store.state.stories.filter(
        (el) => el.platform_id === props.matter.platform_id
      );
      stories.unshift({ id: 0, title: "Без сюжета" } as StoryType);
      return stories;
    });

    const blockedUsersOptions = computed(() => {
      return store.state.users
        .filter((el) => el.platform_ids.includes(props.matter.platform_id))
        .filter((user) => {
          const role = store.state.roles.find(
            (role) => role.id === user.role_id
          );
          return !role?.is_admin;
        })
        .map((el) => ({ id: el.id, title: el.full_name }));
    });

    onMounted(async () => {
      if (!props.matter.editor_id)
        props.matter.editor_id = store.state.users[0].id;
      const params = { platform_id: props.matter.platform_id, limit: 200 };
      const { data } = await api.get<RubricType[]>("rubrics", { params });
      rubrics.value = data;
      if (!props.matter.rubric_id && hasRubric)
        props.matter.rubric_id = rubrics.value[0].id;
      loaded.value = true;
    });

    return () =>
      loaded.value ? (
        <div>
          {hasRubric && can("manage_rubrics") && (
            <SelectNumber
              value={props.matter.rubric_id}
              onChange={(value) => (props.matter.rubric_id = value)}
              options={rubrics.value}
            >
              Рубрика
            </SelectNumber>
          )}
          {hasRubric && cant("manage_rubrics") && (
            <Text value={props.matter.rubric_title}>Рубрика</Text>
          )}
          <SelectNumber
            required
            value={props.matter.editor_id}
            onChange={(value) => (props.matter.editor_id = value)}
            options={matterUsers.value}
          >
            Редактор
          </SelectNumber>
          <Text value={props.matter.initiator_name}>Инициатор</Text>
          <Input
            value={props.matter.published_at}
            onChange={(value) => (props.matter.published_at = value)}
            required
            type="datetime-local"
          >
            Дата и время публикации
          </Input>
          <SelectString
            required
            value={props.matter.status}
            onChange={(value) => (props.matter.status = value)}
            options={matterStatuses.value}
          >
            Статус
          </SelectString>
          {props.matter.type === "Matter::Persona" && (
            <SelectString
              value={props.matter.category}
              onChange={(value) => (props.matter.category = value)}
              options={personaCategories.value}
            >
              Категория
            </SelectString>
          )}
          <SelectNumber
            value={props.matter.story_id}
            onChange={(value) => (props.matter.story_id = value)}
            options={matterStories.value}
          >
            Сюжет
          </SelectNumber>
          <Authors matter={props.matter} />
          <Tags matter={props.matter} />
          <Checkbox
            value={props.matter.show_authors}
            onChange={(value) => (props.matter.show_authors = value)}
          >
            Показывать авторов
          </Checkbox>
          <div class="border-t border-b pt-4 mb-4">
            <Checkbox
              value={props.matter.show_in_day_picture}
              onChange={(value) => (props.matter.show_in_day_picture = value)}
            >
              В картину дня
            </Checkbox>
            <Checkbox
              value={props.matter.show_in_edition_block}
              onChange={(value) => (props.matter.show_in_edition_block = value)}
            >
              В редакционный блок
            </Checkbox>
          </div>
          {store.state.user.role.is_admin && (
            <DropdownNumber
              class="mb-6"
              value={props.matter.blocked_users_ids}
              onChange={(value) => (props.matter.blocked_users_ids = value)}
              options={blockedUsersOptions.value}
            >
              Заблокировать материал для
            </DropdownNumber>
          )}
          <MetaInformation item={props.matter} noindex />
          <details>
            <summary>
              <span>Счетчики</span>
            </summary>
            <Text value={props.matter.counters.views}>Просмотры</Text>
            <Text value={props.matter.counters.uniques}>
              Уникальные посетители
            </Text>
          </details>
          <Input
            value={props.matter.old_uri}
            onChange={(value) => (props.matter.old_uri = value)}
          >
            Старая ссылка
          </Input>
          <Checkbox
            value={props.matter.in_feed}
            onChange={(value) => (props.matter.in_feed = value)}
          >
            Отправить в ленту новостей издания
          </Checkbox>
          <Checkbox
            value={props.matter.in_global_feed}
            onChange={(value) => (props.matter.in_global_feed = value)}
          >
            Отправить в единую ленту новостей
          </Checkbox>
          <Checkbox
            value={props.matter.direct_link_only}
            onChange={(value) => (props.matter.direct_link_only = value)}
          >
            Показывать только по прямой ссылке
          </Checkbox>
          <Checkbox
            value={props.matter.is_promo}
            onChange={(value) => (props.matter.is_promo = value)}
          >
            Рекламный материал
          </Checkbox>
          <Checkbox
            class="border-t pt-4"
            value={props.matter.rss_yandex_news}
            onChange={(value) => (props.matter.rss_yandex_news = value)}
          >
            Отправить в Я.Новости
          </Checkbox>
          <Checkbox
            value={props.matter.rss_yandex_zen}
            onChange={(value) => (props.matter.rss_yandex_zen = value)}
          >
            Отправить в Я.Дзен
          </Checkbox>
          <Checkbox
            value={props.matter.rss_google_news}
            onChange={(value) => (props.matter.rss_google_news = value)}
          >
            Отправить в Google News
          </Checkbox>
        </div>
      ) : (
        <Loading />
      );
  },
});
