/* eslint-disable vue/no-mutating-props */
import { MatterType, TicketType } from "@/helpers/types";
import { computed, defineComponent, PropType } from "vue";
import emptyImage from "../ImageBlock/emptyImage";
import Ticket from "./Ticket";

export default defineComponent({
  name: "Card",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    function setTicketsPositions() {
      props.matter.tickets.forEach((el, i) => (el.position = i));
    }

    function addTicket() {
      props.matter.tickets.push({
        id: Math.random(),
        title: "",
        text: "",
        image: { ...emptyImage },
        position: props.matter.tickets.length,
      });
    }

    function moveTicket(item: TicketType, inc: number) {
      const index = props.matter.tickets.indexOf(item);
      props.matter.tickets.splice(index, 1);
      props.matter.tickets.splice(index + inc, 0, item);
      setTicketsPositions();
    }

    function removeTicket(item: TicketType) {
      item.id >= 1
        ? (item._destroy = true)
        : props.matter.tickets.splice(props.matter.tickets.indexOf(item), 1);
      setTicketsPositions();
    }

    const listTickets = computed(() =>
      props.matter.tickets.filter((el) => !el._destroy)
    );

    return () => (
      <div>
        <div class="text-2xl mb-6">Карточки</div>
        {listTickets.value.map((el) => (
          <Ticket
            key={el.id}
            item={el}
            onDown={() => moveTicket(el, 1)}
            onUp={() => moveTicket(el, -1)}
            onRemove={() => removeTicket(el)}
          />
        ))}
        <button type="button" class="btn m-icon mb-4" onClick={addTicket}>
          add
        </button>
      </div>
    );
  },
});
