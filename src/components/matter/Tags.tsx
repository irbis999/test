/* eslint-disable vue/no-mutating-props */
import { MatterType } from "@/helpers/types";
import { defineComponent, PropType, ref } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import { TagType } from "../publishers/TagForm";
import debounce from "lodash.debounce";
import Loading from "../Loading";
import InfScroll from "../InfScroll";

export default defineComponent({
  name: "Tags",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const apiParams = ref({
      platform_id: props.matter.platform_id,
      q: "",
    });
    const { items, pending, fetch, busy } = useInfScroll<TagType>(
      "tags",
      apiParams
    );

    const processInput = debounce(function () {
      if (!apiParams.value.q) return;
      fetch(true);
    }, 300);

    function addItem(item: string, newItem = false) {
      if (props.matter.tags.includes(item)) return;
      props.matter.tags.push(item);
      if (newItem) apiParams.value.q = "";
    }

    return () => (
      <div class="tags">
        <div class="f-label">Теги</div>
        <div class="tags-container">
          <input
            onKeydown={(e) => {
              if (e.key !== "Enter") return;
              e.preventDefault();
              addItem(apiParams.value.q, true);
            }}
            inputmode="search"
            class="tags-search"
            placeholder="🔍 Поиск"
            v-model={apiParams.value.q}
            onInput={processInput}
          />
          <div class="tags-options">
            {items.value.map((el, i) => (
              <button
                type="button"
                class="tags-option-item"
                key={i}
                onClick={() => addItem(el.title)}
              >
                {el.title}
              </button>
            ))}
            {pending.value && <Loading />}
            <InfScroll
              onLoad={() => (apiParams.value.q ? fetch() : null)}
              busy={busy.value}
              parent
            />
          </div>
          <div class="tags-selected">
            {props.matter.tags.map((el, i) => (
              <div class="tags-selected-item">
                {el}
                <button
                  class="btn m-icon small"
                  type="button"
                  onClick={() => props.matter.tags.splice(i, 1)}
                >
                  delete
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  },
});
