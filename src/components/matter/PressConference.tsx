/* eslint-disable vue/no-mutating-props */
import { MatterType } from "@/helpers/types";
import { defineComponent, onMounted, PropType, ref } from "vue";
import { ColumnistType } from "@/components/publishers/ColumnistForm";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Input from "../form/Input";
import Textarea from "../form/Textarea";
import { DropdownNumber } from "../Dropdown";
import Checkbox from "../form/Checkbox";
import Tiny from "../Tiny";

export default defineComponent({
  name: "PressConference",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const columnists = ref<ColumnistType[]>([]);
    const loaded = ref(false);

    onMounted(async () => {
      try {
        const params = { platform_id: props.matter.platform_id };
        const { data } = await api.get<ColumnistType[]>("columnists", {
          params,
        });
        columnists.value = data;
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div>
        <Input
          type="datetime-local"
          value={props.matter.holding_at}
          onChange={(value) => (props.matter.holding_at = value)}
        >
          Дата проведения
        </Input>
        <Input
          type="url"
          value={props.matter.link}
          onChange={(value) => (props.matter.link = value)}
        >
          Ссылка на конференцию
        </Input>
        <Textarea
          value={props.matter.widget_code}
          onChange={(value) => (props.matter.widget_code = value)}
        >
          Код видео конференции
        </Textarea>
        <div class="f-label">Спикеры</div>
        {loaded.value && (
          <DropdownNumber
            class="mb-6"
            value={props.matter.speaker_ids}
            options={columnists.value.map((el) => ({
              id: el.id,
              title: el.full_name,
            }))}
            onChange={(value) => (props.matter.speaker_ids = value)}
          >
            Спикеры
          </DropdownNumber>
        )}
        <Checkbox
          value={props.matter.is_finished}
          onChange={(value) => (props.matter.is_finished = value)}
        >
          Конференция завершена
        </Checkbox>
        <div class="f-label">Описание</div>
        <Tiny
          value={props.matter.description}
          onChange={(value) => (props.matter.description = value)}
        >
          Описание
        </Tiny>
      </div>
    );
  },
});
