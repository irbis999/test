/* eslint-disable vue/no-mutating-props */
import { MatterType } from "@/helpers/types";
import { computed, defineComponent, PropType, ref } from "vue";
import { useStore } from "@/store";

export default defineComponent({
  name: "Authors",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    const store = useStore();
    const q = ref("");
    const users = computed(() => {
      return store.state.users
        .filter((el) => el.platform_ids.includes(props.matter.platform_id))
        .filter((el) =>
          el.full_name.toLowerCase().includes(q.value.toLowerCase())
        );
    });

    function addItem(
      item: { id: number | null; name: string },
      newItem = false
    ) {
      if (props.matter.authors.find((el) => el.name === item.name)) return;
      props.matter.authors.push(item);
      if (newItem) q.value = "";
    }

    return () => (
      <div class="tags">
        <div class="f-label">Авторы</div>
        <div class="tags-container">
          <input
            onKeydown={(e) => {
              if (e.key !== "Enter") return;
              e.preventDefault();
              addItem({ id: null, name: q.value });
            }}
            inputmode="search"
            class="tags-search"
            placeholder="🔍 Поиск"
            v-model={q.value}
          />
          <div class="tags-options">
            {users.value.map((el, i) => (
              <button
                type="button"
                class="tags-option-item"
                key={i}
                onClick={() => addItem({ id: null, name: el.full_name })}
              >
                {el.full_name}
              </button>
            ))}
          </div>
          <div class="tags-selected">
            {props.matter.authors.map((el, i) => (
              <div class="tags-selected-item">
                {el.name}
                <button
                  class="btn m-icon small"
                  type="button"
                  onClick={() => props.matter.authors.splice(i, 1)}
                >
                  delete
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  },
});
