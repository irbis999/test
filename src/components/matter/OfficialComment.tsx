/* eslint-disable vue/no-mutating-props */
import { error } from "@/helpers/notifications";
import { MatterType } from "@/helpers/types";
import { defineComponent, PropType } from "vue";
import Input from "../form/Input";
import Textarea from "../form/Textarea";

export default defineComponent({
  name: "OfficialComment",
  props: {
    matter: { type: Object as PropType<MatterType>, required: true },
  },
  setup(props) {
    function removeUrl(index: number) {
      if (props.matter.official_comment.urls.length <= 1)
        return error("Комментарий должен содержать минимум 1 ссылку");
      props.matter.official_comment.urls.splice(index, 1);
    }

    function addComment() {
      props.matter.official_comment = {
        id: Math.random(),
        _destroy: false,
        text: "",
        source_name: "",
        source_url: "",
        urls: [""],
      };
    }

    return () => (
      <div class="mt-4">
        {props.matter.official_comment &&
          !props.matter.official_comment._destroy && (
            <div>
              <div class="flex justify-between mb-4">
                <h3 class="font-bold">Официальный комментарий</h3>
                <button
                  type="button"
                  class="remove"
                  onClick={() =>
                    (props.matter.official_comment._destroy = true)
                  }
                >
                  Удалить комментарий
                </button>
              </div>
              <Textarea
                value={props.matter.official_comment.text}
                required
                placeholder="Текст комментария"
                onChange={(value) =>
                  (props.matter.official_comment.text = value)
                }
              >
                Текст комментария
              </Textarea>
              <Input
                required
                value={props.matter.official_comment.source_name}
                onChange={(value) =>
                  (props.matter.official_comment.source_name = value)
                }
              >
                Название источника
              </Input>
              <Input
                required
                type="url"
                value={props.matter.official_comment.source_url}
                onChange={(value) =>
                  (props.matter.official_comment.source_url = value)
                }
              >
                Ссылка на источник
              </Input>
              {props.matter.official_comment.urls.map((el, i) => (
                <div key={i}>
                  <div class="f-label flex justify-between">
                    <span>
                      Ссылка {i + 1} <span class="text-red-600 ml-1">*</span>
                    </span>
                    <button
                      class="remove"
                      type="button"
                      onClick={() => removeUrl(i)}
                    >
                      Удалить ссылку
                    </button>
                  </div>
                  <input
                    class="f-input mb-4"
                    v-model={props.matter.official_comment.urls[i]}
                    required
                    type="url"
                    placeholder="Ссылка"
                  />
                </div>
              ))}
              <button
                type="button"
                class="btn blue"
                onClick={() => props.matter.official_comment.urls.push("")}
              >
                Добавить ссылку
              </button>
            </div>
          )}
        {(!props.matter.official_comment ||
          props.matter.official_comment._destroy) && (
          <button class="btn blue" type="button" onClick={addComment}>
            Добавить официальный комментарий
          </button>
        )}
      </div>
    );
  },
});
