import { defineComponent, PropType } from "vue";
import { MetaInformationType } from "@/helpers/types";
import Input from "./form/Input";
import Textarea from "./form/Textarea";

interface ItemType {
  meta_information_attributes: MetaInformationType;
  noindex_text?: string;
}

export const metaInformation = {
  title: "",
  description: "",
  keywords: "",
};

export default defineComponent({
  name: "MetaInformation",
  props: {
    item: {
      type: Object as PropType<ItemType>,
      required: true,
    },
    noindex: { type: Boolean, default: false },
  },
  setup(props) {
    if (!props.item.meta_information_attributes) {
      // eslint-disable-next-line vue/no-mutating-props
      props.item.meta_information_attributes = { ...metaInformation };
    }

    return () => {
      const meta = props.item.meta_information_attributes;
      return (
        <details>
          <summary>
            <span>SEO</span>
          </summary>
          <Input value={meta.title} onChange={(val) => (meta.title = val)}>
            Title tag
          </Input>
          <Input
            value={meta.description}
            onChange={(val) => (meta.description = val)}
          >
            Description tag
          </Input>
          <Input
            value={meta.keywords}
            onChange={(val) => (meta.keywords = val)}
          >
            Keywords tag
          </Input>
          {props.noindex && (
            <Textarea
              value={props.item.noindex_text}
              // eslint-disable-next-line vue/no-mutating-props
              onChange={(val) => (props.item.noindex_text = val)}
            >
              Неиндексируемый текст
            </Textarea>
          )}
        </details>
      );
    };
  },
});
