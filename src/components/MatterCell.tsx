import { defineComponent, PropType, ref, withKeys, withModifiers } from "vue";
import debounce from "lodash.debounce";
import Loading from "./Loading";
import InfScroll from "./InfScroll";
import useInfScroll from "@/helpers/useInfScroll";
import { MatterType } from "@/helpers/types";
import styles from "../styles/components/MatterCell.module.scss";
import { error } from "@/helpers/notifications";

export interface MatterCellType {
  matter_id: number | null;
  matter_title: string;
}

export default defineComponent({
  name: "MatterCell",
  props: {
    platformId: { type: Number, default: 0 },
    item: { type: Object as PropType<MatterCellType>, required: true },
    type: { type: Array as PropType<string[]>, default: () => [] },
    dropErrorType: { type: String, default: "" },
  },
  setup(props) {
    const el = ref<HTMLDivElement>();
    const apiParams = ref({
      platformId: props.platformId ? props.platformId : undefined,
      q: "",
      status: "published",
      type: props.type,
    });
    const dragging = ref(false);
    const { items, pending, busy, fetch } = useInfScroll<MatterType>(
      "matters",
      apiParams
    );

    function processDragLeave(e: DragEvent) {
      if (!el.value) return;
      if (!el.value.contains(e.relatedTarget as HTMLElement)) {
        dragging.value = false;
      }
    }

    function processDrop(e: DragEvent) {
      console.log("drop");
      dragging.value = false;
      if (!e.dataTransfer) return;
      const matter = JSON.parse(
        e.dataTransfer.getData("text/plain")
      ) as MatterType;
      if (props.type.length && !props.type.includes(matter.type))
        return error(
          "Пожалуйста, выберите материал типа " + props.dropErrorType
        );
      selectMatter(matter);
    }

    function selectMatter(matter: { id: number | null; title: string }) {
      // eslint-disable-next-line vue/no-mutating-props
      props.item.matter_id = matter.id;
      // eslint-disable-next-line vue/no-mutating-props
      props.item.matter_title = matter.title;
    }

    const processInput = debounce((e: Event) => {
      apiParams.value.q = (e.target as HTMLInputElement).value;
      if (apiParams.value.q) fetch(true);
    }, 500);

    return () => (
      <div
        ref={el}
        class={{ [styles.dragging]: dragging.value }}
        onDragover={withModifiers(() => {}, ["prevent"])}
        onDragenter={withModifiers(() => (dragging.value = true), ["prevent"])}
        onDrop={processDrop}
        onDragleave={processDragLeave}
      >
        <div class={styles.selectedMatter}>
          <span>{props.item.matter_title || "Материал не выбран"}</span>
          {props.item.matter_id && (
            <button
              class={styles.remove}
              onClick={() => selectMatter({ id: null, title: "" })}
            >
              ✕
            </button>
          )}
        </div>
        <input
          class="f-input"
          placeholder="🔍 Поиск материала"
          onInput={processInput}
        />
        <div class={styles.searchResults}>
          {items.value.map((el, i) => (
            <div
              class={styles.item}
              key={i}
              role="button"
              tabindex="0"
              onKeydown={withKeys(() => selectMatter(el), ["enter"])}
              onClick={() => selectMatter(el)}
            >
              {el.title}
              {!props.platformId && (
                <span class="text-gray-900 pl-1"> / {el.platform.title}</span>
              )}
            </div>
          ))}
          {pending.value && <Loading class="my-8" />}
          <InfScroll
            onLoad={() => (apiParams.value.q ? fetch() : null)}
            busy={busy.value}
            parent
          />
        </div>
      </div>
    );
  },
});
