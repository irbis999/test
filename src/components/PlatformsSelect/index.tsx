/* eslint-disable vue/one-component-per-file */
import { useStore } from "@/store";
import { defineComponent, PropType, toRefs, watch, ref } from "vue";
import { DropdownNumber } from "../Dropdown";
import { SelectNumber } from "../form/Select";
import { PlatformsModalMultiple, PlatformsModalSingle } from "./PlatformsModal";

function usePlatforms() {
  const store = useStore();
  return store.state.platforms.filter((el) =>
    store.state.user.platform_ids.includes(el.id)
  );
}

export const PlatformsSelectSingle = defineComponent({
  name: "PlatformsSelectSingle",
  props: {
    value: { type: Number, required: true },
    onChange: {
      type: Function as PropType<(value: number) => void>,
      required: true,
    },
  },
  setup(props) {
    const platforms = usePlatforms();
    const platformId = ref(props.value);
    watch(toRefs(props).value, () => (platformId.value = props.value));

    return () =>
      platforms.length < 5 ? (
        <SelectNumber
          class="!mb-0"
          value={platformId.value}
          onChange={(value) => props.onChange(value)}
          options={platforms}
        />
      ) : (
        <PlatformsModalSingle
          value={platformId.value}
          onChange={(value) => props.onChange(value)}
          options={platforms}
        />
      );
  },
});

export const PlatformsSelectMultiple = defineComponent({
  name: "PlatformsSelectMultiple",
  props: {
    value: { type: Array as PropType<number[]>, required: true },
    onChange: {
      type: Function as PropType<(value: number[]) => void>,
      required: true,
    },
  },
  setup(props) {
    const platforms = usePlatforms();
    const platformId = ref(props.value);
    watch(toRefs(props).value, () => (platformId.value = props.value));

    return () =>
      platforms.length < 5 ? (
        <DropdownNumber
          class="!mb-0"
          value={platformId.value}
          onChange={(value) => props.onChange(value)}
          options={platforms}
        >
          Издания
        </DropdownNumber>
      ) : (
        <PlatformsModalMultiple
          value={platformId.value}
          onChange={(value) => props.onChange(value)}
          options={platforms}
        />
      );
  },
});
