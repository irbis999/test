/* eslint-disable vue/one-component-per-file */
import { useStore } from "@/store";
import {
  computed,
  defineComponent,
  PropType,
  ref,
  Transition,
  watch,
  toRefs,
} from "vue";
import Search from "../Search";
import sortPlatforms from "@/helpers/sortPlatforms";
import { PlatformType } from "@/helpers/types";

function usePlatformsModal(options: PlatformType[]) {
  const store = useStore();
  const modalVisible = ref(false);
  const q = ref("");

  const items = computed(() => {
    return sortPlatforms(options).filter((el) =>
      el.title.toLowerCase().includes(q.value.toLowerCase())
    );
  });

  function openModal() {
    modalVisible.value = true;
    store.commit("LOCK_SCROLL");
  }

  function closeModal() {
    modalVisible.value = false;
    store.commit("UNLOCK_SCROLL");
  }

  return { modalVisible, q, items, openModal, closeModal };
}

export const PlatformsModalSingle = defineComponent({
  name: "PlatformsModalSingle",
  props: {
    value: { type: Number, required: true },
    onChange: {
      type: Function as PropType<(value: number) => void>,
      required: true,
    },
    options: { type: Array as PropType<PlatformType[]>, required: true },
  },
  setup(props) {
    const { modalVisible, q, items, openModal, closeModal } = usePlatformsModal(
      props.options
    );

    return () => (
      <div>
        <button class="f-input" onClick={openModal} type="button">
          {items.value.find((el) => el.id === props.value)?.title}
        </button>
        <Transition name="fade-up">
          {modalVisible.value && (
            <div class="fixed left-0 top-0 w-full h-full bg-white p-6 overflow-auto z-10">
              <div class="bottom-buttons">
                <button class="btn" onClick={closeModal} type="button">
                  Закрыть окно
                </button>
              </div>
              <Search
                value={q.value}
                onChange={(value) => (q.value = value)}
                class="mb-6"
              />
              <div
                class="grid gap-4 mb-16"
                style={{
                  gridTemplateColumns: "repeat(auto-fill, minmax(220px, 1fr))",
                }}
              >
                {items.value.map((el) => (
                  <button
                    type="button"
                    key={el.id}
                    class="flex items-center cursor-pointer text-black"
                    onClick={() => {
                      props.onChange(el.id);
                      closeModal();
                    }}
                  >
                    <span class="m-icon mr-2">
                      {props.value === el.id
                        ? "check_box"
                        : "check_box_outline_blank"}
                    </span>
                    <span class={{ "font-bold": el.template === "astrahan24" }}>
                      {el.title}
                    </span>
                  </button>
                ))}
              </div>
            </div>
          )}
        </Transition>
      </div>
    );
  },
});

export const PlatformsModalMultiple = defineComponent({
  name: "PlatformsModalMultiple",
  props: {
    value: { type: Array as PropType<number[]>, required: true },
    onChange: {
      type: Function as PropType<(value: number[]) => void>,
      required: true,
    },
    options: { type: Array as PropType<PlatformType[]>, required: true },
  },
  setup(props) {
    const { modalVisible, q, items, openModal, closeModal } = usePlatformsModal(
      props.options
    );
    const selected = ref([...props.value]);
    watch(toRefs(props).value, () => (selected.value = [...props.value]));

    function togglePlatform(id: number) {
      selected.value.includes(id)
        ? selected.value.splice(selected.value.indexOf(id), 1)
        : selected.value.push(id);
    }

    const selectedLabel = computed(() => {
      if (!selected.value.length) return "";
      return selected.value.length == props.options.length
        ? "Все"
        : selected.value.length.toString();
    });

    return () => (
      <div>
        <button class="f-input text-left" onClick={openModal} type="button">
          Издания{" "}
          {selectedLabel.value && (
            <span class="inline-block bg-gray-500 text-white rounded ml-2 px-1">
              {selectedLabel.value}
            </span>
          )}
        </button>
        <Transition name="fade-up">
          {modalVisible.value && (
            <div class="fixed left-0 top-0 w-full h-full bg-white p-6 overflow-auto z-10">
              <div class="bottom-buttons">
                <button
                  class="btn blue"
                  onClick={() => {
                    props.onChange(selected.value);
                    closeModal();
                  }}
                  type="button"
                >
                  Выбрать {selected.value.length}
                </button>
                <button
                  class="btn green"
                  onClick={() => {
                    selected.value = props.options.map((el) => el.id);
                    props.onChange(selected.value);
                    closeModal();
                  }}
                  type="button"
                >
                  Выбрать все
                </button>
                <button
                  class="btn red"
                  onClick={() => {
                    selected.value = [];
                    props.onChange(selected.value);
                    closeModal();
                  }}
                  type="button"
                >
                  Сбросить
                </button>
                <button class="btn" onClick={closeModal} type="button">
                  Закрыть окно
                </button>
              </div>
              <Search
                value={q.value}
                onChange={(value) => (q.value = value)}
                class="mb-6"
              />
              <div
                class="grid gap-4 mb-16"
                style={{
                  gridTemplateColumns: "repeat(auto-fill, minmax(220px, 1fr))",
                }}
              >
                {items.value.map((el) => (
                  <button
                    type="button"
                    key={el.id}
                    class="flex items-center cursor-pointer text-black"
                    onClick={() => togglePlatform(el.id)}
                  >
                    <span class="m-icon mr-2">
                      {selected.value.includes(el.id)
                        ? "check_box"
                        : "check_box_outline_blank"}
                    </span>
                    <span class={{ "font-bold": el.template === "astrahan24" }}>
                      {el.title}
                    </span>
                  </button>
                ))}
              </div>
            </div>
          )}
        </Transition>
      </div>
    );
  },
});
