import api from "@/helpers/api";
import { error, flash } from "@/helpers/notifications";
import { defineComponent, PropType, ref } from "vue";
import { useRouter } from "vue-router";

export interface ItemType {
  id: number;
  resource_id: number;
  title: string;
}

export default defineComponent({
  name: "RevisionPageItem",
  props: {
    item: { type: Object as PropType<ItemType>, required: true },
  },
  setup(props) {
    const router = useRouter();
    const busy = ref(false);

    async function preview() {
      if (busy.value) return;
      busy.value = true;
      try {
        const { data } = await api.post<{
          data: { preview_id: number; host: string };
        }>("previews/revisions", {
          revision_id: props.item.id,
        });
        window.open(`${data.data.host}/preview/${data.data.preview_id}`);
      } catch (e) {
        error(e);
      }
      busy.value = false;
    }

    async function restore() {
      if (busy.value || !confirm("Подтвердите восстановление версии")) return;
      busy.value = true;
      try {
        await api.post(`revisions/${props.item.id}/restore`);
        await router.push(`/matters/${props.item.resource_id}`);
        flash("Версия восстановлена");
      } catch (e) {
        error(e);
      }
      busy.value = false;
    }

    return () => (
      <div class="list-item">
        <button onClick={preview}>{props.item.title}</button>
        <button
          class="btn m-icon"
          title="Восстановить версию"
          onClick={restore}
        >
          restore
        </button>
      </div>
    );
  },
});
