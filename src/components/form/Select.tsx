/* eslint-disable vue/one-component-per-file */
import { defineComponent, PropType, ref, watch, toRefs } from "vue";

interface StringOptionType {
  id: string;
  title: string;
}

export const SelectString = defineComponent({
  name: "SelectString",
  props: {
    value: { type: String, required: true },
    options: { type: Array as PropType<StringOptionType[]>, required: true },
    onChange: {
      type: Function as PropType<(value: string) => void>,
      required: true,
    },
    required: { type: Boolean, default: false },
  },
  setup(props, { slots }) {
    const selectValue = ref(props.value);
    watch(toRefs(props).value, () => (selectValue.value = props.value));

    function processChange(e: Event) {
      selectValue.value = (e.target as HTMLSelectElement).value;
      props.onChange(selectValue.value);
    }

    return () => (
      <div class="mb-6">
        <div class="f-label flex">
          {slots.default ? slots.default() : null}
          {props.required && <span class="text-red-700 ml-1">*</span>}
        </div>
        <select
          class="f-input"
          value={selectValue.value}
          onChange={processChange}
          onBlur={processChange}
        >
          {props.options.map((el) => (
            <option value={el.id} key={el.id}>
              {el.title}
            </option>
          ))}
        </select>
      </div>
    );
  },
});

interface NumberOptionType {
  id: number;
  title: string;
}

export const SelectNumber = defineComponent({
  name: "SelectNumber",
  props: {
    value: { type: Number, required: true },
    options: { type: Array as PropType<NumberOptionType[]>, required: true },
    onChange: {
      type: Function as PropType<(value: number) => void>,
      required: true,
    },
    required: { type: Boolean, default: false },
  },
  setup(props, { slots }) {
    const selectValue = ref(props.value);
    watch(toRefs(props).value, () => (selectValue.value = props.value));

    function processChange(e: Event) {
      selectValue.value = +(e.target as HTMLSelectElement).value;
      props.onChange(selectValue.value);
    }

    return () => (
      <div class="mb-6">
        <div class="f-label flex">
          {slots.default ? slots.default() : null}
          {props.required && <span class="text-red-700 ml-1">*</span>}
        </div>
        <select
          class="f-input"
          value={selectValue.value}
          onChange={processChange}
          onBlur={processChange}
        >
          {props.options.map((el) => (
            <option value={el.id} key={el.id}>
              {el.title}
            </option>
          ))}
        </select>
      </div>
    );
  },
});
