import { defineComponent, PropType } from "vue";
import useMaxLength from "./useMaxLength";

export default defineComponent({
  name: "Textarea",
  props: {
    value: { type: String, default: "" },
    onChange: {
      type: Function as PropType<(value: string) => void>,
      required: true,
    },
    required: { type: Boolean, default: false },
    maxlength: { type: Number, default: 0 },
    disabled: { type: Boolean, default: false },
    placeholder: { type: String, default: "" },
  },
  setup(props, { slots }) {
    const { inputValue, countdown, maxLengthError } = useMaxLength(props);

    return () => (
      <div class="mb-6">
        <div class="f-label flex">
          {slots.default ? slots.default() : null}
          {props.required && <span class="text-red-700 ml-1">*</span>}
          {props.maxlength > 0 && (
            <span
              class={{
                "ml-auto": true,
                "text-gray-600": !maxLengthError.value,
                "text-red-700": maxLengthError.value,
              }}
            >
              {countdown.value}
            </span>
          )}
        </div>
        <textarea
          class={{
            "f-input": true,
            "h-24": true,
            "border-red-700": maxLengthError.value,
          }}
          value={inputValue.value}
          placeholder={props.placeholder}
          required={props.required}
          disabled={props.disabled}
          maxlength={props.maxlength ? props.maxlength : undefined}
          onInput={(e) => {
            const value = (e.target as HTMLTextAreaElement).value;
            inputValue.value = value;
            props.onChange(value);
          }}
        />
      </div>
    );
  },
});
