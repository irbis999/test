import { defineComponent, PropType } from "vue";
import useMaxLength from "./useMaxLength";

type InputTypeType =
  | "text"
  | "email"
  | "url"
  | "date"
  | "datetime-local"
  | "password"
  | "number";

export default defineComponent({
  name: "Input",
  props: {
    value: { type: String, default: "" },
    onChange: {
      type: Function as PropType<(value: string) => void>,
      required: true,
    },
    required: { type: Boolean, default: false },
    maxlength: { type: Number, default: 0 },
    disabled: { type: Boolean, default: false },
    min: { type: Number, default: -10000 },
    type: {
      type: String as PropType<InputTypeType>,
      default: "text",
    },
    placeholder: { type: String, default: "" },
  },
  setup(props, { slots }) {
    const { inputValue, countdown, maxLengthError } = useMaxLength(props);

    return () => (
      <div class="mb-6">
        <div class="f-label flex">
          {slots.default ? slots.default() : null}
          {props.required && <span class="text-red-700 ml-1">*</span>}
          {props.maxlength > 0 && (
            <span
              class={{
                "ml-auto": true,
                "text-gray-600": !maxLengthError.value,
                "text-red-700": maxLengthError.value,
              }}
            >
              {countdown.value}
            </span>
          )}
        </div>
        <input
          class={{
            "f-input": true,
            "border-red-700": maxLengthError.value,
          }}
          type={props.type}
          value={inputValue.value}
          placeholder={props.placeholder}
          required={props.required}
          min={props.min}
          disabled={props.disabled}
          maxlength={props.maxlength ? props.maxlength : undefined}
          onInput={(e) => {
            const value = (e.target as HTMLTextAreaElement).value;
            inputValue.value = value;
            props.onChange(value);
          }}
        />
      </div>
    );
  },
});
