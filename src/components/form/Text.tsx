import { defineComponent } from "vue";

export default defineComponent({
  name: "Text",
  props: {
    value: { type: [String, Number], default: "" },
  },
  setup(props, { slots }) {
    return () => (
      <div class="mb-6">
        <div class="f-label">{slots.default ? slots.default() : null}</div>
        <input type="text" class="f-input" readonly value={props.value} />
      </div>
    );
  },
});
