import { defineComponent, PropType } from "vue";

export default defineComponent({
  name: "Checkbox",
  props: {
    value: { type: Boolean, default: false },
    onChange: {
      type: Function as PropType<(value: boolean) => void>,
      required: true,
    },
  },
  setup(props, { slots }) {
    return () => (
      <label class="f-label block flex text-base items-center pb-3">
        <input
          type="checkbox"
          class="cursor-pointer"
          checked={props.value}
          onChange={(e) =>
            props.onChange((e.target as HTMLInputElement).checked)
          }
        />
        <div class="pl-2 cursor-pointer">
          {slots.default ? slots.default() : null}
        </div>
      </label>
    );
  },
});
