import { ref, computed, watch, toRefs, Ref, ComputedRef } from "vue";

type Return = {
  inputValue: Ref<string>;
  countdown: ComputedRef<number>;
  maxLengthError: ComputedRef<boolean>;
};

export default (props: { value: string | null; maxlength: number }): Return => {
  const inputValue = ref(props.value || "");
  const countdown = computed(() => props.maxlength - inputValue.value.length);
  const maxLengthError = computed(() =>
    props.maxlength ? countdown.value < 0 : false
  );

  // https://stackoverflow.com/questions/59125857/how-to-watch-props-change-with-vue-composition-api-vue-3
  watch(toRefs(props).value, () => {
    inputValue.value = props.value || "";
  });

  return { inputValue, countdown, maxLengthError };
};
