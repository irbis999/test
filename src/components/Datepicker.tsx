import { defineComponent, PropType, ref, onMounted } from "vue";
import { format } from "date-fns";
import styles from "../styles/components/search/Search.module.scss";
import Litepicker from "litepicker";

export default defineComponent({
  name: "Datepicker",
  props: {
    onChange: {
      type: Function as PropType<
        (dates: { dateStart: string; dateEnd: string }) => void
      >,
      required: true,
    },
  },
  setup(props) {
    const dateStart = ref("");
    const dateEnd = ref("");
    const el = ref<HTMLInputElement>();

    let picker: Litepicker;

    function emit() {
      props.onChange({ dateStart: dateStart.value, dateEnd: dateEnd.value });
    }

    function clearPicker() {
      if (!picker) return;
      dateStart.value = "";
      dateEnd.value = "";
      picker.clearSelection();
      emit();
    }

    // watch props works bad - infinite loop because picker trigers "selected" event
    window.addEventListener("clearDatepicker", () => {
      dateStart.value = "";
      dateEnd.value = "";
      picker.clearSelection();
    });

    onMounted(() => {
      if (!el.value) return;
      picker = new Litepicker({
        element: el.value,
        lang: "ru-RU",
        singleMode: false,
        showTooltip: false,
        format: "DD-MM-YYYY",
        delimiter: " / ",
      });
      type D = { dateInstance: Date };
      picker.on("selected", (d1: D, d2: D) => {
        dateStart.value = `${format(d1.dateInstance, "yyyy-MM-dd")}T00:00:00`;
        dateEnd.value = `${format(d2.dateInstance, "yyyy-MM-dd")}T23:59:59`;
        emit();
      });
    });

    return () => (
      <div class="relative">
        <input
          class={styles.query}
          readonly
          ref={el}
          placeholder="Выберите дату"
        />
        {dateStart.value && (
          <button
            class={styles.reset}
            type="button"
            onClick={() => clearPicker()}
          />
        )}
      </div>
    );
  },
});
