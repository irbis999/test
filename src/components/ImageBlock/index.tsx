/* eslint-disable vue/no-mutating-props */
import { ImageType } from "@/helpers/types";
import uploadFile from "@/helpers/uploadFile";
import { defineComponent, PropType } from "vue";
import styles from "../../styles/components/ImageBlock.module.scss";
import Crop from "../Crop";
import Checkbox from "../form/Checkbox";

export default defineComponent({
  name: "ImageBlock",
  props: {
    image: { type: Object as PropType<ImageType>, required: true },
    removable: { type: Boolean, default: false },
    blackout: { type: Boolean, default: false },
    watermark: { type: Boolean, default: true },
    maxWidth: { type: Number, default: 800 },
  },
  setup(props) {
    let busy = false;

    async function upload(e: Event) {
      const files = (e.target as HTMLInputElement).files;
      const file = files?.length ? files[0] : null;
      if (busy || !file) return;
      busy = true;
      const resource = await uploadFile(file, props.maxWidth);
      if (resource) {
        props.image.file_id = resource.file_id;
        props.image.url = resource.url;
        props.image.original_url = resource.original_url;
        props.image.width = resource.width;
        props.image.height = resource.height;
        props.image.file_size = resource.file_size;
        props.image.file_filename = resource.file_filename;
        props.image.file_content_type = resource.file_content_type;
        props.image.file_changed = resource.file_changed;
      }
      busy = false;
    }

    function remove() {
      props.image.url = "";
      props.image.original_url = "";
      props.image.author = "";
      props.image.source = "";
      props.image.description = "";
      if (props.watermark) props.image.apply_watermark = false;
      props.image.crop = {
        start_x: 0,
        start_y: 0,
        width: 0,
        height: 0,
      };
      props.image._destroy = true;
    }

    return () => (
      <div class={styles.photo}>
        <div>
          {props.image.url ? (
            <img
              src={props.image.url}
              alt="Изображение"
              style={{
                filter: `brightness(${100 - props.image.apply_blackout}%)`,
              }}
            />
          ) : (
            <div class="bg-gray-400 h-full" />
          )}
        </div>
        <div>
          <input
            v-model={props.image.author}
            class="f-input mb-2"
            placeholder="Автор"
          />
          <input
            v-model={props.image.source}
            class="f-input mb-2"
            placeholder="Источник"
          />
          <input
            v-model={props.image.description}
            class="f-input mb-4"
            placeholder="Подпись"
          />
          {props.watermark && (
            <Checkbox
              value={props.image.apply_watermark}
              onChange={(val) => (props.image.apply_watermark = val)}
            >
              Наложить водяной знак
            </Checkbox>
          )}
          <input onChange={upload} type="file" accept="image/jpeg, image/png" />
        </div>
        {props.image.url && (
          <Crop
            class={styles.crop}
            image={props.image}
            blackout={props.blackout}
          />
        )}
        {props.removable && props.image.url && (
          <button
            type="button"
            class="btn red m-icon absolute top-0 left-0 !rounded-none p-2"
            title="Удалить изображение"
            onClick={remove}
          >
            delete
          </button>
        )}
      </div>
    );
  },
});
