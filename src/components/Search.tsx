import {
  defineComponent,
  PropType,
  ref,
  toRefs,
  watch,
  withModifiers,
} from "vue";
import debounce from "lodash.debounce";
import styles from "../styles/components/search/Search.module.scss";

export default defineComponent({
  name: "Search",
  props: {
    value: { type: String, default: "" },
    lazy: { type: Boolean, default: false },
    onChange: {
      type: Function as PropType<(value: string) => void>,
      required: true,
    },
  },
  setup(props) {
    const query = ref(props.value);

    watch(toRefs(props).value, () => {
      if (props.value !== query.value) {
        query.value = props.value;
      }
    });

    const triggerOnChange = debounce(() => {
      props.onChange(query.value);
    }, 200);

    return () => (
      <div class="relative">
        <form
          class="w-full"
          onSubmit={withModifiers(triggerOnChange, ["prevent"])}
        >
          <input
            class={styles.query}
            inputmode="search"
            placeholder="🔍 Поиск..."
            value={query.value}
            onInput={(e) => {
              query.value = (e.target as HTMLInputElement).value;
              if (!props.lazy) triggerOnChange();
            }}
            onChange={() => (props.lazy ? triggerOnChange() : null)}
          />
          <button
            class={styles.reset}
            type="button"
            hidden={!query.value}
            onClick={() => {
              query.value = "";
              triggerOnChange();
            }}
          ></button>
        </form>
      </div>
    );
  },
});
