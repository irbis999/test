import { ImageType } from "@/helpers/types";
import { defineComponent, PropType, ref, Transition } from "vue";
import Modal from "../Modal";
import Cropper from "./Cropper";

export default defineComponent({
  name: "Crop",
  props: {
    image: { type: Object as PropType<ImageType>, required: true },
    blackout: { type: Boolean, default: false },
  },
  setup(props) {
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <Cropper
                image={props.image}
                blackout={props.blackout}
                onClose={() => (modalVisible.value = false)}
              />
            </Modal>
          )}
        </Transition>
        <button
          type="button"
          class="m-button m-icon bg-blue-100 p-2 cursor-pointer text-black"
          onClick={() => (modalVisible.value = true)}
        >
          edit
        </button>
      </div>
    );
  },
});
