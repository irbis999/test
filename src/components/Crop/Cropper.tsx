import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { ImageType } from "@/helpers/types";
import Cropper from "cropperjs";
import "cropperjs/dist/cropper.css";
import {
  defineComponent,
  onBeforeUnmount,
  onMounted,
  PropType,
  ref,
} from "vue";
import styles from "../../styles/components/Cropper.module.scss";

export default defineComponent({
  name: "Cropper",
  props: {
    blackout: { type: Boolean, default: false },
    image: { type: Object as PropType<ImageType>, required: true },
    onClose: { type: Function as PropType<() => void>, required: true },
  },
  setup(props) {
    const img = ref<HTMLImageElement>();
    const preview = ref<HTMLDivElement>();

    let cropper: Cropper;

    let busy = false;
    async function crop() {
      if (busy) return;
      busy = true;
      const box = cropper.getData();
      const data = {
        ...props.image,
        crop: { ...box, start_x: box.x, start_y: box.y },
      };
      try {
        const { data: img } = await api.post<ImageType>("images/crop", data);
        // eslint-disable-next-line vue/no-mutating-props
        props.image.url = img.url;
        // eslint-disable-next-line vue/no-mutating-props
        props.image.crop = img.crop;
        props.onClose();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    onMounted(() => {
      if (!img.value || !preview.value) return;
      cropper = new Cropper(img.value, {
        viewMode: 2,
        zoomable: false,
        aspectRatio: 1.77,
        guides: false,
        checkCrossOrigin: false,
        preview: preview.value,
        data: {
          x: props.image.crop.start_x,
          y: props.image.crop.start_y,
          width: props.image.crop.width,
          height: props.image.crop.height,
        },
      });
    });

    onBeforeUnmount(() => {
      if (cropper) cropper.destroy();
    });

    return () => (
      <div class={styles.cropper}>
        <div>
          <img
            ref={img}
            src={props.image.original_url}
            alt="img"
            class="block rounded-sm max-w-full"
          />
        </div>
        <div>
          <div
            class={styles.preview}
            ref={preview}
            style={{
              filter: `brightness(${100 - props.image.apply_blackout}%)`,
            }}
          />
          {props.blackout && (
            <div>
              <div class="f-label">Градиент {props.image.apply_blackout}%</div>
              <input
                class="block w-full mb-4"
                type="range"
                min="0"
                max="50"
                step="1"
                value={props.image.apply_blackout}
                onChange={(e) => {
                  const value = +(e.target as HTMLInputElement).value;
                  // eslint-disable-next-line vue/no-mutating-props
                  props.image.apply_blackout = value;
                }}
              />
            </div>
          )}
          <button class="btn green" type="button" onClick={crop}>
            Ok
          </button>
          <button class="btn" type="button" onClick={props.onClose}>
            Отмена
          </button>
        </div>
      </div>
    );
  },
});
