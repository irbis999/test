import { defineComponent, PropType, ref, watch } from "vue";
import Editor from "@tinymce/tinymce-vue";
import device from "@/helpers/device";
const tbUp = "styleselect | bold italic link  | bullist numlist";
const desktopButtons =
  "copy paste | alignleft aligncenter alignright alignjustify";
const tbDown =
  "table | spellchecker | removeformat | tp | fullscreen | split_text";

type CallbackType = (value: string) => void;
type EditorCbType = (e: { html: string }) => void;
type EditorType = {
  on: (eventName: string, cb: EditorCbType) => void;
};

export default defineComponent({
  name: "Tiny",
  props: {
    value: { type: String, required: true },
    required: { type: Boolean, default: false },
    onChange: {
      type: Function as PropType<CallbackType>,
      required: true,
    },
    onSplitText: {
      type: Function as PropType<CallbackType>,
      default: null,
    },
  },
  setup(props, { slots }) {
    const init = {
      language: "ru",
      placeholder: "Введите текст",
      height: 200,
      menubar: false,
      plugins: [
        "autolink lists link anchor visualblocks paste spellchecker wordcount fullscreen autoresize typograf table",
      ],
      toolbar: device.isMobile
        ? [tbUp, tbDown]
        : `${tbUp} | ${desktopButtons} | ${tbDown}`,
      spellchecker_languages: "Russian=ru",
      spellchecker_language: "ru",
      spellchecker_rpc_url: "//speller.yandex.net/services/tinyspell",
      browser_spellcheck: true,
      content_style: `p.question {font-weight: bold;}
        p.question::before {content: '—'; display: inline-block; color: #53A2FF; margin: 0 .4em 0 0; font-weight: normal;}
        p.answer::before {content: '—'; display: inline-block; color: #53A2FF; margin: 0 .4em 0 0}
        blockquote {font-bold; font-size: 24px; line-height: 1.3; border-top: 6px solid #4299e1; border-left: none !important; margin: 20px 0 !important; padding: .2em 0 0 0 !important;}`,
      style_formats: [
        { title: "Абзац", format: "p" },
        { title: "H2", format: "h2" },
        { title: "H3", format: "h3" },
        { title: "H4", format: "h4" },
        { title: "Цитата", format: "blockquote" },
        { title: "Вопрос", format: "question" },
        { title: "Ответ", format: "answer" },
      ],
      formats: {
        p: { block: "p" },
        h2: { block: "h2" },
        h3: { block: "h3" },
        h4: { block: "h4" },
        blockquote: { block: "blockquote" },
        question: { block: "p", classes: "question" },
        answer: { block: "p", classes: "answer" },
      },
      external_plugins: {
        typograf: `//${location.host}/tinymce/typograf-plugin.js`,
        split_text: `//${location.host}/tinymce/split-text-plugin.js`,
      },
      setup: (editor: EditorType) =>
        editor.on("splitText", (e) =>
          props.onSplitText ? props.onSplitText(e.html) : null
        ),
    };

    const content = ref(props.value);
    watch(content, () => props.onChange(content.value));

    return () => (
      <div>
        <div class="f-label flex">
          {slots.default ? slots.default() : null}
          {props.required && <span class="text-red-700 ml-1">*</span>}
        </div>
        <Editor
          apiKey="yz2y3wm5kd98okx4p84weqpswujn29i68lv9u60da8geh2cy"
          init={init}
          v-model={content.value}
        />
        {props.required && (
          <input
            required
            value={content.value}
            style="width: 1px; height: 1px; color: transparent; outline: none;"
          />
        )}
      </div>
    );
  },
});
