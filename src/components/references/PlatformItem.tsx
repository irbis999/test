import { defineComponent, PropType, ref, Transition } from "vue";
import PlatformForm from "./PlatformForm";
import { PlatformType } from "@/helpers/types";
import Modal from "../Modal";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { useStore } from "@/store";

export default defineComponent({
  name: "PlatformItem",
  props: {
    item: { type: Object as PropType<PlatformType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: PlatformType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);
    const store = useStore();

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление издания")) return;
      busy = true;
      try {
        await api.delete(`platforms/${props.item.id}`);
        if (props.onRemove) props.onRemove();
        store.commit("REMOVE_PLATFORM", props.item);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item">
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <PlatformForm
                id={props.item.id}
                onUpdate={(item) => {
                  props.onUpdate(item);
                  modalVisible.value = false;
                  store.commit("ADD_PLATFORM", item);
                }}
              />
            </Modal>
          )}
        </Transition>
        <button type="button" onClick={() => (modalVisible.value = true)}>
          {props.item.title}
        </button>
        <button type="button" onClick={removeItem} class="btn ml-2 red m-icon">
          delete
        </button>
      </div>
    );
  },
});
