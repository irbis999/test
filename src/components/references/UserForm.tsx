import {
  defineComponent,
  ref,
  onMounted,
  PropType,
  withModifiers,
  computed,
} from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import { SelectNumber } from "../form/Select";
import { UserType } from "@/helpers/types";
import { useStore } from "@/store";
import Checkbox from "../form/Checkbox";
import { DropdownNumber } from "../Dropdown";

export default defineComponent({
  name: "UserForm",
  props: {
    id: { type: Number, default: 0 },
    platformId: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: UserType) => void>,
      required: true,
    },
  },
  setup(props) {
    const store = useStore();
    const loaded = ref(false);
    const item = ref<UserType>({
      id: 0,
      authorization: "",
      created_at: "",
      first_name: "",
      full_name: "",
      is_active: true,
      last_name: "",
      platform_ids: [],
      role_id: store.state.roles[0].id,
      updated_at: "",
      email: "",
      phone: "",
      last_sign_in_at: "",
      password: "",
      password_confirmation: "",
      role: store.state.roles[0],
    });

    const userRoles = computed(() => {
      return store.state.roles.filter(
        (el) => el.rank <= store.state.user.role.rank
      );
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<UserType>(`users/${props.id}`);
        data.role_id = data.role?.id;
        item.value = data;
      }
      loaded.value = true;
    });

    function validatePassword() {
      const pass = item.value.password;
      if (!pass) return true;
      let err = "";
      if (pass !== item.value.password_confirmation)
        err = "Введенные пароли не совпадают";
      if (!/[a-z]+/.exec(pass))
        err =
          "Пароль должен содержать цифру, маленькую и большую буквы латинского алфавита";
      if (!/[0-9]+/.exec(pass))
        err =
          "Пароль должен содержать цифру, маленькую и большую буквы латинского алфавита";
      if (pass.length < 8) err = "Пароль должен содержать минимум 8 символов";
      if (err) error(err);
      return err ? false : true;
    }

    let busy = false;
    async function send() {
      if (busy) return;
      if (!item.value.platform_ids.length)
        return error("Пожалуйста, выберите минимум 1 издание");
      if (!validatePassword()) return;
      busy = true;
      try {
        const url = item.value.id ? `users/${item.value.id}` : "users";
        const method = item.value.id ? "patch" : "post";
        const user = { ...item.value };
        if (!user.password) {
          user.password = undefined;
          user.password_confirmation = undefined;
        }
        const { data } = await api[method]<UserType>(url, {
          user,
        });
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    function setActive(value: boolean) {
      item.value.is_active = value;
      void send();
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Пользователь</h1>
          <Input
            value={item.value.first_name}
            onChange={(value) => (item.value.first_name = value)}
            required
            placeholder="Имя"
          >
            Имя
          </Input>
          <Input
            value={item.value.last_name}
            onChange={(value) => (item.value.last_name = value)}
            required
            placeholder="Фамилия"
          >
            Фамилия
          </Input>
          <Input
            value={item.value.email}
            onChange={(value) => (item.value.email = value)}
            required
            type="email"
            placeholder="Email"
          >
            Email
          </Input>
          <Input
            value={item.value.phone}
            onChange={(value) => (item.value.phone = value)}
            required
            placeholder="Телефон"
          >
            Телефон
          </Input>
          <Checkbox
            value={item.value.is_active}
            onChange={(value) => (item.value.is_active = value)}
          >
            Активен
          </Checkbox>
          <SelectNumber
            value={item.value.role_id as number}
            onChange={(value) => (item.value.role_id = value)}
            options={userRoles.value as { id: number; title: string }[]}
          >
            Роль
          </SelectNumber>
          <div class="f-label">Издания</div>
          <DropdownNumber
            class="mb-6"
            value={item.value.platform_ids}
            options={store.state.platforms}
            onChange={(value) => (item.value.platform_ids = value)}
          >
            Издания
          </DropdownNumber>
          <Input
            value={item.value.password}
            onChange={(value) => (item.value.password = value)}
            type="password"
          >
            Пароль
          </Input>
          <Input
            value={item.value.password_confirmation}
            onChange={(value) => (item.value.password_confirmation = value)}
            type="password"
          >
            Пароль еще раз
          </Input>
          <div>
            <button class="btn green">Сохранить</button>
            {props.id > 0 && item.value.is_active && (
              <button
                class="btn red"
                type="button"
                onClick={() => setActive(false)}
              >
                Удалить
              </button>
            )}
            {props.id > 0 && !item.value.is_active && (
              <button class="btn" type="button" onClick={() => setActive(true)}>
                Восстановить
              </button>
            )}
          </div>
        </form>
      ) : (
        <Loading />
      );
  },
});
