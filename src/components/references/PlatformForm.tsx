import {
  defineComponent,
  ref,
  onMounted,
  PropType,
  withModifiers,
  computed,
} from "vue";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Loading from "../Loading";
import Input from "../form/Input";
import { SelectString } from "../form/Select";
import { PlatformType, PlatformTemplateType } from "@/helpers/types";
import { metaInformation } from "@/components/MetaInformation";

const templates = [
  { id: "astrahan24", title: "Астрахань24" },
  { id: "local", title: "Районное издание" },
];

export default defineComponent({
  name: "UserForm",
  props: {
    id: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(tag: PlatformType) => void>,
      required: true,
    },
  },
  setup(props) {
    const favicon = ref<File>();
    const logoDesktop = ref<File>();
    const logoMobile = ref<File>();
    const watermark = ref<File>();
    const loaded = ref(false);
    const item = ref<PlatformType>({
      id: 0,
      title: "",
      domain: "",
      hostname: "",
      template: "local",
      color: "",
      contact_email: "",
      robots_txt: "",
      google_analytics_id: "",
      liveinternet_counter_id: "",
      yandex_metrika_id: "",
      yandex_verification: "",
      google_verification: "",
      meta_information_attributes: { ...metaInformation },
      favicon_url: "",
      logo_desktop_url: "",
      logo_mobile_url: "",
      watermark_url: "",
      social_fb: "",
      social_ig: "",
      social_ok: "",
      social_twitter: "",
      social_vk: "",
      social_telegram: "",
      social_youtube: "",
      social_zen: "",
      social_rss: "",
      link_yandex_news: "",
      max_title_length: 150,
      max_lead_length: 300,
      vk_poster_owner_id: "",
      vk_poster_client_id: "",
      vk_poster_access_token: "",
      ok_poster_group_id: "",
      ok_poster_access_token: "",
      ok_poster_application_key: "",
      ok_poster_application_secret_key: "",
      fb_poster_group_id: "",
      fb_poster_access_token: "",
    });

    const vkPosterClientIdLink = computed(() => {
      return `https://oauth.vk.com/authorize?client_id=${item.value.vk_poster_client_id}&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=offline,groups,wall&response_type=token&v=5.70`;
    });

    onMounted(async () => {
      if (props.id) {
        const { data } = await api.get<PlatformType>(`platforms/${props.id}`);
        item.value = data;
      }
      loaded.value = true;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const url = item.value.id ? `platforms/${item.value.id}` : "platforms";
        const method = item.value.id ? "patch" : "post";
        const formData = new FormData();
        const platformStr = "platform";
        formData.append(`${platformStr}[title]`, item.value.title);
        formData.append(`${platformStr}[domain]`, item.value.domain);
        formData.append(`${platformStr}[hostname]`, item.value.hostname);
        formData.append(`${platformStr}[template]`, item.value.template);
        formData.append(`${platformStr}[color]`, item.value.color);
        formData.append(
          `${platformStr}[contact_email]`,
          item.value.contact_email
        );
        formData.append(`${platformStr}[robots_txt]`, item.value.robots_txt);
        formData.append(
          `${platformStr}[google_analytics_id]`,
          item.value.google_analytics_id
        );
        formData.append(
          `${platformStr}[liveinternet_counter_id]`,
          item.value.liveinternet_counter_id
        );
        formData.append(
          `${platformStr}[yandex_metrika_id]`,
          item.value.yandex_metrika_id
        );
        formData.append(
          `${platformStr}[yandex_verification]`,
          item.value.yandex_verification
        );
        formData.append(
          `${platformStr}[google_verification]`,
          item.value.google_verification
        );
        formData.append(
          `${platformStr}[meta_information_attributes][title]`,
          item.value.meta_information_attributes.title
        );
        formData.append(
          `${platformStr}[meta_information_attributes][description]`,
          item.value.meta_information_attributes.description
        );
        formData.append(
          `${platformStr}[meta_information_attributes][keywords]`,
          item.value.meta_information_attributes.keywords
        );
        formData.append(`${platformStr}[social_fb]`, item.value.social_fb);
        formData.append(`${platformStr}[social_ig]`, item.value.social_ig);
        formData.append(`${platformStr}[social_ok]`, item.value.social_ok);
        formData.append(
          `${platformStr}[social_twitter]`,
          item.value.social_twitter
        );
        formData.append(`${platformStr}[social_vk]`, item.value.social_vk);
        formData.append(
          `${platformStr}[social_telegram]`,
          item.value.social_telegram
        );
        formData.append(
          `${platformStr}[social_youtube]`,
          item.value.social_youtube
        );
        formData.append(`${platformStr}[social_zen]`, item.value.social_zen);
        formData.append(`${platformStr}[social_rss]`, item.value.social_rss);
        formData.append(
          `${platformStr}[link_yandex_news]`,
          item.value.link_yandex_news
        );
        formData.append(
          `${platformStr}[max_title_length]`,
          item.value.max_title_length.toString()
        );
        formData.append(
          `${platformStr}[max_lead_length]`,
          item.value.max_lead_length.toString()
        );
        formData.append(
          `${platformStr}[vk_poster_owner_id]`,
          item.value.vk_poster_owner_id
        );
        formData.append(
          `${platformStr}[vk_poster_client_id]`,
          item.value.vk_poster_client_id
        );
        formData.append(
          `${platformStr}[vk_poster_access_token]`,
          item.value.vk_poster_access_token
        );
        formData.append(
          `${platformStr}[ok_poster_group_id]`,
          item.value.ok_poster_group_id
        );
        formData.append(
          `${platformStr}[ok_poster_access_token]`,
          item.value.ok_poster_access_token
        );
        formData.append(
          `${platformStr}[ok_poster_application_key]`,
          item.value.ok_poster_application_key
        );
        formData.append(
          `${platformStr}[ok_poster_application_secret_key]`,
          item.value.ok_poster_application_secret_key
        );
        formData.append(
          `${platformStr}[fb_poster_group_id]`,
          item.value.fb_poster_group_id
        );
        formData.append(
          `${platformStr}[fb_poster_access_token]`,
          item.value.fb_poster_access_token
        );
        if (favicon.value) {
          formData.append(`${platformStr}[favicon]`, favicon.value);
        }
        if (logoDesktop.value) {
          formData.append(`${platformStr}[logo_desktop]`, logoDesktop.value);
        }
        if (logoMobile.value) {
          formData.append(`${platformStr}[logo_mobile]`, logoMobile.value);
        }
        if (watermark.value) {
          formData.append(`${platformStr}[watermark]`, watermark.value);
        }
        const { data } = await api[method]<PlatformType>(url, formData);
        props.onUpdate(data);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Издание</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
            placeholder="Название издания"
          >
            Название издания
          </Input>
          <Input
            value={item.value.hostname}
            onChange={(value) => (item.value.hostname = value)}
            required
            type="url"
            placeholder="https://site.ru"
          >
            Хост
          </Input>
          <Input
            value={item.value.domain}
            onChange={(value) => (item.value.domain = value)}
            required
            placeholder="Домен"
          >
            site.ru
          </Input>
          <Input
            value={item.value.color}
            onChange={(value) => (item.value.color = value)}
            required
            placeholder="#000000"
          >
            Цвет
          </Input>
          <Input
            value={item.value.contact_email}
            onChange={(value) => (item.value.contact_email = value)}
            required
            type="email"
            placeholder="Email для обратной связи"
          >
            Email для обратной связи
          </Input>
          <SelectString
            value={item.value.template}
            onChange={(value) =>
              (item.value.template = value as PlatformTemplateType)
            }
            options={templates}
          >
            Шаблон главной страницы
          </SelectString>
          <Input
            value={item.value.max_title_length.toString()}
            onChange={(value) => (item.value.max_title_length = +value)}
            required
            type="number"
            min={0}
            placeholder="Максимальная длина заголовка"
          >
            Максимальная длина заголовка
          </Input>
          <Input
            value={item.value.max_lead_length.toString()}
            onChange={(value) => (item.value.max_lead_length = +value)}
            required
            type="number"
            min={0}
            placeholder="Максимальная длина лида"
          >
            Максимальная длина лида
          </Input>
          <details>
            <summary>
              <span>Логотипы</span>
            </summary>
            <div class="mb-6">
              <div class="f-label">Фавикон</div>
              {item.value.favicon_url && (
                <img
                  class="mb-2 max-w-100"
                  alt="favicon"
                  src={item.value.favicon_url}
                />
              )}
              <input
                type="file"
                onChange={(e) => {
                  const files = (e.target as HTMLInputElement).files;
                  if (files?.length) favicon.value = files[0];
                }}
              />
            </div>
            <div class="mb-6">
              <div class="f-label">Лого десктоп</div>
              {item.value.logo_desktop_url && (
                <img
                  class="mb-2 max-w-100"
                  alt="logo desktop"
                  src={item.value.logo_desktop_url}
                />
              )}
              <input
                type="file"
                onChange={(e) => {
                  const files = (e.target as HTMLInputElement).files;
                  if (files?.length) logoDesktop.value = files[0];
                }}
              />
            </div>
            <div class="mb-6">
              <div class="f-label">Лого мобайл</div>
              {item.value.logo_mobile_url && (
                <img
                  class="mb-2 max-w-100"
                  alt="logo mobile"
                  src={item.value.logo_mobile_url}
                />
              )}
              <input
                type="file"
                onChange={(e) => {
                  const files = (e.target as HTMLInputElement).files;
                  if (files?.length) logoMobile.value = files[0];
                }}
              />
            </div>
            <div class="mb-6">
              <div class="f-label">Водяной знак</div>
              {item.value.watermark_url && (
                <img
                  class="mb-2 max-w-100"
                  alt="watermark"
                  src={item.value.watermark_url}
                />
              )}
              <input
                type="file"
                onChange={(e) => {
                  const files = (e.target as HTMLInputElement).files;
                  if (files?.length) watermark.value = files[0];
                }}
              />
            </div>
          </details>
          <details>
            <summary>
              <span>Аккаунты в соцсетях</span>
            </summary>
            <Input
              type="url"
              value={item.value.social_fb}
              onChange={(value) => (item.value.social_fb = value)}
            >
              Facebook
            </Input>
            <Input
              type="url"
              value={item.value.social_ig}
              onChange={(value) => (item.value.social_ig = value)}
            >
              Instagram
            </Input>
            <Input
              type="url"
              value={item.value.social_ok}
              onChange={(value) => (item.value.social_ok = value)}
            >
              Одноклассники
            </Input>
            <Input
              type="url"
              value={item.value.social_twitter}
              onChange={(value) => (item.value.social_twitter = value)}
            >
              Twitter
            </Input>
            <Input
              type="url"
              value={item.value.social_vk}
              onChange={(value) => (item.value.social_vk = value)}
            >
              VK
            </Input>
            <Input
              type="url"
              value={item.value.social_telegram}
              onChange={(value) => (item.value.social_telegram = value)}
            >
              Telegram
            </Input>
            <Input
              type="url"
              value={item.value.social_youtube}
              onChange={(value) => (item.value.social_youtube = value)}
            >
              YouTube
            </Input>
            <Input
              type="url"
              value={item.value.social_zen}
              onChange={(value) => (item.value.social_zen = value)}
            >
              Яндекс.Дзен
            </Input>
            <Input
              type="url"
              value={item.value.social_rss}
              onChange={(value) => (item.value.social_rss = value)}
            >
              RSS
            </Input>
          </details>
          <details>
            <summary>
              <span>Аналитика</span>
            </summary>
            <Input
              value={item.value.google_analytics_id}
              onChange={(value) => (item.value.google_analytics_id = value)}
            >
              Google Analytics ID
            </Input>
            <Input
              value={item.value.yandex_metrika_id}
              onChange={(value) => (item.value.yandex_metrika_id = value)}
            >
              Яндекс.Метрика ID
            </Input>
            <Input
              value={item.value.liveinternet_counter_id}
              onChange={(value) => (item.value.liveinternet_counter_id = value)}
            >
              ID группового счетчика Liveinternet
            </Input>
          </details>
          <Input
            value={item.value.link_yandex_news}
            onChange={(value) => (item.value.link_yandex_news = value)}
          >
            Ссылка на Яндекс.Новости
          </Input>
          <Input
            value={item.value.yandex_verification}
            onChange={(value) => (item.value.yandex_verification = value)}
          >
            Я.Вебмастер код верификации
          </Input>
          <Input
            value={item.value.google_verification}
            onChange={(value) => (item.value.google_verification = value)}
          >
            Google Search код верификации
          </Input>
          <h2>SEO</h2>
          <Input
            required
            value={item.value.meta_information_attributes.title}
            onChange={(value) =>
              (item.value.meta_information_attributes.title = value)
            }
          >
            Title tag
          </Input>
          <Input
            required
            value={item.value.meta_information_attributes.description}
            onChange={(value) =>
              (item.value.meta_information_attributes.description = value)
            }
          >
            Description tag
          </Input>
          <Input
            required
            value={item.value.meta_information_attributes.keywords}
            onChange={(value) =>
              (item.value.meta_information_attributes.keywords = value)
            }
          >
            Keywords tag
          </Input>
          <Input
            value={item.value.robots_txt}
            onChange={(value) => (item.value.robots_txt = value)}
          >
            Robots.txt
          </Input>
          <details>
            <summary>
              <span>Постинг материалов в соцсети</span>
            </summary>
            <Input
              value={item.value.vk_poster_owner_id}
              onChange={(value) => (item.value.vk_poster_owner_id = value)}
            >
              VK owner_id
            </Input>
            <Input
              value={item.value.vk_poster_client_id}
              onChange={(value) => (item.value.vk_poster_client_id = value)}
            >
              VK client_id
            </Input>
            <div class="text-gray-600 text-sm mb-6 -mt-4">
              <span>Перейдите</span>
              <a class="mx-1" href={vkPosterClientIdLink.value} target="_blank">
                по ссылке
              </a>
              <span>для получения access_token</span>
            </div>
            <Input
              value={item.value.vk_poster_access_token}
              onChange={(value) => (item.value.vk_poster_access_token = value)}
            >
              VK access_token
            </Input>
            <div class="text-gray-600 text-sm mb-6 -mt-4">
              Перейдите по{" "}
              <a href="https://vk.com/dev/implicit_flow_user" target="_blank">
                ссылке
              </a>
              , дайте разрешение приложению, затем скопируйте access_token из
              URL
            </div>
            <Input
              value={item.value.ok_poster_group_id}
              onChange={(value) => (item.value.ok_poster_group_id = value)}
            >
              OK group_id
            </Input>
            <Input
              value={item.value.ok_poster_access_token}
              onChange={(value) => (item.value.ok_poster_access_token = value)}
            >
              OK access_token
            </Input>
            <Input
              value={item.value.ok_poster_application_key}
              onChange={(value) =>
                (item.value.ok_poster_application_key = value)
              }
            >
              OK application_key
            </Input>
            <Input
              value={item.value.ok_poster_application_secret_key}
              onChange={(value) =>
                (item.value.ok_poster_application_secret_key = value)
              }
            >
              OK application_secret_key
            </Input>
            <Input
              value={item.value.fb_poster_group_id}
              onChange={(value) => (item.value.fb_poster_group_id = value)}
            >
              ФБ ID Группы
            </Input>
            <Input
              value={item.value.fb_poster_access_token}
              onChange={(value) => (item.value.fb_poster_access_token = value)}
            >
              ФБ Токен доступа к API
            </Input>
          </details>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
