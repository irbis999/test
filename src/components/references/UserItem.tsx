import { can } from "@/helpers/permissions";
import { UserType } from "@/helpers/types";
import { useStore } from "@/store";
import { computed, defineComponent, PropType, ref, Transition } from "vue";
import { RouterLink } from "vue-router";
import Modal from "../Modal";
import UserForm from "./UserForm";

export default defineComponent({
  name: "UserItem",
  props: {
    item: { type: Object as PropType<UserType>, required: true },
    onUpdate: {
      type: Function as PropType<(value: UserType) => void>,
      required: true,
    },
  },
  setup(props) {
    const store = useStore();
    const userModalVisible = ref(false);
    const platformsModalVisible = ref(false);
    const userPlatforms = computed(() => {
      return store.state.platforms.filter((el) =>
        store.state.user.platform_ids.includes(el.id)
      );
    });
    const firstUserPlatforms = computed(() => {
      return userPlatforms.value.filter((el, i) => i < 5);
    });
    const canEdit = computed(() => {
      return (
        store.state.user.role.rank < props.item.role.rank && can("manage_users")
      );
    });

    return () => (
      <tr>
        <td>
          <Transition name="fade-up">
            {platformsModalVisible.value && (
              <Modal onClose={() => (platformsModalVisible.value = false)}>
                <div>
                  <h1>Издания пользователя</h1>
                  <ul>
                    {userPlatforms.value.map((el) => (
                      <li key={el.id}>{el.title}</li>
                    ))}
                  </ul>
                  <button
                    type="button"
                    class="btn mt-3"
                    onClick={() => (platformsModalVisible.value = false)}
                  >
                    Закрыть окно
                  </button>
                </div>
              </Modal>
            )}
          </Transition>
          <Transition name="fade-up">
            {userModalVisible.value && (
              <Modal onClose={() => (userModalVisible.value = false)}>
                <UserForm
                  id={props.item.id}
                  onUpdate={(value) => {
                    props.onUpdate(value);
                    userModalVisible.value = false;
                    store.commit("UPDATE_USER", value);
                  }}
                />
              </Modal>
            )}
          </Transition>
          <RouterLink to={`/references/users/${props.item.id}`}>
            {props.item.last_name}
          </RouterLink>
        </td>
        <td>{props.item.first_name}</td>
        <td>{props.item.role.title}</td>
        <td>
          <ul>
            {firstUserPlatforms.value.map((el) => (
              <li key={el.id}>{el.title}</li>
            ))}
          </ul>
          {userPlatforms.value.length > 5 && (
            <button
              type="button"
              onClick={() => (platformsModalVisible.value = true)}
            >
              Показать все издания
            </button>
          )}
        </td>
        <td>{props.item.email}</td>
        <td>{props.item.phone}</td>
        <td>{props.item.is_active ? "Активен" : "Заблокирован"}</td>
        <td>
          {props.item.id === store.state.user.id && (
            <RouterLink to="/my-profile" class="btn m-icon">
              edit
            </RouterLink>
          )}
          {props.item.id !== store.state.user.id && canEdit.value && (
            <button
              type="button"
              class="btn m-icon"
              onClick={() => (userModalVisible.value = true)}
            >
              edit
            </button>
          )}
        </td>
      </tr>
    );
  },
});
