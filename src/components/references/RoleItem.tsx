import { defineComponent, PropType, ref, Transition } from "vue";
import RoleForm from "./RoleForm";
import { RoleType } from "@/helpers/types";
import Modal from "../Modal";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import date from "@/helpers/date";
import { useStore } from "@/store";

export default defineComponent({
  name: "FeedItem",
  props: {
    item: { type: Object as PropType<RoleType>, required: true },
    onUpdate: {
      type: Function as PropType<(item: RoleType) => void>,
      required: true,
    },
    onRemove: { type: Function, default: null },
  },
  setup(props) {
    const modalVisible = ref(false);
    const store = useStore();

    let busy = false;
    async function removeItem() {
      if (busy || !confirm("Подтвердите удаление роли")) return;
      busy = true;
      try {
        await api.delete(`roles/${props.item.id}`);
        if (props.onRemove) props.onRemove();
        store.commit("REMOVE_ROLE", props.item);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <tr>
        <td>
          <Transition name="fade-up">
            {modalVisible.value && (
              <Modal onClose={() => (modalVisible.value = false)}>
                <RoleForm
                  id={props.item.id}
                  onUpdate={(item) => {
                    props.onUpdate(item);
                    modalVisible.value = false;
                    store.commit("UPDATE_ROLE", item);
                  }}
                />
              </Modal>
            )}
          </Transition>
          {props.item.is_editable ? (
            <button type="button" onClick={() => (modalVisible.value = true)}>
              {props.item.title}
            </button>
          ) : (
            <span>{props.item.title}</span>
          )}
        </td>
        <td>{props.item.is_admin ? "Да" : "Нет"}</td>
        <td>{props.item.rank}</td>
        <td>{date(props.item.created_at, "numeric")}</td>
        <td>{date(props.item.updated_at, "numeric")}</td>
        <td>
          {props.item.is_editable && (
            <button
              type="button"
              onClick={removeItem}
              class="btn ml-2 red m-icon"
            >
              delete
            </button>
          )}
        </td>
      </tr>
    );
  },
});
