import { defineComponent, onMounted, PropType, ref, withModifiers } from "vue";
import { RoleType as RType } from "@/helpers/types";
import { useStore } from "@/store";
import { error } from "@/helpers/notifications";
import api from "@/helpers/api";
import Loading from "../Loading";
import Input from "../form/Input";
import Checkbox from "../form/Checkbox";
import { DropdownString } from "../Dropdown";

interface RoleType extends RType {
  matterPermissions: string[];
  createPermissions: string[];
  viewStatusPermissions: string[];
  publisherPermissions: string[];
  usersPermissions: string[];
  otherPermissions: string[];
}

export default defineComponent({
  name: "RoleForm",
  props: {
    id: { type: Number, default: 0 },
    onUpdate: {
      type: Function as PropType<(value: RoleType) => void>,
      required: true,
    },
  },
  setup(props) {
    const store = useStore();
    const loaded = ref(false);
    const item = ref<RoleType>({
      title: "",
      rank: 0,
      is_admin: false,
      permissions: [],
      matterPermissions: [],
      createPermissions: [],
      viewStatusPermissions: [],
      publisherPermissions: [],
      usersPermissions: [],
      otherPermissions: [],
      created_at: "",
      updated_at: "",
    });

    const maxRank = store.state.user.role.rank + 1;
    const permissions = store.state.permissions;
    const matterPermissions = permissions.filter(
      (el) => el.category_id === "matters"
    );
    const createPermissions = permissions.filter(
      (el) => el.category_id === "types"
    );
    const viewStatusPermissions = permissions.filter(
      (el) => el.category_id === "view_status"
    );
    const publisherPermissions = permissions.filter(
      (el) => el.category_id === "publicator"
    );
    const usersPermissions = permissions.filter(
      (el) => el.category_id === "users"
    );
    const otherPermissions = permissions.filter(
      (el) => el.category_id === "other"
    );

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const endpoint = props.id ? `roles/${props.id}` : "roles";
        const method = props.id ? "patch" : "post";
        const data = { ...item.value };
        data.permissions = data.matterPermissions
          .concat(data.createPermissions)
          .concat(data.viewStatusPermissions)
          .concat(data.publisherPermissions)
          .concat(data.usersPermissions)
          .concat(data.otherPermissions);
        const { data: d } = await api[method]<RoleType>(endpoint, data);
        props.onUpdate(d);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    onMounted(async () => {
      try {
        if (props.id) {
          const { data } = await api.get<RoleType>(`roles/${props.id}`);
          data.matterPermissions = data.permissions.filter((id) =>
            matterPermissions.find((el) => el.id === id)
          );
          data.createPermissions = data.permissions.filter((id) =>
            createPermissions.find((el) => el.id === id)
          );
          data.viewStatusPermissions = data.permissions.filter((id) =>
            viewStatusPermissions.find((el) => el.id === id)
          );
          data.publisherPermissions = data.permissions.filter((id) =>
            publisherPermissions.find((el) => el.id === id)
          );
          data.usersPermissions = data.permissions.filter((id) =>
            usersPermissions.find((el) => el.id === id)
          );
          data.otherPermissions = data.permissions.filter((id) =>
            otherPermissions.find((el) => el.id === id)
          );
          item.value = data;
        }
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    });

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <h1>Роль</h1>
          <Input
            value={item.value.title}
            onChange={(value) => (item.value.title = value)}
            required
          >
            Название роли
          </Input>
          <Checkbox
            value={item.value.is_admin}
            onChange={(value) => (item.value.is_admin = value)}
          >
            Является администратором
          </Checkbox>
          <Input
            type="number"
            required
            min={maxRank}
            value={item.value.rank.toString()}
            onChange={(value) => (item.value.rank = +value)}
          >
            Вес роли
          </Input>
          <h2>Разрешения</h2>
          <div class="f-label">Материалы</div>
          <DropdownString
            class="mb-6"
            value={item.value.matterPermissions}
            onChange={(value) => (item.value.matterPermissions = value)}
            options={matterPermissions}
          >
            Материалы
          </DropdownString>
          <div class="f-label">Доступные для создания типы материалов</div>
          <DropdownString
            class="mb-6"
            value={item.value.createPermissions}
            onChange={(value) => (item.value.createPermissions = value)}
            options={createPermissions}
          >
            Доступные для создания типы материалов
          </DropdownString>
          <div class="f-label">Доступные для просмотра статусы материалов</div>
          <DropdownString
            class="mb-6"
            value={item.value.viewStatusPermissions}
            onChange={(value) => (item.value.viewStatusPermissions = value)}
            options={viewStatusPermissions}
          >
            Доступные для просмотра статусы материалов
          </DropdownString>
          <div class="f-label">Доступность публикаторов</div>
          <DropdownString
            class="mb-6"
            value={item.value.publisherPermissions}
            onChange={(value) => (item.value.publisherPermissions = value)}
            options={publisherPermissions}
          >
            Доступность публикаторов
          </DropdownString>
          <div class="f-label">Управление ролями/пользователями</div>
          <DropdownString
            class="mb-6"
            value={item.value.usersPermissions}
            onChange={(value) => (item.value.usersPermissions = value)}
            options={usersPermissions}
          >
            Управление ролями/пользователями
          </DropdownString>
          <div class="f-label">Другое</div>
          <DropdownString
            class="mb-6"
            value={item.value.otherPermissions}
            onChange={(value) => (item.value.otherPermissions = value)}
            options={otherPermissions}
          >
            Другое
          </DropdownString>
          <button class="btn green">Сохранить</button>
        </form>
      ) : (
        <Loading />
      );
  },
});
