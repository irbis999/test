import { defineComponent, ref } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import InfScroll from "@/components/InfScroll";
import Loading from "@/components/Loading";
import styles from "../styles/components/MattersCol.module.scss";
import { MatterType } from "@/helpers/types";
import MatterCard from "./MatterCard";
import { useStore } from "@/store";
import { SelectString } from "./form/Select";
import { DropdownNumber, DropdownString } from "./Dropdown";
import Datepicker from "./Datepicker";
import DropdownAjax from "./Dropdown/DropdownAjax";
import { PlatformsSelectMultiple } from "./PlatformsSelect";

export default defineComponent({
  name: "MattersCol",
  props: {
    platformId: { type: Number, default: 0 },
  },
  setup(props) {
    const store = useStore();
    const filtersVisible = ref(false);
    const apiParams = ref({
      platform_id: props.platformId ? [props.platformId] : [],
      user_id: [] as number[],
      rubric_id: [] as number[],
      type: [] as string[],
      order: store.state.orderOptions[0].id,
      date_start: "",
      date_end: "",
    });

    const { items, pending, busy, fetch } = useInfScroll<MatterType>(
      "matters",
      apiParams
    );

    function reset() {
      apiParams.value = {
        platform_id: props.platformId ? [props.platformId] : [],
        user_id: [],
        rubric_id: [],
        type: [],
        order: store.state.orderOptions[0].id,
        date_start: "",
        date_end: "",
      };
      window.dispatchEvent(new Event("clearDatepicker"));
      fetch(true);
    }

    return () => (
      <div class={styles.col}>
        <div class={styles.filters}>
          <button
            type="button"
            class={`${styles.item} btn`}
            onClick={() => (filtersVisible.value = !filtersVisible.value)}
          >
            Фильтры {filtersVisible.value ? "↑" : "↓"}
          </button>
          <SelectString
            class={styles.item}
            value={apiParams.value.order}
            options={store.state.orderOptions}
            onChange={(value) => {
              apiParams.value.order = value;
              fetch(true);
            }}
          />
          {filtersVisible.value && (
            <>
              {!props.platformId && (
                <PlatformsSelectMultiple
                  class={styles.item}
                  value={apiParams.value.platform_id}
                  onChange={(value) => {
                    apiParams.value.platform_id = value;
                    fetch(true);
                  }}
                />
              )}
              <DropdownAjax
                class={styles.item}
                endpoint={
                  props.platformId
                    ? `rubrics?platform_id=${props.platformId}`
                    : "rubrics"
                }
                value={apiParams.value.rubric_id}
                onChange={(value) => {
                  apiParams.value.rubric_id = value;
                  fetch(true);
                }}
              >
                Рубрики
              </DropdownAjax>
              <DropdownString
                class={styles.item}
                value={apiParams.value.type}
                options={store.state.matter_types}
                onChange={(value) => {
                  apiParams.value.type = value;
                  fetch(true);
                }}
              >
                Типы
              </DropdownString>
              <DropdownNumber
                class={styles.item}
                value={apiParams.value.user_id}
                options={store.state.users.map((el) => ({
                  id: el.id,
                  title: el.full_name,
                }))}
                onChange={(value) => {
                  apiParams.value.user_id = value;
                  fetch(true);
                }}
              >
                Пользователи
              </DropdownNumber>
              <Datepicker
                class={styles.item}
                onChange={({ dateStart, dateEnd }) => {
                  apiParams.value.date_start = dateStart;
                  apiParams.value.date_end = dateEnd;
                  fetch(true);
                }}
              />
              <button
                type="button"
                class={`${styles.item} btn`}
                onClick={reset}
              >
                Сбросить
              </button>
            </>
          )}
        </div>
        <div
          class={{
            [styles.matters]: true,
            [styles.open]: filtersVisible.value,
          }}
        >
          {items.value.map((el) => (
            <div
              draggable
              key={el.id}
              onDragstart={(e) => {
                if (!e.dataTransfer) return;
                e.dataTransfer.setData("text/plain", JSON.stringify(el));
                e.dataTransfer.effectAllowed = "copy";
              }}
            >
              <MatterCard matter={el} />
            </div>
          ))}
          {pending.value && <Loading />}
          <InfScroll onLoad={() => fetch()} busy={busy.value} parent />
        </div>
      </div>
    );
  },
});
