/* eslint-disable vue/one-component-per-file */
import {
  defineComponent,
  ref,
  onMounted,
  PropType,
  computed,
  watch,
  toRefs,
  onBeforeUnmount,
} from "vue";
import styles from "../../styles/components/dropdown/Dropdown.module.scss";

interface PropsType {
  value: (string | number)[];
  options: {
    id: string | number;
    title: string;
  }[];
}

function useDropDown(props: PropsType) {
  const opened = ref(false);
  const q = ref("");
  const el = ref<HTMLElement>();
  const ddValue = ref([...props.value]);

  watch(toRefs(props).value, () => (ddValue.value = [...props.value]));

  function close() {
    opened.value = false;
  }

  function openClose() {
    opened.value = !opened.value;
  }

  function isOptionVisible(title: string) {
    return title.toLowerCase().includes(q.value.toLowerCase());
  }

  function toggleOption(id: string | number) {
    ddValue.value.includes(id)
      ? ddValue.value.splice(ddValue.value.indexOf(id), 1)
      : ddValue.value.push(id);
  }

  function optionClass(id: string | number) {
    return {
      [styles.option]: true,
      [styles.__active]: ddValue.value.includes(id),
    };
  }

  function clickOutside(e: Event) {
    if (!el.value) return;
    if (el.value.contains(e.target as Node)) return;
    close();
  }
  onMounted(() => window.addEventListener("click", clickOutside));
  onBeforeUnmount(() => window.removeEventListener("click", clickOutside));

  function toggleAll() {
    ddValue.value.length
      ? (ddValue.value = [])
      : (ddValue.value = props.options.map((el) => el.id));
    close();
  }

  const selectedLabel = computed(() => {
    if (!ddValue.value.length) return "";
    return ddValue.value.length == props.options.length
      ? "Все"
      : ddValue.value.length.toString();
  });

  return {
    el,
    openClose,
    selectedLabel,
    opened,
    toggleAll,
    q,
    isOptionVisible,
    toggleOption,
    optionClass,
    ddValue,
  };
}

interface StringOptionType {
  id: string;
  title: string;
}

export const DropdownString = defineComponent({
  name: "DropdownString",
  props: {
    value: { type: Array as PropType<string[]>, required: true },
    options: { type: Array as PropType<StringOptionType[]>, required: true },
    onChange: {
      type: Function as PropType<(value: string[]) => void>,
      required: true,
    },
  },
  setup(props, { slots }) {
    const {
      el,
      openClose,
      selectedLabel,
      opened,
      toggleAll,
      q,
      isOptionVisible,
      toggleOption,
      optionClass,
      ddValue,
    } = useDropDown(props);

    return () => (
      <div class={styles.dropdown} ref={el}>
        <button class={styles.trigger} type="button" onClick={openClose}>
          {slots.default ? slots.default() : ""}
          {selectedLabel.value && (
            <div class={styles.selectedLabel}>{selectedLabel.value}</div>
          )}
        </button>
        {opened.value && (
          <div class={styles.wrapper}>
            <div class={styles.wrapperTitle}>
              <button type="button" onClick={close} class="text-black">
                {slots.default ? slots.default() : ""}
              </button>
              <button
                type="button"
                class={styles.reset}
                onClick={() => {
                  toggleAll();
                  props.onChange(ddValue.value as string[]);
                }}
              >
                Выбрать/Сбросить
              </button>
            </div>
            <input
              class={styles.search}
              autocomplete="off"
              value={q.value}
              onInput={(e) => (q.value = (e.target as HTMLInputElement).value)}
              placeholder="Поиск"
            />
            <div class={styles.options}>
              {props.options.map((el) => (
                <button
                  type="button"
                  v-show={isOptionVisible(el.title)}
                  onClick={() => {
                    toggleOption(el.id);
                    props.onChange(ddValue.value as string[]);
                  }}
                  class={optionClass(el.id)}
                >
                  {el.title}
                </button>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  },
});

interface NumberOptionType {
  id: number;
  title: string;
}

export const DropdownNumber = defineComponent({
  name: "DropdownNumber",
  props: {
    value: { type: Array as PropType<number[]>, required: true },
    options: { type: Array as PropType<NumberOptionType[]>, required: true },
    onChange: {
      type: Function as PropType<(value: number[]) => void>,
      required: true,
    },
  },
  setup(props, { slots }) {
    const {
      el,
      openClose,
      selectedLabel,
      opened,
      toggleAll,
      q,
      isOptionVisible,
      toggleOption,
      optionClass,
      ddValue,
    } = useDropDown(props);

    return () => (
      <div class={styles.dropdown} ref={el}>
        <button class={styles.trigger} type="button" onClick={openClose}>
          {slots.default ? slots.default() : ""}
          {selectedLabel.value && (
            <div class={styles.selectedLabel}>{selectedLabel.value}</div>
          )}
        </button>
        {opened.value && (
          <div class={styles.wrapper}>
            <div class={styles.wrapperTitle}>
              <button type="button" onClick={close} class="text-black">
                {slots.default ? slots.default() : ""}
              </button>
              <button
                type="button"
                class={styles.reset}
                onClick={() => {
                  toggleAll();
                  props.onChange(ddValue.value as number[]);
                }}
              >
                Выбрать/Сбросить
              </button>
            </div>
            <input
              class={styles.search}
              autocomplete="off"
              value={q.value}
              onInput={(e) => (q.value = (e.target as HTMLInputElement).value)}
              placeholder="🔍 Поиск"
            />
            <div class={styles.options}>
              {props.options.map((el) => (
                <button
                  type="button"
                  v-show={isOptionVisible(el.title)}
                  onClick={() => {
                    toggleOption(el.id);
                    props.onChange(ddValue.value as number[]);
                  }}
                  class={optionClass(el.id)}
                >
                  {el.title}
                </button>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  },
});
