import {
  defineComponent,
  PropType,
  ref,
  watch,
  toRefs,
  onMounted,
  onBeforeUnmount,
  computed,
} from "vue";
import styles from "../../styles/components/dropdown/Dropdown.module.scss";
import useInfScroll from "@/helpers/useInfScroll";
import debounce from "lodash.debounce";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";

interface OptionType {
  id: number;
  title: string;
  name: string;
}

export default defineComponent({
  name: "DropdownAjax",
  props: {
    value: { type: Array as PropType<number[]>, required: true },
    onChange: {
      type: Function as PropType<(value: number[]) => void>,
      required: true,
    },
    endpoint: { type: String, required: true },
  },
  setup(props, { slots }) {
    const opened = ref(false);
    const el = ref<HTMLElement>();
    const ddValue = ref([...props.value]);

    const apiParams = ref({ q: "" });
    const { items, fetch, pending, busy } = useInfScroll<OptionType>(
      props.endpoint,
      apiParams
    );

    watch(toRefs(props).value, () => (ddValue.value = [...props.value]));

    function close() {
      opened.value = false;
    }

    function openClose() {
      opened.value = !opened.value;
    }

    function clickOutside(e: Event) {
      if (!el.value) return;
      if (el.value.contains(e.target as Node)) return;
      close();
    }

    onMounted(() => window.addEventListener("click", clickOutside));
    onBeforeUnmount(() => window.removeEventListener("click", clickOutside));

    function toggleOption(id: number) {
      ddValue.value.includes(id)
        ? ddValue.value.splice(ddValue.value.indexOf(id), 1)
        : ddValue.value.push(id);
    }

    function optionClass(id: number) {
      return {
        [styles.option]: true,
        [styles.__active]: ddValue.value.includes(id),
      };
    }

    const processInput = debounce((e: Event) => {
      apiParams.value.q = (e.target as HTMLInputElement).value;
      if (apiParams.value.q) fetch(true);
    }, 500);

    const selectedLabel = computed(() => {
      return ddValue.value.length ? ddValue.value.length.toString() : "";
    });

    return () => (
      <div class={styles.dropdown} ref={el}>
        <button class={styles.trigger} type="button" onClick={openClose}>
          {slots.default ? slots.default() : ""}
          {selectedLabel.value && (
            <div class={styles.selectedLabel}>{selectedLabel.value}</div>
          )}
        </button>
        {opened.value && (
          <div class={styles.wrapper}>
            <div class={styles.wrapperTitle}>
              <button type="button" onClick={close} class="text-black">
                {slots.default ? slots.default() : ""}
              </button>
            </div>
            <input
              class={styles.search}
              onInput={processInput}
              placeholder="🔍 Поиск"
            />
            <div class={styles.options}>
              {items.value.map((el) => (
                <button
                  type="button"
                  onClick={() => {
                    toggleOption(el.id);
                    props.onChange(ddValue.value);
                  }}
                  class={optionClass(el.id)}
                >
                  {el.title || el.name}
                </button>
              ))}
              {pending.value && <Loading class="my-8" />}
              <InfScroll
                onLoad={() => (apiParams.value.q ? fetch() : null)}
                busy={busy.value}
                parent
              />
            </div>
          </div>
        )}
      </div>
    );
  },
});
