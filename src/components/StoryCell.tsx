import { defineComponent, PropType, ref, withKeys } from "vue";
import debounce from "lodash.debounce";
import Loading from "./Loading";
import InfScroll from "./InfScroll";
import useInfScroll from "@/helpers/useInfScroll";

export interface StoryCellType {
  story_id: number | null;
  story_title: string;
}

interface StoryType {
  id: number | null;
  title: string;
}

export default defineComponent({
  name: "StoryCell",
  props: {
    platformId: { type: Number, required: true },
    item: { type: Object as PropType<StoryCellType>, required: true },
  },
  setup(props) {
    const apiParams = ref({
      platformId: props.platformId,
      q: "",
    });
    const { items, pending, busy, fetch } = useInfScroll<StoryType>(
      "stories",
      apiParams
    );

    function selectStory(story: StoryType) {
      // eslint-disable-next-line vue/no-mutating-props
      props.item.story_id = story.id;
      // eslint-disable-next-line vue/no-mutating-props
      props.item.story_title = story.title;
    }

    const processInput = debounce((e: Event) => {
      apiParams.value.q = (e.target as HTMLInputElement).value;
      if (apiParams.value.q) fetch(true);
    }, 500);

    return () => (
      <div>
        <div class="py-2 px-3 bg-blue-100 rounded-sm mb-2 flex justify-between">
          <span>{props.item.story_title || "Сюжет не выбран"}</span>
          {props.item.story_id && (
            <button
              class="text-right w-8 cursor-pointer font-bold text-gray-500 ml-2"
              onClick={() => selectStory({ id: null, title: "" })}
            >
              ✕
            </button>
          )}
        </div>
        <input
          class="f-input"
          placeholder="🔍 Поиск сюжета"
          onInput={processInput}
        />
        <div class="overflow-auto mt-2 border" style={{ height: "120px" }}>
          {items.value.map((el, i) => (
            <div
              class="py-2 px-3 cursor-pointer text-blue-500 hover:bg-blue-100"
              key={i}
              role="button"
              tabindex="0"
              onKeydown={withKeys(() => selectStory(el), ["enter"])}
              onClick={() => selectStory(el)}
            >
              {el.title}
            </div>
          ))}
          {pending.value && <Loading class="my-8" />}
          <InfScroll
            onLoad={() => (apiParams.value.q ? fetch() : "")}
            busy={busy.value}
            parent
          />
        </div>
      </div>
    );
  },
});
