import api from "@/helpers/api";
import date from "@/helpers/date";
import { error } from "@/helpers/notifications";
import { useStore } from "@/store";
import { computed, defineComponent, PropType } from "vue";

export interface ItemType {
  id: number;
  is_read: boolean;
  created_at: string;
  text: string;
  platform_id: number;
}

export default defineComponent({
  name: "NotificationsPageItem",
  props: {
    item: { type: Object as PropType<ItemType>, required: true },
  },
  setup(props) {
    const store = useStore();
    const platform = computed(() => {
      const platform = store.state.platforms.find(
        (el) => el.id === props.item.platform_id
      );
      return platform ? platform.title : "";
    });

    let busy = false;
    async function read() {
      if (busy) return;
      busy = true;
      try {
        await api.post(`notifications/${props.item.id}/reads`);
        // eslint-disable-next-line vue/no-mutating-props
        props.item.is_read = !props.item.is_read;
        const num = props.item.is_read ? -1 : 1;
        store.commit("UPDATE_NOTIFICATIONS_COUNT", num);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="list-item justify-start leading-tight">
        <button
          class="flex-shrink-0 rounded-full bg-blue-400 cursor-pointer mr-4"
          style={{
            width: "12px",
            height: "12px",
            background: props.item.is_read ? "#f5f5f5" : "",
          }}
          onClick={read}
        />
        <div class="mr-6 font-bold" v-html={props.item.text}></div>
        <div class="ml-auto mr-6">{date(props.item.created_at)}</div>
        <div>{platform}</div>
      </div>
    );
  },
});
