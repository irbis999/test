import api from "@/helpers/api";
import { useStore } from "@/store";
import { computed, defineComponent } from "vue";
import { RouterLink } from "vue-router";
import { can } from "@/helpers/permissions";
import MatterForm from "./MatterForm";
import device from "@/helpers/device";
import styles from "../../styles/components/Topline.module.scss";

export default defineComponent({
  name: "Topline",
  setup() {
    const store = useStore();
    const userPermissions = computed(() => {
      return store.state.user.role.permissions || [];
    });

    function hasIntersection(arr1: string[], arr2: string[]) {
      let intersects = false;
      for (const el of arr1) if (arr2.includes(el)) intersects = true;
      return intersects;
    }

    const referencesVisible = computed(() => {
      const permissions = [
        "manage_platforms",
        "manage_users",
        "show_users",
        "manage_global_autoscrolls",
        "manage_global_teaser_blocks",
        "manage_roles",
      ];
      return hasIntersection(permissions, userPermissions.value);
    });

    const publishersVisible = computed(() => {
      const permissions = [
        "manage_main_pages",
        "manage_news_feed",
        "manage_promoblocks",
        "manage_rubrics",
        "manage_stories",
        "manage_tags",
        "manage_columnists",
        "manage_autoscrolls",
        "manage_teaser_blocks",
        "manage_static_pages",
        "manage_menu_items",
        "manage_footer",
        "manage_topline",
        "manage_announcements",
        "manage_banners",
        "manage_magazines",
      ];
      return hasIntersection(permissions, userPermissions.value);
    });

    const canCreateMatter =
      can("manage_matters") ||
      can("manage_all_matter") ||
      can("create_matters");

    async function logOut() {
      await api.delete("users/sign_out");
      store.commit("LOG_OUT");
    }

    return () => (
      <div>
        <div class={styles.topline}>
          <RouterLink class={styles.link} to="/matters">
            Материалы
          </RouterLink>
          {publishersVisible.value && (
            <RouterLink class={styles.link} to="/publishers">
              Публикаторы
            </RouterLink>
          )}
          {referencesVisible.value && (
            <RouterLink class={styles.link} to="/references">
              Справочники
            </RouterLink>
          )}
          <RouterLink class={styles.link} to="/tv">
            Телевидение
          </RouterLink>
          {can("create_reports") && (
            <RouterLink class={styles.link} to="/report">
              Отчетность
            </RouterLink>
          )}
          {canCreateMatter && device.isDesktop && <MatterForm />}
          <RouterLink
            to="/notifications"
            class={`ml-auto relative ${styles.link}`}
          >
            <span class="m-icon">notifications</span>
            {store.state.notifications_count > 0 && (
              <span class={styles.num}>
                {store.state.notifications_count > 99
                  ? "99+"
                  : store.state.notifications_count}
              </span>
            )}
          </RouterLink>
          <RouterLink class={`m-icon ${styles.link}`} to="/my-profile">
            person
          </RouterLink>
          <button
            type="button"
            class={`m-icon ${styles.link}`}
            onClick={logOut}
          >
            exit_to_app
          </button>
        </div>
        {canCreateMatter && device.isMobile && (
          <div class="py-4 border-b">
            <MatterForm />
          </div>
        )}
      </div>
    );
  },
});
