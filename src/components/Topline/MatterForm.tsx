import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { MatterType, MatterTypeType } from "@/helpers/types";
import { useStore } from "@/store";
import { defineComponent, ref, withModifiers } from "vue";
import { useRoute, useRouter } from "vue-router";
import { SelectString } from "../form/Select";
import { PlatformsSelectSingle } from "../PlatformsSelect";
const storageKey = "matterFormPlatformId";

export default defineComponent({
  name: "MatterForm",
  setup() {
    const store = useStore();
    const route = useRoute();
    const router = useRouter();
    const fromLs = localStorage.getItem(storageKey);

    const matter = ref({
      title: "-",
      type: store.state.matter_types[0].id,
      tilda_url: "",
      platform_id: fromLs ? +fromLs : store.state.platforms[0].id,
    });

    let busy = false;
    async function send() {
      if (busy) return;
      if (matter.value.type === "Matter::Tilda") {
        const url = prompt("Введите URL тильды");
        if (!url) return;
        matter.value.tilda_url = url;
      }
      busy = true;
      try {
        const content_blocks =
          matter.value.type === "Matter::NewsItem"
            ? [{ kind: "common", text: "" }]
            : undefined;
        const { data } = await api.post<MatterType>("matters", {
          ...matter.value,
          content_blocks,
        });
        localStorage.setItem(storageKey, matter.value.platform_id.toString());
        const path = `/matters/${data.id}`;
        route.name === "matters-id"
          ? // При создании на странице материала совершаем перезагрузку чтобы не возиться с beforeRouteUpdate
            location.assign(path)
          : await router.push(path);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <form
        onSubmit={withModifiers(send, ["prevent"])}
        class="flex items-center mx-4 flex-shrink-0"
      >
        <PlatformsSelectSingle
          class="mr-2 !mb-0"
          value={matter.value.platform_id}
          onChange={(value) => (matter.value.platform_id = value)}
        />
        <SelectString
          class="mr-2 !mb-0"
          value={matter.value.type}
          onChange={(val) => (matter.value.type = val as MatterTypeType)}
          options={store.state.matter_types}
        />
        <button class="btn m-icon" title="Создать материал">
          add_circle
        </button>
      </form>
    );
  },
});
