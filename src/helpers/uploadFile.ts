import compressImg from "./compressImg";
import { error } from "./notifications";
import api from "./api";
import { FileType } from "./types";

export default async function (
  file: File,
  maxWidth = 800
): Promise<FileType | void> {
  try {
    if (["image/jpeg", "image/png"].includes(file.type)) {
      file = await compressImg(file, maxWidth);
    }
    const formData = new FormData();
    formData.append("file", file);
    const { data } = await api.post<FileType>("uploads", formData);
    return data;
  } catch (e) {
    error(e);
  }
}
