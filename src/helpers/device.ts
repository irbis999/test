import DeviceDetector from "device-detector-js";
const deviceDetector = new DeviceDetector();
const device = deviceDetector.parse(navigator.userAgent);
const isMobile = device.device?.type === "smartphone" || !device.device?.type;

export default {
  isMobile,
  isDesktop: !isMobile,
  isWindows: device.os?.name === "Windows",
};
