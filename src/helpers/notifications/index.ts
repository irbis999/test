import axios from "axios";
import "./styles.scss";

function showNotification(text: string, type = "info"): void {
  const modal = document.createElement("div");
  modal.classList.add("flash-component");
  let html = "";
  if (type === "error") {
    html = '<div class="title">Ошибка</div>';
    modal.classList.add("__error");
  }
  html += `<div class="text">${text}</div>`;
  modal.insertAdjacentHTML("afterbegin", html);
  const close = () => {
    modal.classList.remove("__visible");
    modal.addEventListener("transitionend", () => modal.remove());
  };
  document.body.append(modal);
  window.setTimeout(() => modal.classList.add("__visible"), 0);
  window.setTimeout(close, 5000);
  modal.addEventListener("click", close);
}

export function flash(text: string): void {
  showNotification(text);
}

type ErrorType = {
  response: {
    data: {
      error?: string;
      errors?: {
        [key: string]: string;
      };
    };
  };
};

export function error(e: unknown): void {
  console.error(e);
  // Отмена запроса аксиос - это не ошибка
  if (axios.isCancel(e)) return;
  if (typeof e === "string") return showNotification(e, "error");
  const err = e as ErrorType;
  let text = "";
  if (err.response.data.error) {
    text = err.response.data.error;
  }
  if (err.response.data.errors) {
    const errors = err.response.data.errors;
    for (const key in errors) text += errors[key];
  }
  showNotification(text, "error");
}
