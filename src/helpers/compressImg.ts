export default (file: File, maxSize = 800): Promise<File> => {
  return new Promise((resolve) => {
    const elem = document.createElement("img");
    elem.src = URL.createObjectURL(file);
    elem.onload = () => {
      let [width, height] = [elem.width, elem.height];
      if (width > height && width > maxSize) {
        height *= maxSize / width;
        width = maxSize;
      } else if (height > maxSize) {
        width *= maxSize / height;
        height = maxSize;
      }
      const canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      canvas.getContext("2d")?.drawImage(elem, 0, 0, width, height);
      canvas.toBlob(
        (blob) =>
          resolve(new File([blob as Blob], file.name, { type: "image/jpeg" })),
        "image/jpeg"
      );
    };
  });
};
