export interface MetaInformationType {
  title: string;
  description: string;
  keywords: string;
}

export interface StoryType {
  id: number;
  title: string;
  slug: string;
  lead: string;
  main_page_title: string;
  image: ImageType;
  platform_id: number;
  text: string;
  is_main?: boolean;
  meta_information_attributes: MetaInformationType;
}

export type PlatformTemplateType = "astrahan24" | "local";

export type MatterUsersType = {
  id: number | string;
  users: string;
}[];

export interface PlatformType {
  id: number;
  title: string;
  domain: string;
  hostname: string;
  template: PlatformTemplateType;
  color: string;
  contact_email: string;
  robots_txt: string;
  google_analytics_id: string;
  liveinternet_counter_id: string;
  yandex_metrika_id: string;
  yandex_verification: string;
  google_verification: string;
  meta_information_attributes: MetaInformationType;
  favicon_url: string;
  logo_desktop_url: string;
  logo_mobile_url: string;
  watermark_url: string;
  social_fb: string;
  social_ig: string;
  social_ok: string;
  social_twitter: string;
  social_vk: string;
  social_telegram: string;
  social_youtube: string;
  social_zen: string;
  social_rss: string;
  link_yandex_news: string;
  max_title_length: number;
  max_lead_length: number;
  vk_poster_owner_id: string;
  vk_poster_client_id: string;
  vk_poster_access_token: string;
  ok_poster_group_id: string;
  ok_poster_access_token: string;
  ok_poster_application_key: string;
  ok_poster_application_secret_key: string;
  fb_poster_group_id: string;
  fb_poster_access_token: string;
}

export interface RoleType {
  id?: number;
  is_admin: boolean;
  permissions: string[];
  rank: number;
  title: string;
  is_editable?: boolean;
  created_at: string;
  updated_at: string;
}

export interface UserType {
  authorization: string;
  created_at: string;
  first_name: string;
  full_name: string;
  id: number;
  is_active: boolean;
  last_name: string;
  platform_ids: number[];
  role: RoleType;
  role_id?: number;
  updated_at: string;
  email: string;
  phone: string;
  last_sign_in_at: string;
  password?: string;
  password_confirmation?: string;
}

export interface FileType {
  crop: {
    start_x: number;
    start_y: number;
    width: number;
    height: number;
  };
  file_changed: boolean;
  file_content_type: string;
  file_filename: string;
  file_id: string;
  file_size: number;
  original_url: string;
  url: string;
  width: number;
  height: number;
  description: string;
}

export interface ImageType extends FileType {
  id: number;
  author: string;
  source: string;
  apply_watermark: boolean;
  apply_blackout: number;
  _destroy?: boolean;
}

export type MatterTypeType =
  | "Matter::NewsItem"
  | "Matter::Article"
  | "Matter::Opinion"
  | "Matter::PressConference"
  | "Matter::Card"
  | "Matter::Tilda"
  | "Matter::Gallery"
  | "Matter::Video"
  | "Matter::Survey"
  | "Matter::Test"
  | "Matter::Persona";

export interface TicketType {
  id: number;
  position: number;
  text: string;
  title: string;
  image: ImageType;
  _destroy?: boolean;
}

export interface TestAnswerType {
  id: number;
  text: string;
  is_correct: boolean;
  _destroy: boolean;
}

export interface TestQuestionType {
  id: number;
  text: string;
  explanation: string;
  answers: TestAnswerType[];
  image: ImageType;
  _destroy: boolean;
}

export interface TestResultType {
  id: number;
  text: string;
  score: number[];
  image: ImageType;
  _destroy: boolean;
}

export interface MatterType {
  created_at: string;
  lightning: boolean;
  id: number;
  lead: string;
  platform_id: number;
  platform: PlatformType;
  published_at: string;
  rubric_id: number;
  rubric_title: string;
  story_id: number;
  show_authors: boolean;
  show_in_day_picture: boolean;
  show_in_edition_block: boolean;
  blocked_users_ids: number[];
  meta_information_attributes: MetaInformationType;
  old_uri: string;
  in_feed: boolean;
  in_global_feed: boolean;
  direct_link_only: boolean;
  is_promo: boolean;
  rss_yandex_news: boolean;
  rss_google_news: boolean;
  rss_yandex_zen: boolean;
  title: string;
  image: ImageType;
  type: MatterTypeType;
  updated_at: string;
  editor_name: string;
  chars_count: number;
  images_count: number;
  is_locked: boolean;
  editor_id: number;
  photographer_id: number;
  initiator_name: string;
  counters: {
    views: number;
    uniques: number;
  };
  url: string;
  can_unlock: boolean;
  status: string;
  type_title: string;
  official_comment: {
    id: number;
    _destroy: boolean;
    text: string;
    source_name: string;
    source_url: string;
    urls: string[];
  };
  authors: { id: number | null; name: string }[];
  tags: string[];
  poll: {
    id: number;
    _destroy: boolean;
    title: string;
    is_active: boolean;
    options: { id: number; title: string; _destroy: boolean }[];
  };
  content_blocks: ContentBlockType[];
  // Opinion
  columnist_id: number;
  // Card
  tickets: TicketType[];
  // Tilda
  tilda_url: string;
  // Test/Survey
  questions: TestQuestionType[];
  results: TestResultType[];
  text: string;
  result: {
    text: string;
    image: ImageType;
  };
  // Press conference
  holding_at: string;
  link: string;
  widget_code: string;
  speaker_ids: number[];
  is_finished: boolean;
  description: string;
  // Persona
  last_name: string;
  first_name: string;
  patronymic: string;
  occupation: string;
  sex: string;
  birth_date: string;
  show_only_year: boolean;
  birth_place: string;
  party: string;
  education: string;
  marital_status: string;
  children: string;
  facebook: string;
  vkontakte: string;
  twitter: string;
  odnoklassniki: string;
  instagram: string;
  youtube: string;
  website: string;
  wikipedia: string;
  resume: string;
  category: string;
}

export type ContentBlockKindType =
  | "common"
  | "quote"
  | "html"
  | "image"
  | "image360"
  | "file"
  | "video"
  | "read_also"
  | "teaser_matter"
  | "teaser_gallery"
  | "gallery";

export interface ContentBlockType {
  id: number;
  position: number;
  kind: ContentBlockKindType;
  _destroy?: boolean;
}

export interface CommonBlockType extends ContentBlockType {
  text: string;
  kind: "common";
}

export interface QuoteBlockType extends ContentBlockType {
  text: string;
  source_title: string;
  source_url: string;
  kind: "quote";
}

export interface HtmlBlockType extends ContentBlockType {
  code: string;
  kind: "html";
}

export interface ImageBlockType extends ContentBlockType {
  kind: "image" | "image360";
  image: ImageType;
}

export interface FileBlockType extends ContentBlockType {
  kind: "file";
  file: FileType;
}

export interface VideoBlockType extends ContentBlockType {
  kind: "video";
  code: string;
  video: FileType;
  image: ImageType;
}

export interface ReadAlsoBlockType extends ContentBlockType {
  kind: "read_also";
  title: string;
  read_also: { title: string; url: string }[];
}

export interface TeaserMatterBlockType extends ContentBlockType {
  kind: "teaser_matter";
  matter_title: string;
  matter_id: number;
}

export interface TeaserGalleryBlockType extends ContentBlockType {
  kind: "teaser_gallery";
  matter_title: string;
  matter_id: number;
}

export interface GalleryBlockType extends ContentBlockType {
  kind: "gallery";
  images: ImageType[];
}
