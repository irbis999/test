import axios from "axios";
import { store } from "@/store";

const api = axios.create({
  baseURL: "https://api-astrahan.devmi.ru/admin/",
});

api.interceptors.request.use((config) => {
  if (store.state.loggedIn) {
    config.headers.authorization = store.state.user.authorization;
  }
  return config;
});

export default api;
