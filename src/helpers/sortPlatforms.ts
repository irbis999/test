import { PlatformType } from "@/helpers/types";

export default (items: PlatformType[]): PlatformType[] => {
  const sortFn = (a: PlatformType, b: PlatformType) =>
    a.title > b.title ? 1 : -1;
  const ast = items.filter((el) => el.template === "astrahan24").sort(sortFn);
  const local = items.filter((el) => el.template === "local").sort(sortFn);
  // сначала ast24, потом local, сортируем по названию
  return ast.concat(local);
};
