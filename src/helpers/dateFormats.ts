// Для использования даты в нативном календаре
// 2021-05-24T22:26:00.823+03:00 -> 2021-05-24T22:26
export function fullDateToHtmlDate(datestring: string): string {
  // Пустую дату не трогаем
  return datestring ? datestring.substr(0, 16) : "";
}

// Для преобразования даты из нативного календаря в формат сервера
// 2021-05-24T22:26 -> 2021-05-24T22:26:00.823+03:00
export function htmlDatetoFullDate(datestring: string): string {
  // Пустую дату не трогаем
  return datestring ? datestring + ":00+03:00" : "";
}
