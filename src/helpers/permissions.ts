import { store } from "../store";

export function can(permission: string): boolean {
  return store.state.user.role.permissions.includes(permission);
}

export function cant(permission: string): boolean {
  return !can(permission);
}
