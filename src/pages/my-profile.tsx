import Input from "@/components/form/Input";
import Text from "@/components/form/Text";
import Loading from "@/components/Loading";
import Modal from "@/components/Modal";
import api from "@/helpers/api";
import date from "@/helpers/date";
import { error, flash } from "@/helpers/notifications";
import { UserType } from "@/helpers/types";
import { useStore } from "@/store";
import {
  computed,
  defineComponent,
  onMounted,
  Ref,
  ref,
  Transition,
  withModifiers,
} from "vue";

interface ItemType extends UserType {
  role_id: number;
  password?: string;
  password_confirmation?: string;
}

export default defineComponent({
  name: "MyProfilePage",
  setup() {
    const store = useStore();
    const platformModalVisible = ref(false);
    const item = ref<ItemType>() as Ref<ItemType>;

    const userPlatforms = computed(() => {
      const platforms = [];
      for (const id of item.value.platform_ids) {
        const platform = store.state.platforms.find((el) => el.id === id);
        if (platform) platforms.push(platform);
      }
      return platforms;
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const user = { ...item.value };
        // delete empty strings
        if (!user.password) {
          delete user.password;
          delete user.password_confirmation;
        }
        await api.patch(`users/${item.value.id}`, { user });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    onMounted(async () => {
      const { data } = await api.get<UserType>("user");
      item.value = {
        ...data,
        role_id: data.role.id || 0,
        password: "",
        password_confirmation: "",
      };
    });

    return () => (
      <div class="container">
        <Transition name="fade-up">
          {platformModalVisible.value && (
            <Modal onClose={() => (platformModalVisible.value = false)}>
              <h1 class="mb-2">Мои издания</h1>
              <ul class="ml-6">
                {userPlatforms.value.map((el) => (
                  <li class="list-disc mb-2" key={el.id}>
                    {el.title}
                  </li>
                ))}
              </ul>
              <button
                class="mt-3"
                type="button"
                onClick={() => (platformModalVisible.value = false)}
              >
                Закрыть окно
              </button>
            </Modal>
          )}
        </Transition>
        {!item.value ? (
          <Loading />
        ) : (
          <div>
            <h1>Мои издания</h1>
            <ul class="ml-6">
              {userPlatforms.value
                .filter((el, i) => i < 5)
                .map((el) => (
                  <li class="list-disc mb-2" key={el.id}>
                    {el.title}
                  </li>
                ))}
            </ul>
            {userPlatforms.value.length > 5 && (
              <button
                type="button"
                onClick={() => (platformModalVisible.value = true)}
              >
                Показать все издания
              </button>
            )}
            <h1 class="mt-6">Мой профиль</h1>
            <form onSubmit={withModifiers(send, ["prevent"])}>
              <Input
                required
                value={item.value.first_name}
                onChange={(value) => (item.value.first_name = value)}
              >
                Имя
              </Input>
              <Input
                required
                value={item.value.last_name}
                onChange={(value) => (item.value.last_name = value)}
              >
                Фамилия
              </Input>
              <Input
                required
                type="email"
                value={item.value.email}
                onChange={(value) => (item.value.email = value)}
              >
                Почта
              </Input>
              <Input
                required
                value={item.value.phone}
                onChange={(value) => (item.value.phone = value)}
              >
                Телефон
              </Input>
              <Text value={item.value.role.title}>Роль</Text>
              <Text value={date(item.value.created_at)}>Дата создания</Text>
              <Text value={date(item.value.last_sign_in_at)}>
                Дата последней авторизации
              </Text>
              <Input
                type="password"
                value={item.value.password}
                onChange={(value) => (item.value.password = value)}
              >
                Пароль
              </Input>
              <Input
                type="password"
                value={item.value.password_confirmation}
                onChange={(value) => (item.value.password_confirmation = value)}
              >
                Пароль еще раз
              </Input>
              <button class="btn green">Сохранить</button>
            </form>
          </div>
        )}
      </div>
    );
  },
});
