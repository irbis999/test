import { MatterType, MatterUsersType } from "@/helpers/types";
import {
  defineComponent,
  onBeforeUnmount,
  onMounted,
  ref,
  withModifiers,
  watch,
} from "vue";
import { can, cant } from "@/helpers/permissions";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import { RouterLink, useRoute, useRouter } from "vue-router";
import Loading from "@/components/Loading";
import styles from "../../styles/pages/Matter.module.scss";
import MainBlock from "@/components/matter/MainBlock";
import AsideBlock from "@/components/matter/AsideBlock";
import { fullDateToHtmlDate, htmlDatetoFullDate } from "@/helpers/dateFormats";
import date from "@/helpers/date";
import { format } from "date-fns";
import { onBeforeRouteLeave } from "vue-router";

const leaveMessage =
  "Подтвердите уход со страницы. Несохраненные данные будут потеряны.";
const beforeUnload = (e: { returnValue: string }) =>
  (e.returnValue = leaveMessage);

export default defineComponent({
  name: "MatterPage",
  beforeRouteEnter() {
    if (cant("manage_matters") && cant("manage_all_matters"))
      return "/my-profile";
  },
  setup() {
    const route = useRoute();
    const router = useRouter();
    const matter = ref<MatterType>();
    const matterUsers = ref<MatterUsersType>([]);
    const button = ref("");
    const wasPublishedOnFetch = ref(false);
    const publishTimeOnFetch = ref("");
    const dirty = ref(false);

    onBeforeRouteLeave(() => (dirty.value ? confirm(leaveMessage) : true));

    function dirtyStartWatch() {
      // При изменении материала не даем уйти со страницы
      const unwatch = watch(
        matter,
        () => {
          dirty.value = true;
          window.addEventListener("beforeunload", beforeUnload);
          unwatch();
        },
        { deep: true }
      );
    }

    function dirtyStopWatch() {
      dirty.value = false;
      window.removeEventListener("beforeunload", beforeUnload);
    }

    function setMatter(mat: MatterType) {
      if (mat.tickets)
        mat.tickets.forEach((el, index) => (el.position = index));
      // Нужно убрать секунды и временную зону, чтобы работал нативный календарь
      if (mat.published_at)
        mat.published_at = fullDateToHtmlDate(mat.published_at);
      matter.value = mat;
      // Был ли опубликован материал на момент его получения
      wasPublishedOnFetch.value = ["planned", "published"].includes(
        matter.value.status
      );
      // Дата публикация на момент получения материала
      publishTimeOnFetch.value = matter.value.published_at;
    }

    let busy = false;
    async function updateMatter() {
      if (busy || !matter.value) return;
      if (cant("edit_matters"))
        return error("У вас нет прав редактировать материалы");
      const confirmMsgDate =
        button.value === "publishNow"
          ? "сейчас"
          : `на дату: ${date(matter.value.published_at)}`;
      const confirmMsg = `Вы действительно хотите опубликовать материал ${confirmMsgDate} ?`;
      // Материал публикуется
      const matterHasPublishStatus =
        ["planned", "published"].includes(matter.value.status) ||
        ["publish", "publishNow"].includes(button.value);
      let isPublishing = matterHasPublishStatus && !wasPublishedOnFetch.value;
      // У опубликованного материала меняется время публикации
      if (
        matterHasPublishStatus &&
        publishTimeOnFetch.value !== matter.value.published_at
      )
        isPublishing = true;
      if (isPublishing && !confirm(confirmMsg)) return;
      busy = true;
      let message = "Материал обновлен";
      if (button.value === "preview") {
        await preview();
        button.value = "";
        busy = false;
        return;
      }
      if (button.value === "operation") matter.value.status = "operation";
      if (button.value === "publish") {
        const now = new Date();
        const date = new Date(matter.value.published_at);
        matter.value.status = date > now ? "planned" : "published";
        message = date > now ? "Материал запланирован" : "Материал опубликован";
      }
      if (button.value === "publishNow") {
        message = "Материал опубликован";
        matter.value.status = "published";
      }
      // Добавляем временную зону Уфы для всех случаев кроме Опубликовать сейчас
      const published_at =
        button.value === "publishNow"
          ? format(new Date(), "yyyy-MM-dd'T'HH:mm:ssxxxxx")
          : htmlDatetoFullDate(matter.value.published_at);
      button.value = "";
      try {
        const { data } = await api.patch<MatterType>(
          `matters/${matter.value.id}`,
          {
            matter: { ...matter.value, published_at },
          }
        );
        setMatter(data);
        dirtyStopWatch();
        setTimeout(dirtyStartWatch, 500);
        flash(message);
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function removeMatter() {
      if (!matter.value) return;
      if (busy || !confirm("Подтвердите удаление материала")) return;
      busy = true;
      try {
        await api.delete(`matters/${matter.value.id}`);
        dirtyStopWatch();
        await router.push("/matters");
        flash("Материал удален");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function toggleLock() {
      if (busy || !matter.value) return;
      busy = true;
      try {
        const is_locked = !matter.value.is_locked;
        const message = is_locked
          ? "Материал заблокирован"
          : "Материал разблокировн";
        await api.patch(`matters/${matter.value.id}`, {
          matter: { ...matter.value, is_locked },
        });
        matter.value.is_locked = is_locked;
        flash(message);
        dirtyStopWatch();
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function preview() {
      try {
        interface PreviewType {
          data: { preview_id: number; host: string };
        }
        const { data } = await api.post<PreviewType>("previews", {
          matter: matter.value,
        });
        window.open(`${data.data.host}/preview/${data.data.preview_id}`);
      } catch (e) {
        error(e);
      }
    }

    async function getUsers() {
      try {
        const { data } = await api.post<{ users: MatterUsersType }>(
          "matters/users",
          {
            id: route.params.id,
          }
        );
        matterUsers.value = data.users;
      } catch (e) {
        error(e);
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    const intervalId = setInterval(getUsers, 10000);
    onMounted(async () => {
      try {
        type ErrorType = { message: string; error: boolean };
        const { data } = await api.get<MatterType & ErrorType>(
          `matters/${route.params.id}`
        );
        if (data.error) return error(data.message);
        setMatter(data);
        // Компоненты могут доставлять недостающие свойства при инициализации, даем им время отработать
        setTimeout(dirtyStartWatch, 5000);
        void getUsers();
      } catch (e) {
        error(e);
      }
    });

    onBeforeUnmount(() => {
      clearInterval(intervalId);
      window.removeEventListener("beforeunload", beforeUnload);
    });

    return () => (
      <div class="container mb-20">
        {matter.value ? (
          <form onSubmit={withModifiers(updateMatter, ["prevent"])}>
            {/* Кнопки отправки материала должны идти до предпросмотра, иначе по enter будет работать предпросмотр */}
            {can("edit_matters") && (
              <div class="bottom-buttons">
                {/* Материал заблокирован, прав для разблокировки нет */}
                {matter.value.is_locked && !matter.value.can_unlock && (
                  <div class="btn disabled">Материал заблокирован</div>
                )}
                {/* Материал не заблокировн либо есть права на разблокировку */}
                {(!matter.value.is_locked || matter.value.can_unlock) && (
                  <>
                    <button class="btn green">Сохранить</button>
                    {can("publish_matters") &&
                      matter.value.status === "published" && (
                        <button
                          class="btn green"
                          onClick={() => (button.value = "operation")}
                        >
                          Вернуть в работу
                        </button>
                      )}
                    {can("publish_matters") &&
                      matter.value.status !== "published" && (
                        <>
                          <button
                            class="btn green"
                            onClick={() => (button.value = "publish")}
                          >
                            Опубликовать
                          </button>
                          <button
                            class="btn green"
                            onClick={() => (button.value = "publishNow")}
                          >
                            Опубликовать сейчас
                          </button>
                        </>
                      )}
                    {/* Материал заблокирован, права на разблокировку есть */}
                    {matter.value.is_locked && matter.value.can_unlock && (
                      <button
                        class="btn blue"
                        type="button"
                        onClick={toggleLock}
                      >
                        Разблокировать
                      </button>
                    )}
                    {/* Материал не заблокирован */}
                    {!matter.value.is_locked && (
                      <button
                        class="btn red"
                        type="button"
                        onClick={toggleLock}
                      >
                        Заблокировать
                      </button>
                    )}
                  </>
                )}
              </div>
            )}
            <div class={styles.top}>
              <div class="flex items-center text-red-600">
                <span class="m-icon">edit</span>
                <span>{matterUsers.value}</span>
              </div>
              <div class={styles.topLinks}>
                {matter.value.type === "Matter::Survey" && (
                  <RouterLink
                    to={`/matters/test-survey-results/${matter.value.id}?type=survey`}
                  >
                    Результаты
                  </RouterLink>
                )}
                {matter.value.type === "Matter::Test" && (
                  <RouterLink
                    to={`/matters/test-survey-results/${matter.value.id}?type=test`}
                  >
                    Результаты
                  </RouterLink>
                )}
                {matter.value.url && (
                  <a href={matter.value.url} target="_blank">
                    На сайте
                  </a>
                )}
                <button onClick={() => (button.value = "preview")}>
                  Предпросмотр
                </button>
                <RouterLink to={`/history/${matter.value.id}`}>
                  История
                </RouterLink>
                <RouterLink
                  class="btn m-icon"
                  to={`/revisions/${matter.value.id}`}
                >
                  restore
                </RouterLink>
                <button
                  type="button"
                  class="btn red m-icon"
                  onClick={removeMatter}
                >
                  delete
                </button>
              </div>
            </div>
            <div class="matter-cols">
              <MainBlock matter={matter.value} />
              <AsideBlock matter={matter.value} />
            </div>
          </form>
        ) : (
          <Loading />
        )}
      </div>
    );
  },
});
