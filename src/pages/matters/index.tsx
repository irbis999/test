import {
  computed,
  defineComponent,
  onBeforeUnmount,
  onMounted,
  ref,
} from "vue";
import axios, { Canceler } from "axios";
import { cant } from "@/helpers/permissions";
import { useStore } from "@/store";
import { MatterType, MatterUsersType } from "@/helpers/types";
import { error } from "@/helpers/notifications";
import api from "@/helpers/api";
import styles from "../../styles/pages/Matters.module.scss";
import device from "@/helpers/device";
import { DropdownNumber, DropdownString } from "@/components/Dropdown";
import DropdownAjax from "@/components/Dropdown/DropdownAjax";
import Datepicker from "@/components/Datepicker";
import MattersPageColumn from "@/components/MattersPageColumn";
import Loading from "@/components/Loading";
import Search from "@/components/Search";
import { PlatformsSelectMultiple } from "@/components/PlatformsSelect";

const emptyParams = {
  status: [] as string[],
  platform_id: [] as number[],
  type: [] as string[],
  data_start: "",
  date_end: "",
  user_id: [] as number[],
  author_id: [] as number[],
  q: "",
};

type ItemsType = {
  [key: string]: MatterType[];
};

export default defineComponent({
  name: "MattersPage",
  beforeRouteEnter() {
    if (cant("manage_matters") && cant("manage_all_matters"))
      return "/my-profile";
  },
  setup() {
    const store = useStore();
    const matterUsers = ref<MatterUsersType>([]);
    const savedParams = localStorage.getItem("indexParams");
    const initialParams = savedParams
      ? (JSON.parse(savedParams) as typeof emptyParams)
      : { ...emptyParams };
    const params = ref(initialParams);
    const loaded = ref(false);
    const items = ref<ItemsType>({});
    const filtersVisivle = ref(device.isDesktop);
    const cancel = ref<Canceler>();

    const columns = computed(() => {
      return params.value.status.length
        ? store.state.statuses.filter((el) =>
            params.value.status.includes(el.id)
          )
        : store.state.statuses;
    });

    async function getUsers() {
      try {
        const { data } = await api.get<{ users: MatterUsersType }>(
          "matters/users"
        );
        matterUsers.value = data.users;
      } catch (e) {
        error(e);
      }
    }

    function saveParams() {
      localStorage.setItem("indexParams", JSON.stringify(params.value));
    }

    async function getItems(reset = false) {
      const order = {} as { [key: string]: string };
      for (const status of store.state.statuses) {
        order[status.id] =
          localStorage.getItem(`indexSort_${status.id}`) || "created_at,desc";
      }
      if (reset) {
        params.value = { ...emptyParams };
        window.dispatchEvent(new Event("clearDatepicker"));
      }
      const prms = {
        ...params.value,
        order,
        limit: 100,
      };
      saveParams();
      if (cancel.value) cancel.value();
      loaded.value = false;
      try {
        const { data } = await api.get<ItemsType>("matters/overall", {
          params: prms,
          cancelToken: new axios.CancelToken((c) => (cancel.value = c)),
        });
        items.value = data;
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    }

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    const intervalId = setInterval(getUsers, 10000);
    onMounted(() => {
      void getUsers();
      void getItems();
    });
    onBeforeUnmount(() => clearInterval(intervalId));

    return () => (
      <div>
        <div class={`submenu __dense ${styles.submenu}`}>
          {device.isMobile && (
            <button
              type="button"
              class="s-item btn"
              style={{ flexBasis: "calc(100% - 14px)" }}
              onClick={() => (filtersVisivle.value = !filtersVisivle.value)}
            >
              Фильтры {filtersVisivle.value ? "↑" : "↓"}
            </button>
          )}
          {filtersVisivle.value && (
            <>
              <PlatformsSelectMultiple
                class="s-item"
                value={params.value.platform_id}
                onChange={(value) => {
                  params.value.platform_id = value;
                  void getItems();
                }}
              />
              <DropdownString
                class="s-item"
                value={params.value.status}
                options={store.state.statuses}
                onChange={(value) => {
                  params.value.status = value;
                  void getItems();
                }}
              >
                Статусы
              </DropdownString>
              <DropdownString
                class="s-item"
                value={params.value.type}
                options={store.state.matter_types}
                onChange={(value) => {
                  params.value.type = value;
                  void getItems();
                }}
              >
                Типы
              </DropdownString>
              <DropdownNumber
                class="s-item"
                value={params.value.user_id}
                options={store.state.users.map((el) => ({
                  id: el.id,
                  title: el.full_name,
                }))}
                onChange={(value) => {
                  params.value.user_id = value;
                  void getItems();
                }}
              >
                Пользователи
              </DropdownNumber>
              <DropdownAjax
                class="s-item"
                value={params.value.author_id}
                endpoint="authors"
                onChange={(value) => {
                  params.value.author_id = value;
                  void getItems();
                }}
              >
                Авторы
              </DropdownAjax>
              <Search
                class="s-item"
                lazy
                value={params.value.q}
                onChange={(value) => {
                  params.value.q = value;
                  void getItems();
                }}
              />
              <Datepicker
                onChange={(dates) => {
                  params.value.data_start = dates.dateStart;
                  params.value.date_end = dates.dateEnd;
                  void getItems();
                }}
              />
              <button
                type="button"
                class="s-item btn"
                onClick={() => getItems(true)}
              >
                Сбросить
              </button>
            </>
          )}
        </div>
        {loaded.value ? (
          <div
            class={`${styles.columns} ${styles[`__${columns.value.length}`]}`}
          >
            {columns.value.map((column, i) => (
              <MattersPageColumn
                key={i}
                status={column}
                params={params.value}
                matters={items.value[column.id]}
                mattersUsers={matterUsers.value}
              />
            ))}
          </div>
        ) : (
          <Loading />
        )}
      </div>
    );
  },
});
