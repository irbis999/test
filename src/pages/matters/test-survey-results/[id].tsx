import Loading from "@/components/Loading";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import { defineComponent, onMounted, ref } from "vue";
import { RouterLink, useRoute } from "vue-router";

interface ResultsType {
  total_started: number;
  total_completed: number;
  questions: {
    text: string;
    answers: {
      answers: { text: string; rate: number }[];
    };
  }[];
}

export default defineComponent({
  name: "TestResultPage",
  setup() {
    const route = useRoute();
    const results = ref<ResultsType>();

    onMounted(async () => {
      try {
        const params = { id: route.params.id };
        const endpoint =
          route.query.type === "test" ? "results/test" : "results/survey";
        const { data } = await api.get<{ results: ResultsType }>(endpoint, {
          params,
        });
        results.value = data.results;
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div class="container">
        <div class="control-bar">
          <RouterLink
            class="btn m-icon mr-4"
            to={`/matters/${route.params.id}`}
          >
            arrow_back
          </RouterLink>
          <div class="font-bold text-2xl mr-auto">Результаты</div>
        </div>
        {results.value ? (
          <div>
            <h3 class="font-bold my-4 text-lg">Общие</h3>
            <div class="my-2">Начали опрос: {results.value.total_started}</div>
            <div class="my-2">
              Завершили опрос: {results.value.total_completed}
            </div>
            <h3 class="font-bold my-4 text-lg">По вопросам</h3>
            {results.value.questions.map((q, i) => (
              <div key={i}>
                <div class="my-2">{q.text}</div>
                <ul class="list-disc ml-6 mb-6">
                  {q.answers.answers.map((a, i) => (
                    <li key={i} class="mb-2">
                      {a.text}: {a.rate}
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        ) : (
          <Loading />
        )}
      </div>
    );
  },
});
