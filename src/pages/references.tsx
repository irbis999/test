import { defineComponent } from "vue";
import { RouterLink, RouterView, useRoute } from "vue-router";
import { can } from "@/helpers/permissions";

export default defineComponent({
  name: "ReferencesPage",
  setup() {
    const route = useRoute();

    return () => (
      <div>
        <div class="submenu __scroll">
          {can("manage_platforms") && (
            <RouterLink class="s-item" to="/references/platforms">
              Издания
            </RouterLink>
          )}
          {can("show_users") && (
            <RouterLink class="s-item" to="/references/users">
              Пользователи
            </RouterLink>
          )}
          {can("manage_global_autoscrolls") && (
            <RouterLink class="s-item" to="/references/autoscroll">
              Сквозной автодоскрол
            </RouterLink>
          )}
          {can("manage_global_teaser_blocks") && (
            <RouterLink class="s-item" to="/references/teaser-blocks">
              Сквозные тизерные блоки
            </RouterLink>
          )}
          {can("manage_roles") && (
            <RouterLink class="s-item" to="/references/roles">
              Роли
            </RouterLink>
          )}
        </div>
        <RouterView class="container" key={route.fullPath} />
      </div>
    );
  },
});
