import { defineComponent, ref } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import InfScroll from "@/components/InfScroll";
import Loading from "@/components/Loading";
import NotificationPageItem, {
  ItemType,
} from "@/components/NotificationPageItem";
import { cant } from "@/helpers/permissions";
import device from "@/helpers/device";
import { DropdownNumber } from "@/components/Dropdown";
import { useStore } from "@/store";
import { SelectString } from "@/components/form/Select";
import Datepicker from "@/components/Datepicker";
import styles from "../styles/pages/notifications.module.scss";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";

export default defineComponent({
  name: "NotificationsPage",
  beforeRouteEnter() {
    if (cant("manage_global_notifications")) return "/";
  },
  setup() {
    const store = useStore();
    const orderOptions = [
      { id: "created_at,desc", title: "По дате создания ↓" },
      { id: "created_at,asc", title: "По дате создания ↑" },
    ];
    const apiParams = ref({
      platform_id: [] as number[],
      user_id: [] as number[],
      order: orderOptions[0].id,
      date_start: "",
      date_end: "",
    });
    const filtersVisible = ref(device.isDesktop);
    const { items, pending, busy, fetch } = useInfScroll<ItemType>(
      "global_notifications",
      apiParams
    );

    let readAllbusy = false;
    async function readAll() {
      if (readAllbusy) return;
      readAllbusy = true;
      try {
        await api.post("notifications/all");
        items.value.forEach((el) => (el.is_read = true));
        store.commit("RESET_NOTIFICATIONS_COUNT");
      } catch (e) {
        error(e);
      }
      readAllbusy = false;
    }

    return () => (
      <div>
        <div class={`submenu ${styles.menu}`}>
          {device.isMobile && (
            <button
              type="button"
              class="s-item btn text-center"
              style={{ flexBasis: "calc(100% - 14px)" }}
              onClick={() => (filtersVisible.value = !filtersVisible.value)}
            >
              Фильтры {filtersVisible.value ? "↑" : "↓"}
            </button>
          )}
          {filtersVisible.value && (
            <>
              <DropdownNumber
                options={store.state.platforms}
                class={`s-item ${styles.item}`}
                value={apiParams.value.platform_id}
                onChange={(value) => {
                  apiParams.value.platform_id = value;
                  fetch(true);
                }}
              >
                Издания
              </DropdownNumber>
              <DropdownNumber
                options={store.state.users.map((el) => ({
                  id: el.id,
                  title: el.full_name,
                }))}
                class={`s-item ${styles.item}`}
                value={apiParams.value.user_id}
                onChange={(value) => {
                  apiParams.value.user_id = value;
                  fetch(true);
                }}
              >
                Пользователи
              </DropdownNumber>
              <Datepicker
                style={{ width: "230px" }}
                class={`s-item ${styles.item}`}
                onChange={({ dateStart, dateEnd }) => {
                  apiParams.value.date_start = dateStart;
                  apiParams.value.date_end = dateEnd;
                  fetch(true);
                }}
              />
              <SelectString
                class={`s-item ${styles.item}`}
                value={apiParams.value.order}
                options={orderOptions}
                onChange={(value) => {
                  apiParams.value.order = value;
                  fetch(true);
                }}
              />
            </>
          )}
        </div>
        <div class="container">
          <button class="btn mb-4" type="button" onClick={readAll}>
            Отметить все уведомления прочитанными
          </button>
          {items.value.map((el) => (
            <NotificationPageItem item={el} key={el.id} />
          ))}
          {pending.value && <Loading />}
          <InfScroll onLoad={() => fetch()} busy={busy.value} />
        </div>
      </div>
    );
  },
});
