import { computed, defineComponent, ref, withKeys, withModifiers } from "vue";
import { cant } from "@/helpers/permissions";
import { useStore } from "@/store";
import sortPlatforms from "@/helpers/sortPlatforms";
import styles from "../styles/pages/report.module.scss";
import Search from "@/components/Search";
import Input from "@/components/form/Input";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";

export default defineComponent({
  name: "ReportPage",
  beforeRouteEnter() {
    if (cant("create_reports")) return "/";
  },
  setup() {
    const store = useStore();
    const email = ref("");
    const q = ref("");
    const user = store.state.user;
    const platforms = computed(() => {
      return sortPlatforms(store.state.platforms)
        .filter((el) => user.platform_ids.includes(el.id))
        .filter((el) => el.title.toLowerCase().includes(q.value.toLowerCase()));
    });
    const reportOptions = [
      { id: "matters", title: "Экспорт материалов" },
      { id: "overall", title: "Общая статистика" },
      { id: "reads", title: "Количество прочтений" },
      { id: "feedbacks", title: "Обратная связь" },
    ];
    const params = ref({
      platform_id: [] as number[],
      type: [] as string[],
      report_type: "matters",
      date_start: "",
      date_end: "",
      email: [user.email],
    });

    function isSelected(
      items: (number | string)[],
      item: { id: number | string }
    ) {
      return items.find((el) => el === item.id);
    }

    function select(items: (number | string)[], item: { id: number | string }) {
      isSelected(items, item)
        ? (items = items.splice(items.indexOf(item.id), 1))
        : items.push(item.id);
    }

    function togglePlatforms() {
      params.value.platform_id.length
        ? (params.value.platform_id = [])
        : (params.value.platform_id = platforms.value.map((el) => el.id));
    }

    function addEmail(e: Event) {
      e.preventDefault();
      if (!email.value) return;
      params.value.email.push(email.value);
      email.value = "";
    }

    let busy = false;
    async function send() {
      if (busy) return;
      if (!params.value.email.length)
        return error("Пожалуйста, укажите email получателей отчета");
      busy = true;
      try {
        await api.post("reports", params.value);
        flash("Отчет сформирован");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <form onSubmit={withModifiers(send, ["prevent"])} class="container mb-10">
        <div class={styles.cols}>
          <div class={`${styles.col} ${styles.__scroll}`}>
            <div class="sticky top-0 bg-white pb-1">
              <div class="mb-4 flex justify-between items-center">
                <span>Издание</span>
                <button type="button" class="text-sm" onClick={togglePlatforms}>
                  Выбрать/Сбросить все
                </button>
              </div>
              <Search
                value={q.value}
                onChange={(value) => (q.value = value)}
                class="mb-3"
              />
            </div>
            {platforms.value.map((el) => (
              <button
                type="button"
                class={styles.item}
                key={el.id}
                onClick={() => select(params.value.platform_id, el)}
              >
                <span class="m-icon">
                  {isSelected(params.value.platform_id, el)
                    ? "check_box"
                    : "check_box_outline_blank"}
                </span>
                <span>{el.title}</span>
              </button>
            ))}
          </div>
          <div
            class={{
              [styles.col]: true,
              "opacity-0": params.value.report_type === "feedbacks",
            }}
          >
            <div class="mb-4">Тип материала</div>
            {store.state.matter_types.map((el) => (
              <button
                type="button"
                key={el.id}
                class={styles.item}
                onClick={() => select(params.value.type, el)}
              >
                <span class="m-icon">
                  {isSelected(params.value.type, el)
                    ? "check_box"
                    : "check_box_outline_blank"}
                </span>
                <span>{el.title}</span>
              </button>
            ))}
          </div>
          <div class={styles.col}>
            <div class="mb-4">Тип отчета</div>
            {reportOptions.map((el) => (
              <button
                type="button"
                key={el.id}
                class={styles.item}
                onClick={() => (params.value.report_type = el.id)}
              >
                <span class="m-icon">
                  {el.id === params.value.report_type
                    ? "check_box"
                    : "check_box_outline_blank"}
                </span>
                <span>{el.title}</span>
              </button>
            ))}
          </div>
          <div class={styles.col}>
            <div class="mb-4">Период</div>
            <Input
              value={params.value.date_start}
              onChange={(value) => (params.value.date_start = value)}
              type="date"
            >
              От
            </Input>
            <Input
              value={params.value.date_end}
              onChange={(value) => (params.value.date_end = value)}
              type="date"
            >
              До
            </Input>
            <div class="f-label">Email получателей</div>
            <input
              class="f-input"
              type="email"
              v-model={email.value}
              placeholder="Введите email, нажмите enter"
              onKeydown={withKeys(addEmail, ["enter"])}
            />
            <div class={styles.emails}>
              {params.value.email.map((el, i) => (
                <div class={styles.email}>
                  <span>{el}</span>
                  <button
                    type="button"
                    class="m-icon"
                    onClick={() => params.value.email.splice(i, 1)}
                  >
                    close
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div class="bottom-buttons">
          <button class="btn green">Создать и отправить отчет</button>
        </div>
      </form>
    );
  },
});
