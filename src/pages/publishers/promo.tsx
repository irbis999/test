import { defineComponent, onMounted, ref, withModifiers } from "vue";
import { cant } from "@/helpers/permissions";
import MattersCol from "@/components/MattersCol";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import MatterCell, { MatterCellType } from "@/components/MatterCell";
import device from "@/helpers/device";

export default defineComponent({
  name: "PromoPage",
  beforeRouteEnter() {
    if (cant("manage_promo_blocks")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const seeAlso = ref<MatterCellType[]>([]);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<{ see_also: MatterCellType[] }>(
          "promo_config",
          {
            params,
          }
        );
        seeAlso.value = data.see_also;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const promo_config = {
          platform_id: props.platformId,
          see_also: seeAlso.value,
        };
        await api.patch("promo_config", {
          promo_config,
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="container publisher-container">
        {device.isDesktop && <MattersCol platformId={props.platformId} />}
        <div>
          <form class="mb-16" onSubmit={withModifiers(send, ["prevent"])}>
            <h2>Читайте также</h2>
            <div class="publisher-matters-grid">
              {seeAlso.value.map((el, i) => (
                <MatterCell key={i} item={el} platformId={props.platformId} />
              ))}
            </div>
            <div class="bottom-buttons">
              <button class="btn green">Обновить</button>
            </div>
          </form>
        </div>
      </div>
    );
  },
});
