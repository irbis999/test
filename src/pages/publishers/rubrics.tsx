import { defineComponent, ref, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import RubricForm, { RubricType } from "@/components/publishers/RubricForm";
import RubricItem from "@/components/publishers/RubricItem";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import Search from "@/components/Search";
import Modal from "@/components/Modal";

export default defineComponent({
  name: "RubricsPage",
  beforeRouteEnter() {
    if (cant("manage_rubrics")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<RubricType>(
      "rubrics",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <RubricForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          />
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <RubricItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
