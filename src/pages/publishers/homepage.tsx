import { computed, defineComponent, onMounted, ref, withModifiers } from "vue";
import { cant } from "@/helpers/permissions";
import MatterCell, { MatterCellType } from "@/components/MatterCell";
import StoryCell, { StoryCellType } from "@/components/StoryCell";
import api from "@/helpers/api";
import { error, flash } from "@/helpers/notifications";
import { useStore } from "@/store";
import MattersCol from "@/components/MattersCol";
import device from "@/helpers/device";

interface ItemsType {
  day_picture: MatterCellType[];
  see_also: MatterCellType[];
  video_galleries: MatterCellType[];
  stories: StoryCellType[];
}

export default defineComponent({
  name: "HomePage",
  beforeRouteEnter() {
    if (cant("manage_main_pages")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const store = useStore();
    const items = ref<ItemsType>({
      day_picture: [],
      see_also: [],
      video_galleries: [],
      stories: [],
    });

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<ItemsType>("main_page_config", {
          params,
        });
        items.value = data;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const main_page_config = {
          platform_id: props.platformId,
          ...items.value,
        };
        await api.patch("main_page_config", { main_page_config });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    const template = computed(() => {
      const platform = store.state.platforms.find(
        (el) => el.id === props.platformId
      );
      return platform?.template;
    });

    return () => (
      <div class="container publisher-container">
        {device.isDesktop && <MattersCol platformId={props.platformId} />}
        <div>
          <form class="mb-16" onSubmit={withModifiers(send, ["prevent"])}>
            <h2>Сюжеты</h2>
            <div class="publisher-matters-grid ">
              {items.value.stories.map((el, i) => (
                <StoryCell key={i} item={el} platformId={props.platformId} />
              ))}
            </div>
            <h2>Картина дня</h2>
            <div class="publisher-matters-grid">
              {items.value.day_picture.map((el, i) => (
                <MatterCell
                  key={i}
                  item={el}
                  platformId={props.platformId}
                  type={store.state.autoscrollAllowedTypes}
                  dropErrorType="Новость, Статья, Мнение, Фотогалерея, Видео, Подкаст"
                />
              ))}
            </div>
            <h2>Читайте также</h2>
            <div class="publisher-matters-grid">
              {items.value.see_also.map((el, i) => (
                <MatterCell key={i} item={el} platformId={props.platformId} />
              ))}
            </div>
            {template.value === "astrahan24" && (
              <div>
                <h2>Видео</h2>
                <div class="publisher-matters-grid">
                  {items.value.video_galleries.map((el, i) => (
                    <MatterCell
                      key={i}
                      item={el}
                      platformId={props.platformId}
                      dropErrorType="Видео"
                      type={["Matter::Video"]}
                    />
                  ))}
                </div>
              </div>
            )}
            <div class="bottom-buttons">
              <button class="btn green">Сохранить</button>
            </div>
          </form>
        </div>
      </div>
    );
  },
});
