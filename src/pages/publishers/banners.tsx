import { defineComponent, ref, Transition } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import { cant } from "@/helpers/permissions";
import BannerForm, { BannerType } from "@/components/publishers/BannerForm";
import BannerItem from "@/components/publishers/BannerItem";
import Modal from "@/components/Modal";
import InfScroll from "@/components/InfScroll";
import Loading from "@/components/Loading";

export default defineComponent({
  name: "BannersPage",
  beforeRouteEnter() {
    if (cant("manage_banners")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<BannerType>(
      "banners",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <BannerForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <BannerItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
