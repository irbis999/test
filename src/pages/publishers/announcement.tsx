import { defineComponent, onMounted, Ref, ref, withModifiers } from "vue";
import { cant } from "@/helpers/permissions";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import Loading from "@/components/Loading";
import Input from "@/components/form/Input";
import Checkbox from "@/components/form/Checkbox";
import ImageBlock from "@/components/ImageBlock";
import { ImageType } from "@/helpers/types";

interface AnnouncementType {
  title: string;
  url: string;
  description: string;
  image: ImageType;
  is_active: boolean;
}

export default defineComponent({
  name: "AnnouncementPage",
  beforeRouteEnter() {
    if (cant("manage_announcements")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref() as Ref<AnnouncementType>;

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<AnnouncementType>("announcement", {
          params,
        });
        item.value = data;
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      if (!item.value.image.url)
        return error("Пожалуйста, выберите изображение анонса");
      busy = true;
      try {
        await api.patch("announcement", {
          announcement: item.value,
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <div class="container">
          <form onSubmit={withModifiers(send, ["prevent"])} class="mb-16">
            <h2>Анонс</h2>
            <Input
              value={item.value.title}
              onChange={(value) => (item.value.title = value)}
              required
            >
              Заголовок
            </Input>
            <Input
              type="url"
              value={item.value.url}
              onChange={(value) => (item.value.url = value)}
              required
            >
              Ссылка
            </Input>
            <Input
              value={item.value.description}
              onChange={(value) => (item.value.description = value)}
            >
              Описание
            </Input>
            <div class="f-label flex">
              <span>Изображение</span>
              <span class="text-red-700 ml-1">*</span>
            </div>
            <ImageBlock
              class="mb-4"
              image={item.value.image}
              watermark={false}
            />
            <Checkbox
              value={item.value.is_active}
              onChange={(value) => (item.value.is_active = value)}
            >
              Анонс активен
            </Checkbox>
            <div class="bottom-buttons">
              <button class="btn green">Сохранить</button>
            </div>
          </form>
        </div>
      ) : (
        <Loading />
      );
  },
});
