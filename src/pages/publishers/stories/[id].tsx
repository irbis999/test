import api from "@/helpers/api";
import { MatterType as MType, StoryType } from "@/helpers/types";
import { defineComponent, onMounted, ref, Transition, Ref } from "vue";
import { useRoute } from "vue-router";
import useInfScroll from "@/helpers/useInfScroll";
import MattersCol from "@/components/MattersCol";
import device from "@/helpers/device";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import { error, flash } from "@/helpers/notifications";
import styles from "../../../styles/pages/publishers/StoryMatters.module.scss";
import Modal from "@/components/Modal";
import StoryMatterForm from "@/components/publishers/StoryMatterForm";

interface MatterType extends MType {
  is_main: boolean;
}

export default defineComponent({
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const story = ref() as Ref<StoryType>;
    const modalVisible = ref(false);
    const route = useRoute();
    const { items, pending, busy, fetch } = useInfScroll<MatterType>(
      `stories/${route.params.id}/matters`
    );

    onMounted(async () => {
      const { data } = await api.get<StoryType>(`stories/${route.params.id}`);
      story.value = data;
    });

    async function removeFromStory(item: MatterType, index: number) {
      if (!confirm("Подтвердите удаление материала из сюжета")) return;
      try {
        await api.delete(`stories/${story.value.id}/matters/${item.id}`);
        items.value.splice(index, 1);
        flash("Материал удален из сюжета");
      } catch (e) {
        error(e);
      }
    }

    async function toggleMain(item: MatterType) {
      try {
        const is_main = !item.is_main;
        await api.patch(`stories/${story.value.id}/matters/${item.id}`, {
          is_main,
        });
        item.is_main = is_main;
      } catch (e) {
        error(e);
      }
    }

    async function processDrop(e: DragEvent) {
      const str = e.dataTransfer?.getData("text/plain");
      if (!str) return;
      const matter = JSON.parse(str) as MatterType;
      matter.is_main = false;
      try {
        await api.post(`/stories/${story.value.id}/matters`, {
          id: matter.id,
        });
        items.value.push(matter);
        flash("Материал добавлен в сюжет");
      } catch (e) {
        error(e);
      }
    }

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <StoryMatterForm
                story={story.value}
                onAdd={(item) => items.value.push(item)}
              />
            </Modal>
          )}
        </Transition>
        <div class={styles.cols}>
          {device.isDesktop && <MattersCol platformId={props.platformId} />}
          <div onDragover={(e) => e.preventDefault()} onDrop={processDrop}>
            <div class="control-bar">
              <h2 class="mb-0">{story.value?.title}</h2>
              <button
                class="btn m-icon"
                title="Добавить материал в сюжет"
                onClick={() => (modalVisible.value = true)}
              >
                add_circle
              </button>
            </div>
            {items.value.map((el, i) => (
              <div key={i} class="list-item">
                <span>{el.title}</span>
                <button
                  type="button"
                  class={styles.checkbox}
                  onClick={() => toggleMain(el)}
                >
                  <div class="m-icon m-2">
                    {el.is_main ? "check_box" : "check_box_outline_blank"}
                  </div>
                  <span>Главный</span>
                </button>
                <button
                  type="button"
                  class="m-icon btn red"
                  onClick={() => removeFromStory(el, i)}
                >
                  delete
                </button>
              </div>
            ))}
            {pending.value && <Loading />}
            <InfScroll onLoad={() => fetch()} busy={busy.value} />
          </div>
        </div>
      </div>
    );
  },
});
