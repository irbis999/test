import { defineComponent, ref, onMounted } from "vue";
import { cant } from "@/helpers/permissions";
import { MenuSectionType } from "@/components/publishers/MenuSectionForm";
import MenuSections from "@/components/publishers/MenuSections";
import { error } from "@/helpers/notifications";
import api from "@/helpers/api";

export default defineComponent({
  name: "MenuItemsPage",
  beforeRouteEnter() {
    if (cant("manage_menu_items")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const items = ref<MenuSectionType[]>([]);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<MenuSectionType[]>("menu_sections", {
          params,
        });
        items.value = data.filter((el) => el.section_type === "main");
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div>
        <h1>Меню</h1>
        <MenuSections sections={items.value} platformId={props.platformId} />
      </div>
    );
  },
});
