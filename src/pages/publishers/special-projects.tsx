import { defineComponent, ref, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import SpecialProjectForm, {
  SpecialProjectType,
} from "@/components/publishers/SpecialProjectForm";
import SpecialProjectItem from "@/components/publishers/SpecialProjectItem";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import Search from "@/components/Search";
import Modal from "@/components/Modal";

export default defineComponent({
  name: "SpecialProjectsPage",
  beforeRouteEnter() {
    if (cant("manage_special_projects")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<SpecialProjectType>(
      "special_projects",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <SpecialProjectForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          ></Search>
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <SpecialProjectItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
