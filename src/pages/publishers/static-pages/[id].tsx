import { defineComponent, onMounted, ref, withModifiers, Ref } from "vue";
import { cant } from "@/helpers/permissions";
import Loading from "@/components/Loading";
import { ContentBlockType, MetaInformationType } from "@/helpers/types";
import Input from "@/components/form/Input";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import { useRoute, useRouter } from "vue-router";
import device from "@/helpers/device";
import MetaInformation from "@/components/MetaInformation";
import Checkbox from "@/components/form/Checkbox";
import ContentBlocks from "@/components/ContentBlocks";

interface StaticPageType {
  id: number;
  title: string;
  slug: string;
  is_active: boolean;
  meta_information_attributes: MetaInformationType;
  content_blocks: ContentBlockType[];
}

export default defineComponent({
  name: "StaticPagePage",
  beforeRouteEnter() {
    if (cant("manage_static_pages")) return "/";
  },
  setup() {
    const loaded = ref(false);
    const item = ref() as Ref<StaticPageType>;
    const route = useRoute();
    const router = useRouter();

    onMounted(async () => {
      try {
        const { data } = await api.get<StaticPageType>(
          `static_pages/${route.params.id}`
        );
        item.value = data;
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        await api.patch(`static_pages/${item.value.id}`, {
          static_page: item.value,
        });
        await router.push("/publishers/static-pages");
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <form onSubmit={withModifiers(send, ["prevent"])}>
          <div class="matter-cols">
            <div>
              <Input
                value={item.value.title}
                onChange={(value) => (item.value.title = value)}
                required
              >
                Заголовок
              </Input>
              <Input
                value={item.value.slug}
                onChange={(value) => (item.value.slug = value)}
                required
              >
                Слаг
              </Input>
              <ContentBlocks item={item.value} />
              {device.isDesktop && <button class="btn green">Сохранить</button>}
            </div>
            <div>
              <MetaInformation item={item.value} />
              <Checkbox
                value={item.value.is_active}
                onChange={(value) => (item.value.is_active = value)}
              >
                Страница активна
              </Checkbox>
              {device.isMobile && <button class="btn green">Сохранить</button>}
            </div>
          </div>
        </form>
      ) : (
        <Loading />
      );
  },
});
