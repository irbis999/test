import { defineComponent, ref, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import StaticPageForm, {
  StaticPageType,
} from "@/components/publishers/StaticPageForm";
import StaticPageItem from "@/components/publishers/StaticPageItem";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import Search from "@/components/Search";
import Modal from "@/components/Modal";

export default defineComponent({
  name: "ColumnistsPage",
  beforeRouteEnter() {
    if (cant("manage_static_pages")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<StaticPageType>(
      "static_pages",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <StaticPageForm platformId={props.platformId} />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          ></Search>
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <StaticPageItem
            item={el}
            key={el.id}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
