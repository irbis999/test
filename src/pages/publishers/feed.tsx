import {
  defineComponent,
  onMounted,
  ref,
  withModifiers,
  Transition,
} from "vue";
import { cant } from "@/helpers/permissions";
import FeedItem from "@/components/publishers/FeedItem";
import FeedForm, { ItemType } from "@/components/publishers/FeedForm";
import MattersCol from "@/components/MattersCol";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import MatterCell, { MatterCellType } from "@/components/MatterCell";
import Modal from "@/components/Modal";
import styles from "../../styles/pages/publishers/Feed.module.scss";
import device from "@/helpers/device";

export default defineComponent({
  name: "FeedPage",
  beforeRouteEnter() {
    if (cant("manage_news_feed") || cant("manage_news_feed_promo_blocks"))
      return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const items = ref<ItemType[]>([]);
    const promo = ref<MatterCellType[]>([]);
    const modalVisible = ref(false);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data: feedItems } = await api.get<ItemType[]>("news_feeds", {
          params,
        });
        items.value = feedItems;
        const { data: promoItems } = await api.get<{ promo: MatterCellType[] }>(
          "news_feed_promo_blocks",
          { params }
        );
        promo.value = promoItems.promo;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        const news_feed_promo_block = {
          platform_id: props.platformId,
          promo: promo.value,
        };
        await api.patch("news_feed_promo_blocks", {
          news_feed_promo_block,
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class={`container ${styles.page}`}>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <FeedForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        {device.isDesktop && <MattersCol platformId={props.platformId} />}
        <div>
          <form onSubmit={withModifiers(send, ["prevent"])}>
            <h2>Промо</h2>
            <div class={styles.matters}>
              {promo.value.map((el, i) => (
                <MatterCell key={i} item={el} platformId={props.platformId} />
              ))}
            </div>
            <button class="btn green">Обновить</button>
          </form>
          <h2 class="mt-6 mb-0">Вкладки</h2>
          {items.value.map((el) => (
            <FeedItem
              key={el.id}
              item={el}
              onUpdate={(item) =>
                items.value.splice(
                  items.value.findIndex((e) => e.id === el.id),
                  1,
                  item
                )
              }
              onRemove={() =>
                items.value.splice(
                  items.value.findIndex((e) => e.id === el.id),
                  1
                )
              }
            />
          ))}
          <button
            type="button"
            class="btn green mt-4"
            onClick={() => (modalVisible.value = true)}
          >
            Добавить вкладку
          </button>
        </div>
      </div>
    );
  },
});
