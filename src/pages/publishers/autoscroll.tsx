import { defineComponent, onMounted, ref } from "vue";
import { cant } from "@/helpers/permissions";
import MattersCol from "@/components/MattersCol";
import device from "@/helpers/device";
import AutoscrollItem, {
  AutoscrollItemType,
} from "@/components/publishers/AutoscrollItem";
import { fullDateToHtmlDate } from "@/helpers/dateFormats";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";

export default defineComponent({
  name: "AutoscrollPage",
  beforeRouteEnter() {
    if (cant("manage_autoscrolls")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const items = ref<AutoscrollItemType[]>([]);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<AutoscrollItemType[]>(
          "autoscroll_configs",
          { params }
        );
        for (const item of data) {
          // Нужно убрать время и временную зону, чтобы работал нативный календарь
          if (item.show_start_at)
            item.show_start_at = fullDateToHtmlDate(item.show_start_at);
          if (item.show_end_at)
            item.show_end_at = fullDateToHtmlDate(item.show_end_at);
        }
        items.value = data;
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div class="container publisher-container">
        {device.isDesktop && <MattersCol platformId={props.platformId} />}
        <div>
          <h2>Автодоскролл</h2>
          <div class="grid gap-6 grid-cols-1">
            {items.value.map((el, i) => (
              <AutoscrollItem key={i} item={el} platformId={props.platformId} />
            ))}
          </div>
        </div>
      </div>
    );
  },
});
