import { defineComponent, onMounted, Ref, ref, withModifiers } from "vue";
import { cant } from "@/helpers/permissions";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import Loading from "@/components/Loading";
import Tiny from "@/components/Tiny";

interface FooterType {
  footer_column_first: string;
}

export default defineComponent({
  name: "FooterPage",
  beforeRouteEnter() {
    if (cant("manage_footer")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const loaded = ref(false);
    const item = ref() as Ref<FooterType>;

    onMounted(async () => {
      try {
        const { data } = await api.get<FooterType>(
          `platforms/${props.platformId}`
        );
        item.value = data;
        loaded.value = true;
      } catch (e) {
        error(e);
      }
    });

    let busy = false;
    async function send() {
      if (busy) return;
      busy = true;
      try {
        await api.patch(`platforms/${props.platformId}`, {
          footer_column_first: item.value.footer_column_first,
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () =>
      loaded.value ? (
        <div class="container">
          <form onSubmit={withModifiers(send, ["prevent"])} class="mb-16">
            <Tiny
              value={item.value.footer_column_first}
              onChange={(value) => (item.value.footer_column_first = value)}
              required
            >
              Текст
            </Tiny>
            <div class="bottom-buttons">
              <button class="btn green">Сохранить</button>
            </div>
          </form>
        </div>
      ) : (
        <Loading />
      );
  },
});
