import { defineComponent, ref, onMounted, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import ToplineItemForm, {
  ToplineItemType,
} from "@/components/publishers/ToplineItemForm";
import ToplineItem from "@/components/publishers/ToplineItem";
import api from "@/helpers/api";
import { error } from "@/helpers/notifications";
import Modal from "@/components/Modal";

export default defineComponent({
  name: "ToplineItemsPage",
  beforeRouteEnter() {
    if (cant("manage_topline")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const items = ref<ToplineItemType[]>([]);
    const modalVisible = ref(false);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<ToplineItemType[]>("topline_items", {
          params,
        });
        items.value = data;
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <ToplineItemForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <button
            type="button"
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el, i) => (
          <ToplineItem
            key={el.id}
            item={el}
            onUpdate={(item) => items.value.splice(i, 1, item)}
            onRemove={() => items.value.splice(i, 1)}
          />
        ))}
      </div>
    );
  },
});
