import { defineComponent, ref, onMounted } from "vue";
import { cant } from "@/helpers/permissions";
import MatterCell, { MatterCellType } from "@/components/MatterCell";
import api from "@/helpers/api";
import { error, flash } from "@/helpers/notifications";
import device from "@/helpers/device";
import MattersCol from "@/components/MattersCol";
import Checkbox from "@/components/form/Checkbox";

interface ItemType extends MatterCellType {
  id?: number;
  show_on_main_page: boolean;
  show_on_minor_page: boolean;
}

export default defineComponent({
  name: "LightningPage",
  beforeRouteEnter() {
    if (cant("manage_lightnings")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const items = ref<ItemType[]>([]);

    onMounted(async () => {
      try {
        const params = { platform_id: props.platformId };
        const { data } = await api.get<ItemType[]>("lightning_matters", {
          params,
        });
        items.value = data;
      } catch (e) {
        error(e);
      }
    });

    function addItem() {
      items.value.push({
        matter_id: null,
        matter_title: "",
        show_on_minor_page: false,
        show_on_main_page: false,
      });
    }

    let busy = false;
    async function removeItem(item: ItemType, index: number) {
      if (!item.id) return items.value.splice(index, 1);
      if (busy || !confirm("Подтвердите удаление молнии")) return;
      busy = true;
      try {
        await api.delete(`lightning_matters/${item.id}`, {
          params: { platform_id: props.platformId },
        });
        items.value.splice(index, 1);
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function createItem(item: ItemType, index: number) {
      if (busy) return;
      if (!item.matter_id) error("Пожалуйста, выберите материал");
      busy = true;
      try {
        const { data } = await api.post<ItemType>("lightning_matters", {
          ...item,
          platform_id: props.platformId,
        });
        items.value.splice(index, 1, data);
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    async function updateItem(item: ItemType) {
      if (busy) return;
      if (!item.matter_id) error("Пожалуйста, выберите материал");
      busy = true;
      try {
        await api.patch(`lightning_matters/${item.id}`, {
          ...item,
          platform_id: props.platformId,
        });
        flash("Данные обновлены");
      } catch (e) {
        error(e);
      }
      busy = false;
    }

    return () => (
      <div class="container publisher-container">
        {device.isDesktop && <MattersCol platformId={props.platformId} />}
        <div>
          <div class="mb-16">
            <div class="flex items-center mb-4">
              <h2 class="mb-0">Молнии</h2>
              <button
                type="button"
                class="btn green m-icon ml-2"
                onClick={addItem}
              >
                add_circle
              </button>
            </div>
            <div class="publisher-matters-grid">
              {items.value.map((el, i) => (
                <div key={i}>
                  <MatterCell
                    class="mb-2"
                    item={el}
                    platformId={props.platformId}
                  />
                  <Checkbox
                    value={el.show_on_main_page}
                    onChange={(value) => (el.show_on_main_page = value)}
                  >
                    Показывать на главной
                  </Checkbox>
                  <Checkbox
                    value={el.show_on_minor_page}
                    onChange={(value) => (el.show_on_minor_page = value)}
                  >
                    Показывать на внутренних страницах
                  </Checkbox>
                  <div>
                    {el.id ? (
                      <button
                        class="btn blue"
                        type="button"
                        onClick={() => updateItem(el)}
                      >
                        Обновить
                      </button>
                    ) : (
                      <button
                        class="btn green"
                        type="button"
                        onClick={() => createItem(el, i)}
                      >
                        Сохранить
                      </button>
                    )}
                    <button
                      class="btn red"
                      type="button"
                      onClick={() => removeItem(el, i)}
                    >
                      Удалить
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  },
});
