import { defineComponent, ref, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import Modal from "@/components/Modal";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import TeaserForm, { TeaserType } from "@/components/publishers/TeaserForm";
import TeaserItem from "@/components/publishers/TeaserItem";
import Search from "@/components/Search";

export default defineComponent({
  name: "TeasersPage",
  beforeRouteEnter() {
    if (cant("manage_teaser_blocks")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<TeaserType>(
      "teaser_blocks",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <TeaserForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          ></Search>
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <TeaserItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
