import { defineComponent, ref, Transition } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import Modal from "@/components/Modal";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import TagForm, { TagType } from "@/components/publishers/TagForm";
import TagItem from "@/components/publishers/TagItem";
import Search from "@/components/Search";

export default defineComponent({
  name: "TagsPage",
  beforeRouteEnter() {
    if (cant("manage_tags")) return "/";
  },
  props: {
    platformId: { type: Number, required: true },
  },
  setup(props) {
    const apiParams = ref({
      q: "",
      platform_id: props.platformId,
    });
    const { items, pending, busy, fetch } = useInfScroll<TagType>(
      "tags",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <TagForm
                platformId={props.platformId}
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          ></Search>
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <TagItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
