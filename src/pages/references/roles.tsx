import { defineComponent, ref, Transition } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import { cant } from "@/helpers/permissions";
import RoleForm from "@/components/references/RoleForm";
import RoleItem from "@/components/references/RoleItem";
import { RoleType } from "@/helpers/types";
import Modal from "@/components/Modal";
import InfScroll from "@/components/InfScroll";
import Loading from "@/components/Loading";
import { useStore } from "@/store";

export default defineComponent({
  name: "RolesPage",
  beforeRouteEnter() {
    if (cant("manage_roles")) return "/";
  },
  setup() {
    const store = useStore();
    const { items, pending, busy, fetch } = useInfScroll<RoleType>("roles");
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <RoleForm
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                  store.commit("ADD_ROLE", item);
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        <div class="table-container">
          <table class="list-table">
            <tr>
              <th>Название</th>
              <th>Админ</th>
              <th>Вес</th>
              <th>Создан</th>
              <th>Обновлен</th>
              <th>Действия</th>
            </tr>
            {items.value.map((el) => (
              <RoleItem
                item={el}
                key={el.id}
                onUpdate={(item) => {
                  items.value.splice(items.value.indexOf(el), 1, item);
                }}
                onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
              />
            ))}
          </table>
        </div>
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
