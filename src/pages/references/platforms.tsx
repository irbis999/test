import { defineComponent, ref, Transition } from "vue";
import useInfScroll from "@/helpers/useInfScroll";
import { cant } from "@/helpers/permissions";
import PlatformForm from "@/components/references/PlatformForm";
import { PlatformType } from "@/helpers/types";
import PlatformItem from "@/components/references/PlatformItem";
import Modal from "@/components/Modal";
import InfScroll from "@/components/InfScroll";
import Loading from "@/components/Loading";
import Search from "@/components/Search";
import { useStore } from "@/store";

export default defineComponent({
  name: "PlatformsPage",
  beforeRouteEnter() {
    if (cant("manage_platforms")) return "/";
  },
  setup() {
    const store = useStore();
    const apiParams = ref({
      q: "",
    });
    const { items, pending, busy, fetch } = useInfScroll<PlatformType>(
      "platforms",
      apiParams
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <PlatformForm
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                  store.commit("UPDATE_PLATFORM", item);
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <Search
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
            lazy
          />
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <PlatformItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
