import { defineComponent, onMounted, ref } from "vue";
import { cant } from "@/helpers/permissions";
import MattersCol from "@/components/MattersCol";
import device from "@/helpers/device";
import AutoscrollItem, {
  AutoscrollItemType,
} from "@/components/publishers/AutoscrollItem";
import { error } from "@/helpers/notifications";
import api from "@/helpers/api";

export default defineComponent({
  name: "AutoscrollPage",
  beforeRouteEnter() {
    if (cant("manage_global_autoscrolls")) return "/";
  },
  setup() {
    const items = ref<AutoscrollItemType[]>([]);

    onMounted(async () => {
      try {
        const params = { is_global: true };
        const { data } = await api.get<AutoscrollItemType[]>(
          "autoscroll_configs",
          { params }
        );
        for (const item of data) {
          // Нужно убрать время и временную зону, чтобы работал нативный календарь
          if (item.show_start_at)
            item.show_start_at = item.show_start_at.substr(0, 16);
          if (item.show_end_at)
            item.show_end_at = item.show_end_at.substr(0, 16);
          // Издания - оставляет только id
          // TODO попросить исправить - присылать сразу массив id
          item.platforms = (item.platforms as unknown as { id: number }[]).map(
            (el) => el.id
          );
        }
        items.value = data;
      } catch (e) {
        error(e);
      }
    });

    return () => (
      <div class="container publisher-container">
        {device.isDesktop && <MattersCol />}
        <div>
          <h2>Автодоскролл</h2>
          <div class="grid gap-6">
            {items.value.map((el, i) => (
              <AutoscrollItem item={el} key={i} />
            ))}
          </div>
        </div>
      </div>
    );
  },
});
