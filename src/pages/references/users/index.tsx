/* eslint-disable jsx-a11y/no-onchange */
import { defineComponent, ref, Transition } from "vue";
import { cant, can } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import { useStore } from "@/store";
import { UserType } from "@/helpers/types";
import Modal from "@/components/Modal";
import UserForm from "@/components/references/UserForm";
import UserItem from "@/components/references/UserItem";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";
import Search from "@/components/Search";
import { DropdownNumber } from "@/components/Dropdown";
import styles from "../../../styles/pages/references/Users.module.scss";
import { PlatformsSelectMultiple } from "@/components/PlatformsSelect";

export default defineComponent({
  name: "UsersPage",
  beforeRouteEnter() {
    if (cant("show_users")) return "/";
  },
  setup() {
    const store = useStore();
    const modalVisible = ref(false);
    const apiParams = ref({
      q: "",
      is_active: true,
      platform_id: [] as number[],
      role_id: [] as number[],
    });
    const { items, pending, busy, fetch } = useInfScroll<UserType>(
      "users",
      apiParams
    );
    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <UserForm
                onUpdate={(value) => {
                  items.value.push(value);
                  modalVisible.value = false;
                  store.commit("ADD_USER", value);
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class={`control-bar ${styles.controls}`}>
          <Search
            lazy
            value={apiParams.value.q}
            onChange={(value) => {
              apiParams.value.q = value;
              fetch(true);
            }}
          />
          <PlatformsSelectMultiple
            value={apiParams.value.platform_id}
            onChange={(value) => {
              apiParams.value.platform_id = value;
              fetch(true);
            }}
          />
          <DropdownNumber
            value={apiParams.value.role_id}
            options={store.state.roles as { id: number; title: string }[]}
            onChange={(value) => {
              apiParams.value.role_id = value;
              fetch(true);
            }}
          >
            Роли
          </DropdownNumber>
          <select
            class="f-input"
            onChange={(e) => {
              apiParams.value.is_active =
                (e.target as HTMLSelectElement).value === "1" ? true : false;
              fetch(true);
            }}
          >
            <option value="1">Активен</option>
            <option value="0">Неактивен</option>
          </select>
          {can("manage_users") && (
            <button
              class="btn green m-icon"
              onClick={() => (modalVisible.value = true)}
            >
              add_circle
            </button>
          )}
        </div>
        <div class="table-container">
          <table class="list-table">
            <tr>
              <th>Фамилия</th>
              <th>Имя</th>
              <th>Роль</th>
              <th>Издания</th>
              <th>Эл. почта</th>
              <th>Телефон</th>
              <th>Статус</th>
              <th>Действия</th>
            </tr>
            {items.value.map((el) => (
              <UserItem
                item={el}
                key={el.id}
                onUpdate={(item) => {
                  items.value.splice(items.value.indexOf(el), 1, item);
                }}
              />
            ))}
          </table>
        </div>
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
