import { defineComponent, ref } from "vue";
import { cant } from "@/helpers/permissions";
import useInfScroll from "@/helpers/useInfScroll";
import { useRoute } from "vue-router";
import date from "@/helpers/date";
import Loading from "@/components/Loading";
import InfScroll from "@/components/InfScroll";

interface ItemType {
  id: number;
  created_at: string;
  text: string;
}

export default defineComponent({
  name: "UserPage",
  beforeRouteEnter() {
    if (cant("show_users")) return "/";
  },
  setup() {
    const route = useRoute();
    const apiParams = ref({
      user_id: route.params.id,
    });
    const { items, fetch, pending, busy } = useInfScroll<ItemType>(
      "user_notifications",
      apiParams
    );
    return () => (
      <div class="container">
        <div class="text-2xl font-bold">История пользователя</div>
        {items.value.map((el) => (
          <div class="list-item">
            <span innerHTML={el.text}></span>
            <span>{date(el.created_at)}</span>
          </div>
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
