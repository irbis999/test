import { defineComponent } from "vue";
import { RouterLink, RouterView } from "vue-router";

export default defineComponent({
  setup() {
    return () => (
      <div>
        <div class="submenu">
          <RouterLink class="s-item" to="/tv/channels">
            Телеканалы
          </RouterLink>
          <RouterLink class="s-item" to="/tv/shows">
            Программы
          </RouterLink>
          <RouterLink class="s-item" to="/tv/episodes">
            Эпизоды
          </RouterLink>
        </div>
        <RouterView class="container" />
      </div>
    );
  },
});
