import { defineComponent, ref, Transition } from "vue";
import useInfScroll from "../../helpers/useInfScroll";
import InfScroll from "../../components/InfScroll";
import Loading from "../../components/Loading";
import Modal from "../../components/Modal";
import EpisodeForm, { TvEpisodeType } from "@/components/tv/EpisodeForm";
import EpisodeItem from "@/components/tv/EpisodeItem";

export default defineComponent({
  name: "ChannelsPage",
  setup() {
    const { items, pending, busy, fetch } = useInfScroll<TvEpisodeType>(
      "television/episodes"
    );
    const modalVisible = ref(false);

    return () => (
      <div>
        <Transition name="fade-up">
          {modalVisible.value && (
            <Modal onClose={() => (modalVisible.value = false)}>
              <EpisodeForm
                onUpdate={(item) => {
                  items.value.push(item);
                  modalVisible.value = false;
                }}
              />
            </Modal>
          )}
        </Transition>
        <div class="control-bar">
          <button
            class="btn green m-icon"
            onClick={() => (modalVisible.value = true)}
          >
            add_circle
          </button>
        </div>
        {items.value.map((el) => (
          <EpisodeItem
            item={el}
            key={el.id}
            onUpdate={(item) => {
              items.value.splice(items.value.indexOf(el), 1, item);
            }}
            onRemove={() => items.value.splice(items.value.indexOf(el), 1)}
          />
        ))}
        {pending.value && <Loading />}
        <InfScroll onLoad={() => fetch()} busy={busy.value} />
      </div>
    );
  },
});
