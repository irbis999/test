import { defineComponent, ref, withModifiers } from "vue";
import { useRoute } from "vue-router";
import styles from "../styles/pages/login/login.module.scss";
import { error, flash } from "@/helpers/notifications";
import api from "@/helpers/api";
import { UserType } from "@/helpers/types";
import { useStore } from "@/store";

export default defineComponent({
  name: "LoginPage",
  setup() {
    const store = useStore();
    const route = useRoute();
    const tab = ref<"changePassword" | "password" | "signIn">(
      route.query.token ? "changePassword" : "signIn"
    );
    const password = ref("");
    const password_confirmation = ref("");
    const email = ref("");
    const busy = ref(false);

    async function signIn() {
      if (busy.value) return;
      busy.value = true;
      try {
        const response = await api.post<UserType>("users/sign_in", {
          user: {
            email: email.value,
            password: password.value,
          },
        });
        const user = {
          ...response.data,
          authorization: response.headers.authorization as string,
        };
        store.commit("LOG_IN", user);
        location.assign("/matters");
      } catch (e) {
        error(e);
      }
      busy.value = false;
    }

    async function updatePassword() {
      if (busy.value) return;
      busy.value = true;
      try {
        const user = {
          reset_password_token: route.query.token,
          password: password.value,
          password_confirmation: password_confirmation.value,
        };
        await api.patch(`users/passwords/${route.query.user_id}`, { user });
        flash("Пароль успешно обновлен. Вы можете авторизоваться");
        tab.value = "signIn";
      } catch (e) {
        error(e);
      }
      busy.value = false;
    }

    async function resetPassword() {
      if (busy.value) return;
      busy.value = true;
      try {
        await api.post("users/passwords", { user: { email: email.value } });
        flash("Инструкции по восстановлению пароля отправлены на ваш email");
      } catch (e) {
        error(e);
      }
      busy.value = false;
    }

    return () => (
      <div class={styles.login}>
        <div class={styles.content}>
          {tab.value === "changePassword" && (
            <form
              class="mb-5"
              onSubmit={withModifiers(updatePassword, ["prevent"])}
            >
              <h1>Установите пароль</h1>
              <div class="f-label text-white">Пароль</div>
              <input
                v-model={password.value}
                class="f-input mb-6"
                type="password"
                placeholder="Пароль"
                required
              />
              <div class="f-label text-white">Пароль еще раз</div>
              <input
                v-model={password_confirmation.value}
                class="f-input mb-6"
                type="password"
                placeholder="Пароль еще раз"
                required
              />
              <button class="btn green w-full">Продолжить</button>
            </form>
          )}
          {tab.value === "signIn" && (
            <form class="mb-5" onSubmit={withModifiers(signIn, ["prevent"])}>
              <h1>Авторизация</h1>
              <div class="f-label text-white">Email</div>
              <input
                v-model={email.value}
                class="f-input mb-6"
                type="email"
                placeholder="Email"
                required
              />
              <div class="f-label text-white">Пароль</div>
              <input
                v-model={password.value}
                class="f-input mb-6"
                type="password"
                placeholder="Пароль"
                required
              />
              <button class="btn green w-full">Войти</button>
            </form>
          )}
          {tab.value === "password" && (
            <form
              class="mb-5"
              onSubmit={withModifiers(resetPassword, ["prevent"])}
            >
              <h1>Восстановление пароля</h1>
              <div class="f-label text-white">Email</div>
              <input
                v-model={email.value}
                class="f-input mb-6"
                type="email"
                placeholder="Email"
                required
              />
              <button class="btn green w-full">Восстановить пароль</button>
            </form>
          )}
          <div class="text-center">
            {tab.value === "signIn" && (
              <button onClick={() => (tab.value = "password")}>
                Забыли пароль
              </button>
            )}
            {tab.value === "password" && (
              <button onClick={() => (tab.value = "signIn")}>Отмена</button>
            )}
            {tab.value === "changePassword" && (
              <button onClick={() => (tab.value = "signIn")}>Войти</button>
            )}
          </div>
        </div>
      </div>
    );
  },
});
