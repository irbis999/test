import { defineComponent, ref } from "vue";
import { RouterLink, RouterView } from "vue-router";
import { can } from "@/helpers/permissions";
import { useStore } from "@/store";
import { PlatformsSelectSingle } from "@/components/PlatformsSelect";
// https://www.typescriptlang.org/docs/handbook/2/functions.html#construct-signatures
type ViewType = new () => {
  $props: {
    class: string;
    key: number;
    platformId: number;
  };
};
const View = RouterView as ViewType;
const storageKey = "publisherPlatformId";

export default defineComponent({
  name: "PublishersPage",
  setup() {
    const store = useStore();
    const fromLs = localStorage.getItem(storageKey);
    const platformId = ref(fromLs ? +fromLs : store.state.platforms[0].id);

    return () => (
      <div>
        <div class="submenu __scroll">
          <PlatformsSelectSingle
            class="s-item flex-shrink-0"
            value={platformId.value}
            onChange={(value) => {
              localStorage.setItem(storageKey, value.toString());
              platformId.value = value;
            }}
          />
          {can("manage_main_pages") && (
            <RouterLink class="s-item" to="/publishers/homepage">
              Главная
            </RouterLink>
          )}
          {can("manage_news_feed") && can("manage_news_feed_promo_blocks") && (
            <RouterLink class="s-item" to="/publishers/feed">
              Лента новостей
            </RouterLink>
          )}
          {can("manage_lightnings") && (
            <RouterLink class="s-item" to="/publishers/lightning">
              Молнии
            </RouterLink>
          )}
          {can("manage_rubrics") && (
            <RouterLink class="s-item" to="/publishers/rubrics">
              Рубрики
            </RouterLink>
          )}
          {can("manage_stories") && (
            <RouterLink class="s-item" to="/publishers/stories">
              Сюжеты
            </RouterLink>
          )}
          {can("manage_tags") && (
            <RouterLink class="s-item" to="/publishers/tags">
              Теги
            </RouterLink>
          )}
          {can("manage_columnists") && (
            <RouterLink class="s-item" to="/publishers/columnists">
              Колумнисты
            </RouterLink>
          )}
          {can("manage_autoscrolls") && (
            <RouterLink class="s-item" to="/publishers/autoscroll">
              Автодоскрол
            </RouterLink>
          )}
          {can("manage_teaser_blocks") && (
            <RouterLink class="s-item" to="/publishers/teaser-blocks">
              Тизерные блоки
            </RouterLink>
          )}
          {can("manage_static_pages") && (
            <RouterLink class="s-item" to="/publishers/static-pages">
              Статические страницы
            </RouterLink>
          )}
          {can("manage_menu_items") && (
            <RouterLink class="s-item" to="/publishers/menu-items">
              Меню
            </RouterLink>
          )}
          {can("manage_footer") && (
            <RouterLink class="s-item" to="/publishers/footer">
              Футер
            </RouterLink>
          )}
          {can("manage_topline") && (
            <RouterLink class="s-item" to="/publishers/topline-items">
              Пункты топлайн
            </RouterLink>
          )}
          {can("manage_announcements") && (
            <RouterLink class="s-item" to="/publishers/announcement">
              Анонс
            </RouterLink>
          )}
          {can("manage_banners") && (
            <RouterLink class="s-item" to="/publishers/banners">
              Баннеры
            </RouterLink>
          )}
          {can("manage_special_projects") && (
            <RouterLink class="s-item" to="/publishers/special-projects">
              Спецпроекты
            </RouterLink>
          )}
        </div>
        <View
          class="container"
          key={platformId.value}
          platformId={platformId.value}
        />
      </div>
    );
  },
});
