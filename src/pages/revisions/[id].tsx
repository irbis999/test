import { defineComponent, ref, onMounted } from "vue";
import Item, { ItemType } from "@/components/RevisionPageItem";
import { useRoute, useRouter } from "vue-router";
import api from "@/helpers/api";

export default defineComponent({
  name: "RevisionsPage",
  setup() {
    const route = useRoute();
    const router = useRouter();
    router.currentRoute;
    const items = ref<ItemType[]>([]);
    const params = { matter_id: route.params.id };
    onMounted(async () => {
      const { data } = await api.get<ItemType[]>("revisions", { params });
      items.value = data;
    });

    return () => (
      <div class="container">
        <div class="flex items-center">
          <button class="btn m-icon mr-4" onClick={() => router.go(-1)}>
            arrow_back
          </button>
          <div class="text-2xl font-bold">Версия материала</div>
        </div>
        {items.value.map((item, i) => (
          <Item key={i} item={item} />
        ))}
      </div>
    );
  },
});
