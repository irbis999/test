import MattersCol from "@/components/MattersCol";
import { defineComponent, ref } from "vue";

export default defineComponent({
  name: "TestPage",
  setup() {
    return () => (
      <div class="container grid grid-cols-3">
        <MattersCol />
      </div>
    );
  },
});
