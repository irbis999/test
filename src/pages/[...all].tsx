import { defineComponent } from "vue";

export default defineComponent({
  name: "404Page",
  setup() {
    return () => (
      <div style={{ padding: "20px 30px" }}>
        <div class="font-bold" style={{ fontSize: "140px" }}>
          404
        </div>
        <div class="text-2xl">Страница не найдена</div>
      </div>
    );
  },
});
