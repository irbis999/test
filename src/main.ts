// eslint-disable-next-line import/no-unresolved
import "virtual:windi.css";
import "@/styles/app.scss";
import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
// eslint-disable-next-line import/no-unresolved
import routes from "virtual:generated-pages";
import App from "./App";
import { store, key, ReferencesType } from "@/store";
import api from "./helpers/api";
import { UserType } from "./helpers/types";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const app = createApp(App);
app.use(store, key);

// Авторизация
router.beforeEach((to) => {
  const loggedIn = store.state.loggedIn;
  if (loggedIn && to.name === "login") return "/matters";
  if (to.path === "/") return "/matters";
  if (to.name !== "login" && !loggedIn) return "/login";
});

async function mount() {
  // Авторизация
  const token = localStorage.getItem("token");
  if (token)
    try {
      const { data: user } = await api.get<UserType>("user", {
        headers: { authorization: token },
      });
      store.commit("LOG_IN", user);
      const { data } = await api.get<ReferencesType>("user/references");
      store.commit("SET_REFERENCES", data);
    } catch (e) {
      console.log(e);
    }
  app.use(router);
  app.mount("#app");
  // window.$app = app;
}

void mount();
